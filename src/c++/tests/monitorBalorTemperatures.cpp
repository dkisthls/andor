
#include "atcore.h"


#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <sstream>

static const bool MONITOR_WITH_ACQUISITION=true;
static const int BUFFERCOUNT=10;
static const int FRAMESTOACQUIRE = 100000;
static const int TIMEOUTMS = 10000;
static const int UPDATEINTERVALFRAMES=1;
static const int ROWSTOPRINT=5;
static const int PIXELSPERROWTOPRINT=10;
static const int PAUSEAFTER = false;

static const AT_WC* TRIGGERMODE = L"Internal";

std::vector<std::wstring> temperatureFeatures = {
  L"MainBoard_Air",
  L"MainBoard_Board",
  L"MainBoard_FPGA",
  L"MainBoard_Heatsink",
  L"MainBoard_Internal",
  L"SensorBoard_DDR",
  L"SensorBoard_Internal",
  L"SensorBoard_Kintex",
  L"SensorBoard_Package",
  L"SensorBoard_TEC",
  L"SensorBoard_Zynq"
};

std::vector<double> temperatureValues(temperatureFeatures.size(), 0.0);

//uncomment the line below to print the SDK calls and results from each call
//#define PRINTSDKCALLS

#define CAT(x, y)  x##y
#define WIDE(x)    CAT(L,x)

#ifdef PRINTSDKCALLS
#define RETURN_ON_ERROR(command) { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result <<std::endl; if(result!=AT_SUCCESS) return result; }
#define WARN_ON_ERROR(command)   { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result<<std::endl;}
#else
#define RETURN_ON_ERROR(command) { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl; return result;} }
#define WARN_ON_ERROR(command)   { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl;} }
#endif


typedef std::unique_ptr<AT_U8[]> image_t;

int Run();

void PrintMono16Frame(int frameNumber, unsigned char* frameP, int imageSizeBytes, int aoistride, int pixelCount, int rowCount);

int PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value);
int PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value);
int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value);
int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value);
int PrintBoolFeature(AT_H H, const AT_WC* feature);
int PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision=6);
int PrintStringFeature(AT_H H, const AT_WC* feature);

void WaitForSensorInitialisation(AT_H H);
int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);

int main()
{
  int err = Run();
  if(AT_SUCCESS==err) {
    std::wcout<<L"Completed sucessfully!"<<std::endl;
  }
  else {
    std::wcout<<L"Failed!"<<std::endl;
  }
  
  if (PAUSEAFTER) {
    std::wcout << L"Press enter to exit ..." << std::endl;
    getchar();
  }
  return err;
}

int Run() {
  std::cout << "MONITOR_WITH_ACQUISITION: " << std::boolalpha << MONITOR_WITH_ACQUISITION << std::endl
            << "BUFFERCOUNT: " << BUFFERCOUNT << std::endl
            << "FRAMESTOACQUIRE: " << FRAMESTOACQUIRE << std::endl;
  std::wcout << "TRIGGERMODE: " << TRIGGERMODE << std::endl;

  RETURN_ON_ERROR(AT_InitialiseLibrary());

  AT_64 devcount;
  WARN_ON_ERROR(AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount", &devcount));
  std::wcout << L"Found " << devcount <<L" devices"<< std::endl;

  AT_H H;
  int err = AT_Open(0, &H);

  if (AT_SUCCESS == err) {
    std::wcout << L"Successfully Opened Camera"<< std::endl;

    PrintStringFeature(H, L"CameraName");
    PrintStringFeature(H, L"CameraModel");
    PrintStringFeature(H, L"SerialNumber");
    PrintBoolFeature(H,L"DirectQueueing");
    PrintFloatFeature(H, L"FrameRate");
    AT_64 imageSizeBytes;
    RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));
    std::wcout << L"ImageSizeBytes: " << imageSizeBytes <<L"B"<< std::endl;

    WaitForSensorInitialisation(H);

    PrintAndSetEnumFeature(H,L"TriggerMode",TRIGGERMODE);
    PrintAndSetEnumFeature(H,L"CycleMode",L"Fixed");
    PrintAndSetIntFeature(H,L"FrameCount", FRAMESTOACQUIRE);
    PrintAndSetBoolFeature(H,L"DirectQueueing",AT_FALSE);
//    PrintAndSetFloatFeature(H,L"FrameRate", 10.0);

    std::cout << std::endl << std::string(100,'=') << std::endl;

    // Data headings
    std::wostringstream msgStr;
    for( int i=0; i<temperatureFeatures.size(); i++ )
      msgStr << temperatureFeatures[i] << ", ";
    msgStr << L"TemperatureStatus" << ", " << std::endl;
    std::wcout << msgStr.str();

    err = AcquireFrames(H, FRAMESTOACQUIRE);

    WARN_ON_ERROR(AT_Close(H));
  }
  else {
    std::wcout << L"AT_Open return code: =" << err << std::endl;
  }

  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}


int monitorTemperatures(AT_H H)
{
  int index;
  int sts(AT_SUCCESS);
  AT_WC temperatureStatus[80];

  RETURN_ON_ERROR( AT_GetEnumIndex(H,L"TemperatureStatus",&index) );
  RETURN_ON_ERROR( AT_GetEnumStringByIndex(H,L"TemperatureStatus", index, temperatureStatus, 80) );

 for( int i=0; i<temperatureFeatures.size(); i++ )
  {
    RETURN_ON_ERROR( AT_SetEnumString(H,L"ReadTemperatureChannel", temperatureFeatures[i].c_str()) );
    RETURN_ON_ERROR( AT_GetFloat(H,L"ReadTemperatureValue",&temperatureValues[i]) );
  }

  std::wostringstream msgStr;
  msgStr << std::fixed;
  for( int i=0; i<temperatureValues.size(); i++ )
    msgStr << temperatureValues[i] << ", ";
  msgStr << temperatureStatus << std::endl;
  std::wcout << msgStr.str();

  return sts;
}

static int idx(0);
int monitorTemperatures2(AT_H H)
{
  int index;
  int sts(AT_SUCCESS);
  AT_WC temperatureStatus[80];

  RETURN_ON_ERROR( AT_SetEnumString(H,L"ReadTemperatureChannel", temperatureFeatures[idx].c_str()) );
  RETURN_ON_ERROR( AT_GetFloat(H,L"ReadTemperatureValue",&temperatureValues[idx]) );
  idx = (idx + 1) % temperatureFeatures.size();

  if( idx == 0 )
  {
    RETURN_ON_ERROR( AT_GetEnumIndex(H,L"TemperatureStatus",&index) );
    RETURN_ON_ERROR( AT_GetEnumStringByIndex(H,L"TemperatureStatus", index, temperatureStatus, 80) );

    std::wostringstream msgStr;
    msgStr << std::fixed;
    for( int i=0; i<temperatureValues.size(); i++ )
      msgStr << temperatureValues[i] << ", ";
    msgStr << temperatureStatus << std::endl;
    std::wcout << msgStr.str();
  }

  return sts;
}


void WaitForSensorInitialisation(AT_H H) {

  AT_BOOL sensorInitialisedImplemented = AT_FALSE;
  AT_IsImplemented(H, L"SensorInitialised", &sensorInitialisedImplemented);

  if (sensorInitialisedImplemented == AT_TRUE) {

    AT_BOOL sensorInitialised = AT_FALSE;
    int returnCode = AT_SUCCESS;
    std::wcout << L"Waiting for sensor initialisation...";
    while (AT_SUCCESS == AT_GetBool(H, L"SensorInitialised", &sensorInitialised) && sensorInitialised == AT_FALSE)
    {
      std::wcout << L".";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    std::wcout << L"OK!" << std::endl;
  }
}

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

//  std::wcout << L"ImageSizeBytes: " << imageSizeBytes <<L"B"<< std::endl;

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }

  if( MONITOR_WITH_ACQUISITION )
  {
    RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

    int err = AcquisitionLoop(H,frameCount);

    RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
    RETURN_ON_ERROR(AT_Flush(H));
  }
  else
  {
    for( int i=0; i<FRAMESTOACQUIRE; i++ )
      monitorTemperatures(H);
  }

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));
  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
      if(1==frame) {
        ; //std::wcout<<L"Got first frame"<<std::endl;
      }

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
//        if( duration >= 1.0 )
//        {
//          monitorTemperatures(H);
          monitorTemperatures2(H);
//          start = std::chrono::steady_clock::now();
//        }
        double frameRate = static_cast<double>(frame) / duration;
//        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;
//        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
      return err;
    }
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}

void PrintMono16Frame(int frameNumber, unsigned char* frameP, int imageSizeBytes, int aoistride, int pixelCount, int rowCount)
{
  std::wcout<<L"  Frame "<<frameNumber<<std::endl;

  for(int row = 0; row<rowCount; row++) {
    std::wcout<<L"    Row "<<std::dec<<(row+1)<<L" : "<<std::hex;
    unsigned short *pixels = reinterpret_cast<unsigned short *>(frameP+ aoistride*row);
    for (int i = 0; i < pixelCount; i++) {
      std::wcout << L"0x" << *pixels++;
      if (i < pixelCount- 1) {
        std::wcout << L", ";
      }
    }
    std::wcout << std::endl;
  }

  std::wcout<<L"    Last Row End Pixels: ";

  unsigned short *pixels = reinterpret_cast<unsigned short*>(frameP+imageSizeBytes-pixelCount*2);
  for (int i=0; i<pixelCount; i++) {
    std::wcout<<L"0x"<<*pixels++;
    if (i<pixelCount-1) {
      std::wcout<<L", ";
    }
  }

  std::wcout<<std::endl<<std::dec;
}

int PrintBoolFeature(AT_H H, const AT_WC* feature) {
  AT_BOOL value;

  RETURN_ON_ERROR(AT_GetBool(H, feature, &value));

  std::wcout << feature << L": " << value << std::endl << std::flush;

  return AT_SUCCESS;
}

int PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision) {
  double value;

  RETURN_ON_ERROR(AT_GetFloat(H, feature, &value));
  
  std::wcout << std::fixed << std::fixed << std::setprecision(precision) 
             << feature << ": " << value << std::endl << std::flush;

  return AT_SUCCESS;
}

int PrintStringFeature(AT_H H, const AT_WC* feature) {
  int length = 0;
  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));

  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  std::wcout<<feature<<L": "<<&stringValue[0]<<std::endl;
  return AT_SUCCESS;
}

int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetEnumString(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetInt(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

int PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetBool(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

int PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetFloat(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

