
#include "atcore.h"


#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>



static const int BUFFERCOUNT=10;
static const int FRAMESTOACQUIRE = 10;
static const int TIMEOUTMS = 10000;
static const int UPDATEINTERVALFRAMES=1;
static const int ROWSTOPRINT=5;
static const int PIXELSPERROWTOPRINT=10;
static const int PAUSEAFTER = false;

static const AT_WC* TRIGGERMODE = L"Internal";
//static const AT_WC* TRIGGERMODE = L"External";

//uncomment the line below to print the SDK calls and results from each call
//#define PRINTSDKCALLS

#define CAT(x, y)  x##y
#define WIDE(x)    CAT(L,x)

#ifdef PRINTSDKCALLS
#define RETURN_ON_ERROR(command) { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result <<std::endl; if(result!=AT_SUCCESS) return result; }
#define WARN_ON_ERROR(command)   { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result<<std::endl;}
#else
#define RETURN_ON_ERROR(command) { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl; return result;} }
#define WARN_ON_ERROR(command)   { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl;} }
#endif


typedef std::unique_ptr<AT_U8[]> image_t;

int Run();

void PrintMono16Frame(int frameNumber, unsigned char* frameP, int imageSizeBytes, int aoistride, int pixelCount, int rowCount);

int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value);
int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value);
int PrintStringFeature(AT_H H, const AT_WC* feature);

void WaitForSensorInitialisation(AT_H H);
int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);

int main()
{
  int err = Run();
  if(AT_SUCCESS==err) {
    std::wcout<<L"Completed sucessfully!"<<std::endl;
  }
  else {
    std::wcout<<L"Failed!"<<std::endl;
  }
  
  if (PAUSEAFTER) {
    std::wcout << L"Press enter to exit ..." << std::endl;
    getchar();
  }
  return err;
}

int Run() {
  RETURN_ON_ERROR(AT_InitialiseLibrary());

  AT_64 devcount;
  WARN_ON_ERROR(AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount", &devcount));
  std::wcout << L"Found " << devcount <<L" devices"<< std::endl;

  AT_H H;
  int err = AT_Open(0, &H);

  if (AT_SUCCESS == err) {
    std::wcout << L"Successfully Opened Camera"<< std::endl;

    PrintStringFeature(H, L"CameraName");
    PrintStringFeature(H, L"CameraModel");
    PrintStringFeature(H, L"SerialNumber");

    WaitForSensorInitialisation(H);

    PrintAndSetEnumFeature(H,L"TriggerMode",TRIGGERMODE);
    PrintAndSetEnumFeature(H,L"CycleMode",L"Fixed");
    PrintAndSetIntFeature(H,L"FrameCount", FRAMESTOACQUIRE);

    err = AcquireFrames(H, FRAMESTOACQUIRE);

    WARN_ON_ERROR(AT_Close(H));
  }
  else {
    std::wcout << L"AT_Open return code: =" << err << std::endl;
  }

  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}

void WaitForSensorInitialisation(AT_H H) {

  AT_BOOL sensorInitialisedImplemented = AT_FALSE;
  AT_IsImplemented(H, L"SensorInitialised", &sensorInitialisedImplemented);

  if (sensorInitialisedImplemented == AT_TRUE) {

    AT_BOOL sensorInitialised = AT_FALSE;
    int returnCode = AT_SUCCESS;
    std::wcout << L"Waiting for sensor initialisation " << std::endl;
    while (AT_SUCCESS == AT_GetBool(H, L"SensorInitialised", &sensorInitialised) && sensorInitialised == AT_FALSE)
    {
      std::wcout << L".";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    std::wcout << std::endl;
    std::wcout << L"OK!";
  }
}

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::wcout << L"ImageSizeBytes: " << imageSizeBytes <<L"B"<< std::endl;


  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));
  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;

        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}

void PrintMono16Frame(int frameNumber, unsigned char* frameP, int imageSizeBytes, int aoistride, int pixelCount, int rowCount)
{
  std::wcout<<L"  Frame "<<frameNumber<<std::endl;

  for(int row = 0; row<rowCount; row++) {
    std::wcout<<L"    Row "<<std::dec<<(row+1)<<L" : "<<std::hex;
    unsigned short *pixels = reinterpret_cast<unsigned short *>(frameP+ aoistride*row);
    for (int i = 0; i < pixelCount; i++) {
      std::wcout << L"0x" << *pixels++;
      if (i < pixelCount- 1) {
        std::wcout << L", ";
      }
    }
    std::wcout << std::endl;
  }

  std::wcout<<L"    Last Row End Pixels: ";

  unsigned short *pixels = reinterpret_cast<unsigned short*>(frameP+imageSizeBytes-pixelCount*2);
  for (int i=0; i<pixelCount; i++) {
    std::wcout<<L"0x"<<*pixels++;
    if (i<pixelCount-1) {
      std::wcout<<L", ";
    }
  }

  std::wcout<<std::endl<<std::dec;
}

int PrintStringFeature(AT_H H, const AT_WC* feature) {
  int length = 0;
  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));

  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  std::wcout<<feature<<L": "<<&stringValue[0]<<std::endl;
  return AT_SUCCESS;
}

int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetEnumString(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value)
{
  std::wcout<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetInt(H,feature,value);
  if(AT_SUCCESS == err) {
    std::wcout<<L"OK";
  }
  else {
    std::wcout<<L"Error - "<<err;
  }
  std::wcout<<std::endl;
  return err;
}

