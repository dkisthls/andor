
#include "atcore.h"

#include "AoiRoi.h"
#include "CamUtil.h"
#include "CmdLineUtil.h"
#include "Timer.h"
#include "ZylaCalculator.h"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ios>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>
#include <sstream>
#include <vector>

static const double FRAMERATE = 10;
static const AT_WC* TRIGGERMODE = L"Internal";

AOI fullFrameAOI;

#define _CALC   0    // Array index to calcualted value
#define _ACTUAL 1    // Array index to actual value

typedef std::unique_ptr<AT_U8[]> image_t;

bool debugEnabled(false);                               // Overridden by --debug
bool forceError(false);                                 // Overridden by --forceError
bool printFrameData(DEFAULT_PRINTFRAMEDATA);            // Overridden by --printFrameData
AT_BOOL directQueueing(DEFAULT_DIRECTQUEUEING);         // Overridden by --disableDirectQueueing
AT_BOOL fastAOIFrameRateEnable(DEFAULT_FASTAOI);        // Overridden by --fastAOIFrameRateEnable
int exposureInterval_ms(0);                             // = (1.0/frameRate) * 1000
int framesToAcquire(DEFAULT_FRAMESTOACQUIRE);           // Overridden by --framesToAcquire
int rowsToPrint(DEFAULT_ROWSTOPRINT);                   // Overridden by --rowsToPrint
int pixelsPerRowToPrint(DEFAULT_PIXELSPERROWTOPRINT);   // Overridden by --pixelsPerRowToPrint
int quickCheck(0);                                      // Overiddedn by --quickCheck
int startWith(1);                                       // Overiddedn by --startWith
double exposureTime_s(DEFAULT_EXPOSURETIME);            // Overridden by --exposureTime
double frameRate_Hz(DEFAULT_FRAMERATE);                 // Overridden by --frameRate
struct Counts {
  int tests;
  int fail;
  int pass;
  int warn;
  Counts() : tests(0), fail(0), pass(0), warn(0) {}
};

#define NUM_TESTS 15
std::vector<int> uniqueTests;                 // From command line option --test
std::vector<int> testsToTest;                 // The actual tests to run
std::vector<int> uniqueGainModes;             // From command line option --gainMode
std::vector<std::string> gainModesToTest;     // The actual gainModes to run
std::vector<int> uniqueShutterModes;          // From command line option --shutterMode
std::vector<std::string> shutterModesToTest;  // The actual shutterModes to run
std::vector<int> uniqueTriggerModes;          // From command line option --triggerMode
std::vector<std::string> triggerModesToTest;  // The actual triggerModes to run

// AOI Tests
std::vector<std::string> aoiTestDescriptions = {
/* Test 1 */  "Full Sensor - Sweep AOIHeight from Max to Min, Top fixed at 1",
/* Test 2 */  "Full Sensor - Sweep AOIHeight from Min to Max, Top fixed at 1",
/* Test 3 */  "1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at 1",
/* Test 4 */  "1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at 1",
/* Test 5 */  "1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at Max/2 + 1",
/* Test 6 */  "1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at Max/2 + 1",
/* Test 7 */  "Full Sensor - Sweep AOITop from 1 to (HeightMax-HeightMin+1), Height adjusted from Max to Min",
/* Test 8 */  "Full Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to 1, Height adjusted from Min to Max",
/* Test 9 */  "1/2  Sensor - Sweep AOITop from 1 to HeightMax/2, Height adjusted from Max/2 to Min",
/* Test 10 */ "1/2  Sensor - Sweep AOITop from (HeightMax/2-HeightMin+1) to 1, Height adjusted from Min to Max/2",
/* Test 11 */ "1/2  Sensor - Sweep AOITop from (HeightMax/2+1) to (HeightMax-HeightMin+1), Height adjusted from Max/2 to Min",
/* Test 12 */ "1/2  Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to (HeightMax/2+1), Height adjusted from Min to Max/2",
/* Test 13 */ "Variable - Shrink AOI verticallly staying centered on sensor, Top increment by 1, Height decrement by 2",
/* Test 14 */ "Variable - Shrink AOI vertically and horizontally staying centered on sensor, Top increment by 1, Height/width decrement by 2"
};

// Multitrack Tests
std::vector<std::string> mtTestDescriptions = {
/* Test 15 */ "2 Contiguous MTs - Sweep MT1 start up from Top=1, For each pair MT1 start++, height of MT1&MT2++",
/* Test 16 */ "2 Contiguous MTs - Sweep MT2 down from Top=Max, For each pair MT2 (start+height)--, height of MT1&MT2++",
/* Test 17 */ "2 MTs - MT1/MT2 pin to edges, H1/H2=1, grow number of rows by 1 towards center, total # rows even",
/* Test 18 */ "2 MTs - MT1/MT2 pin to edges, H1=1,H2=2, grow number of rows by 1 towards center, total # rows odd",
/* Test 19 */ "2 MTs - MT1/MT2 height=sensorHeight/2, shrink both by 1 towards edges",
/* Test 20 */ "2 MTs - MT1/MT2 height=1, start at center and grow size by 1 towards edges",
/* Test 21 */ "2 MTs - MT1/MT2 height=sensorHeight/2, shrink height/top together to 1 row @ 1/4 & 3/4 height (even rows)",
/* Test 22 */ "2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT1 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)",
/* Test 23 */ "2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT2 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)",
/* Test 24 */ "2 MTs - Start/End MT1/MT2 at edges, H1/H2=1, size++ towards center, 10x MT1 Start++, MT2 End--",
/* Test 25 */ "2 MTs - MT1/MT2 centered, H1/H2=1, height++ towards edges, total # rows even",
/* Test 26 */ "2 MTs - MT1/MT2 centered, H1=2,H2=1, height++ towards edges, total # rows odd",
/* Test 27 */ "2 MTs - Explicit ViSP Use Cases",
/* Test 28 */ "Specific sensor edge/near sensor edge test cases, top1=1",
/* Test 29 */ "Specific sensor edge/near sensor edge test cases, top2+height=sensorHeight",
/* Test 30 */ "Random MTs"
///* Test 31 */ "SOME MULTITRACK TEST 4"
};

int AcquireFrames(AT_H H, int frameCount, AT_64 bytesPerFrame);
int AcquisitionLoop(AT_H H, int frameCount, AT_64 bytesPerFrame);
int Run( const CamInfo& camInfo );
int setAoiAndCheckCalculated(
  const CamInfo& camInfo,       // Contains device handle and camera ID
  ZylaCalculator& zylaCalc,
  const std::vector<AOI>& AOIs, // AOIs to test
  const std::string& testDescription,
  Counts& counts );
int setMTAndCheckCalculated(
  const CamInfo& camInfo,
  ZylaCalculator& zylaCalc,
  const std::vector<ROI>& ROIs, // ROIs to test
  const std::string& testDescription,
  Counts& counts );
int setMTAndCheckCalculated(
  const CamInfo& camInfo,       // Contains device handle and camera ID
  ZylaCalculator& zylaCalc,
  const std::vector<AOI>& AOIs, // AOIs to test
  const std::string& testDescription,
  Counts& counts );
double calculateLET(
  const double readoutTime_s,
  const double rowReadTime_s );
double calculateReadoutTime(
  const int numRowReads,
  const double rowReadTime_s );
int calculateNumRowReads(
  const CamInfo& camInfo,
  const bool debugEnabled,
  const AOI& AOIs,
  const AT_BOOL fastAOIFrameRateEnable,
  std::string& pathTaken );
int calculateNumRowReads(
  const CamInfo& camInfo,
  const bool debugEnabled,
  const std::vector<AOI>& AOIs,
  const AT_BOOL fastAOIFrameRateEnable,
  std::string& pathTaken );

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
       << "Tests frame generator data values for ALL frame gen modes\n"
       << "   -OR-\n"
       << "Tests selected frame gen modes for correct data values!\n\n" 
       << "Usage: " << progname << " [options]\n"
       << "     --help\n"
       << "       This usage help\n"
       << std::endl
       << "     --debug\n"
       << "       Enable debug output.\n"
       << std::endl
       << "     --forceError=<int>\n"
       << "       Force an error condition.\n"
       << std::endl
       << "     --disableDirectQueueing\n"
       << "       Direct queueing is enabled by default. Use this flag to disable it.\n"
       << std::endl
       << "     --fastAOIFrameRateEnable\n"
       << "       Enable the \"fastAOIFrameRateEnable\" feature of the camera.\n"
       << std::endl
       << "     --quickCheck=<int>\n"
       << "       Execute but only test the first N test cases.\n"
       << "       <int> is one of:\n"
       << "         0 - Disable quick check\n"
       << "         1 <= value <= 100 : Number of test cases to check\n"
       << std::endl
       << "     --startWith=<int>\n"
       << "       Start tests beginning with supplied test number (default=1).\n"
       << std::endl
       << "     --exposureTime=<val>\n"
       << "       The exposure time in seconds (default="
       <<         std::fixed << DEFAULT_EXPOSURETIME << "s).\n"
       << std::endl
       << "     --framesToAcquire=<val>\n"
       << "       The number of frames to acquire and test (default="
       <<         DEFAULT_FRAMESTOACQUIRE << ").\n"
       << std::endl
       << "     --gainMode=<int>{,<int>}\n"
       << "       Used to specify the gain mode(s) to use in conjunction with this test.\n"
       << "       <int> is one of:\n"
       << "         Andor Balor Camera:\n";
  for( int i=0; i<balorGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorGainModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaGainModes[i] << std::endl;
  std::cout << "       (default=All gainModes of camera)\n"
       << std::endl
       << "     --shutterMode=<int>{,<int>}\n"
       << "       Used to specify the shutter mode(s) to use in conjunction with this test.\n"
       << "       <int> is one of:\n"
       << "         Andor Balor Camera:\n";
  for( int i=0; i<balorShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorShutterModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaShutterModes[i] << std::endl;
  std::cout << "       (default=All CSS supported shutter modes)\n"
       << std::endl
       << "     --triggerMode=<int>{,<int>}\n"
       << "       Used to specify the trigger mode to use in conjunction with this test.\n"
       << "       <int> is one of\n";
  for( int i=0; i<cssSupportedTriggerModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << cssSupportedTriggerModes[i] << std::endl;
  std::cout << "       (default=All CSS supported trigger modes)\n"
       << std::endl
       << "     --printFrameData\n"
       << "       Print pixel data for each frame acquired (default="
       <<         std::boolalpha << DEFAULT_PRINTFRAMEDATA << ").\n"
       << std::endl
       << "     --pixelsPerRowToPrint=<val>\n"
       << "       When --printFrameData is present, print the specified number \n"
       << "       of pixels-per-row (default=" << DEFAULT_PIXELSPERROWTOPRINT << ").\n"
       << std::endl
       << "     --rowsToPrint=<val>\n"
       << "       When --printFrameData is present, print the specified number \n"
       << "       of rows (default=" << DEFAULT_ROWSTOPRINT << ").\n"
       << std::endl
       << "     --test=<int>{,<int>,<int>,<int>}\n"
       << "       Used to select specific tests (default=All tests).\n"
       << "       <int> is one of:\n"
       << "         AOI Tests:\n";
int testNum(1);
for( int i=0; i<aoiTestDescriptions.size(); i++, testNum++ )
  std::cout << "           " << std::setw(2) << std::right << testNum << ": " << aoiTestDescriptions[i] << std::endl;

std::cout << "         Multitrack Tests:\n";
for( int i=0; i<mtTestDescriptions.size(); i++, testNum++ )
  std::cout << "           " << std::setw(2) << std::right << testNum << ": " << mtTestDescriptions[i] << std::endl;
std::cout << std::endl;
}
// End of usage()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "debug" )
        {
          debugEnabled = true;
        }
        else if( option == "forceError" )
        {
          forceError = true;
        }
        else if( option == "disableDirectQueueing" )
        {
          directQueueing = AT_FALSE;
        }
        else if( option == "fastAOIFrameRateEnable" )
        {
          fastAOIFrameRateEnable = AT_TRUE;
        }
        else if( (cmdSwitchPos = option.find("quickCheck")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            quickCheck = std::stoi(option.substr(equalsSign+1));
            if( quickCheck < 0 || quickCheck > 100 )
              throw std::out_of_range("value must be in the range 0 <= n <= 100, 0 == disable");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("startWith")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            startWith = std::stoi(option.substr(equalsSign+1));
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("exposureTime")) != std::string::npos &&
                  cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            exposureTime_s = std::stof(option.substr(equalsSign+1));
            if( exposureTime_s <= 0.0 )
              throw std::out_of_range("value must be > 0.0");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("forceError")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            forceError = std::stoi(option.substr(equalsSign+1));
            if( forceError < 0 || forceError > 2 )
              throw std::out_of_range("value must be 0 <= val <= 2");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("frameRate")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            frameRate_Hz = std::stof(option.substr(equalsSign+1));
            if( frameRate_Hz <= 0 )
              throw std::out_of_range("value must be > 0");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("framesToAcquire")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            framesToAcquire = std::stoi(option.substr(equalsSign+1));
            if( framesToAcquire < 1 )
              throw std::out_of_range("value must be >= 1");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("gainMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxModes = std::max(balorGainModes.size(), zylaGainModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueGainModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("shutterMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxModes = std::max(balorShutterModes.size(), zylaShutterModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueShutterModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("triggerMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxModes = cssSupportedTriggerModes.size();
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueTriggerModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( option == "printFrameData" )
        {
          printFrameData = true;
        }
        else if( (cmdSwitchPos = option.find("pixelsPerRowToPrint")) != std::string::npos &&
                  cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            pixelsPerRowToPrint = std::stoi(option.substr(equalsSign+1));
            if( pixelsPerRowToPrint < 1 ) // Upper limit will be checked when camera is known
              throw std::out_of_range("value outside of sensor limits");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("rowsToPrint")) != std::string::npos &&
                  cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            rowsToPrint = std::stoi(option.substr(equalsSign+1));
            if( rowsToPrint < 1 ) // Upper limit will be checked when camera is known
              throw std::out_of_range("value outside of sensor limits");
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("test")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueTests = CmdLineUtil::getIntChoices(
              &argv[0][0], values, aoiTestDescriptions.size() + mtTestDescriptions.size() );
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else
        { 
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          CmdLineUtil::cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    } 
  } catch( CommandLineError& err ) {
    errMsg << err.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  }

  CamInfo camInfo;
  int err = CamUtil::OpenAndIdentify( camInfo );

  fullFrameAOI = AOI(1,1,camInfo.getSensorWidth(), camInfo.getSensorHeight(),"Full Sensor");

  // A couple more cmdline arguments to check now that we know the sensor size
  if( rowsToPrint != DEFAULT_ROWSTOPRINT && rowsToPrint > camInfo.getSensorHeight() )
  {
    errMsg << "value outside of sensor limits: rowsToPrint";
    CmdLineUtil::cmdLineError( progName, errMsg );
  }
  if( pixelsPerRowToPrint != DEFAULT_PIXELSPERROWTOPRINT && 
      pixelsPerRowToPrint > camInfo.getSensorWidth() )
  {
    errMsg << "value outside of sensor limits: pixelsPerRowToPrint";
    CmdLineUtil::cmdLineError( progName, errMsg );
  }

  //------ Process gainMode choices from command line
  if( !uniqueGainModes.empty() )
  {
    for( int v : uniqueGainModes )
    {
      if( v <= 0 || v > camInfo.getGainModes().size() )
      {
        errMsg << "invalid selected gainMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        gainModesToTest.push_back( camInfo.getGainModes()[v-1] );
    }
  }
  else
    gainModesToTest = camInfo.getGainModes();

  //------ Process shutterMode choices from command line
  if( !uniqueShutterModes.empty() )
  {
    for( int v : uniqueShutterModes )
    {
      if( v <= 0 || v > camInfo.getShutterModes().size() )
      {
        errMsg << "invalid selected shutterMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        shutterModesToTest.push_back( camInfo.getShutterModes()[v-1] );
    }
  }
  else
    shutterModesToTest = camInfo.getShutterModes();

  //------ Process triggerMode choices from command line
  if( !uniqueTriggerModes.empty() )
  {
    for( int v : uniqueTriggerModes )
    {
      if( v <= 0 || v > camInfo.getCssTriggerModes().size() )
      {
        errMsg << "invalid selected triggerMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        triggerModesToTest.push_back( camInfo.getCssTriggerModes()[v-1] );
    }
  }
  else
    triggerModesToTest = camInfo.getCssTriggerModes();

  // Summarize test setup and command line options...
  std::cout << "General Test Parameters:" << std::endl;
  std::cout << "  debugEnabled=" << std::boolalpha << debugEnabled << std::endl;
  std::cout << "  directQueueing=" << std::boolalpha << ((directQueueing == AT_TRUE) ? true : false) << std::endl;
  std::cout << "  fastAOIFrameRateEnable=" << std::boolalpha << ((fastAOIFrameRateEnable == AT_TRUE) ? true : false) << std::endl;
  std::cout << "  quickCheck=" << quickCheck << std::endl;
  std::cout << "  startWith=" << startWith << std::endl;
  std::cout << "  exposureTime=" << std::fixed << exposureTime_s << "sec" << std::endl;
  std::cout << "  framesToAcquire=" << std::dec << framesToAcquire << std::endl;
  exposureInterval_ms = (1.0 / frameRate_Hz) * 1000.0;
  std::cout << "  exposureInterval=" << std::fixed << exposureInterval_ms << "ms" << std::endl;

  std::cout << "  gainModesToTest=";
  for( int i=0; i<gainModesToTest.size()-1; i++ )
    std::cout << "\"" << gainModesToTest[i] << "\",";
  std::cout << "\"" << gainModesToTest[gainModesToTest.size()-1] << "\"" << std::endl;

  std::cout << "  shutterModesToTest=";
  for( int i=0; i<shutterModesToTest.size()-1; i++ )
    std::cout << "\"" << shutterModesToTest[i] << "\",";
  std::cout << "\"" << shutterModesToTest[shutterModesToTest.size()-1] << "\"" << std::endl;

  std::cout << "  triggerModesToTest=";
  for( int i=0; i<triggerModesToTest.size()-1; i++ )
    std::cout << "\"" << triggerModesToTest[i] << "\",";
  std::cout << "\"" << triggerModesToTest[triggerModesToTest.size()-1] << "\"" << std::endl;

  if( !uniqueTests.empty() )
    testsToTest = uniqueTests;
  else
    for( int i=1; i<=aoiTestDescriptions.size() + mtTestDescriptions.size(); i++ )
      testsToTest.push_back(i);

  std::cout << "  testsToTest=";
  for( int i=0; i<testsToTest.size()-1; i++ )
    std::cout << testsToTest[i] << ",";
  std::cout << testsToTest[testsToTest.size()-1] << std::endl;

  std::cout << "  printFrameData=" << std::boolalpha << printFrameData << std::endl;
  if( printFrameData )
  {
    std::cout << "    Rows to print=" << rowsToPrint << std::endl;
    std::cout << "    Pixels-per-row to print=" << pixelsPerRowToPrint << std::endl;
  }

  // Do the actual tests...
  err = Run( camInfo );

  if(AT_SUCCESS==err) {
    std::cout << "Completed sucessfully!" << std::endl;
  }
  else {
    std::cout << "Failed!" << std::endl;
  }

  if (PAUSEAFTER) {
    std::wcout << "Press enter to exit ..." << std::endl;
    getchar();
  }

  WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}
// End of main()


//---------------------------------- Run() -------------------------------------
//
int Run( const CamInfo& camInfo )
{
  bool success(true);
  int err(AT_SUCCESS);
  int setupNumber(0);
  AT_H H(camInfo.getHandle());
  std::vector<std::string> testSummary;
  std::ostringstream testSummaryString;
  ZylaCalculator zylaCalc( camInfo, debugEnabled );

  auto totalTestDurationStart = std::chrono::steady_clock::now();
  Counts totalCounts;

  for( std::string triggerMode : triggerModesToTest )
  {
    for( std::string shutterMode : shutterModesToTest )
    {
      for( std::string gainMode : gainModesToTest )
      {
        setupNumber++;
        std::ostringstream setupDescription;
        setupDescription << "SETUP #" << std::setfill('0') << std::setw(2) << setupNumber
                         << ": TriggerMode=" << triggerMode
                         << ", ElectronicShutteringMode=" << std::setfill(' ') << std::left << std::setw(7) << shutterMode
                         << ", GainMode=\"" << gainMode << "\"";

        std::cout << std::endl << std::string(100,'=') << std::endl
                  << setupDescription.str() << std::endl;

        testSummary.push_back( setupDescription.str() );

        auto timenow =
          std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::cout << "Date: " << std::ctime(&timenow);

        RETURN_ON_ERROR( CamUtil::PrintAndSetEnumFeature( H,
          L"ElectronicShutteringMode", CamUtil::string2wcs(shutterMode).c_str()) );
        if( camInfo.getCameraType() == ::CameraType::BALOR )
        {
          RETURN_ON_ERROR( CamUtil::PrintAndSetEnumFeature( H,
            L"GainMode", CamUtil::string2wcs(gainMode).c_str()) );
        }
        else
        {
          RETURN_ON_ERROR( CamUtil::PrintAndSetEnumFeature( H,
            L"SimplePreAmpGainControl", CamUtil::string2wcs(gainMode).c_str()) );
        }

        RETURN_ON_ERROR( CamUtil::SetBasicConfiguration(camInfo) );

        RETURN_ON_ERROR( CamUtil::PrintAndSetIntFeature( H,
          L"FrameCount", framesToAcquire) );
        RETURN_ON_ERROR( CamUtil::PrintAndSetFloatFeature( H,
          L"FrameRate", FRAMERATE) );

        double expMin;
        RETURN_ON_ERROR( AT_GetFloatMin( H,
          L"ExposureTime", &expMin) );
        RETURN_ON_ERROR( CamUtil::PrintAndSetFloatFeature( H,
          L"ExposureTime", expMin) );

        RETURN_ON_ERROR( CamUtil::PrintAndSetBoolFeature( H,
          L"FastAOIFrameRateEnable", fastAOIFrameRateEnable) );

        RETURN_ON_ERROR( CamUtil::PrintAndSetEnumFeature( H,
          L"TriggerMode", CamUtil::string2wcs(triggerMode).c_str()) );

        // Configure Zyla calculator for current modes
        zylaCalc.setFastAOIFrameRateEnable( fastAOIFrameRateEnable );
        zylaCalc.setShutterMode( shutterMode );
        zylaCalc.setTriggerMode( triggerMode );

        // Ok, now on to AOI testing
        AT_64 aoiHeightMin;
        AT_64 aoiHeightMax;
        AT_64 aoiTopMin;
        AT_64 aoiTopMax;
        RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIHeight", &aoiHeightMin) );
        RETURN_ON_ERROR( AT_GetIntMax(H, L"AOIHeight", &aoiHeightMax) );
        RETURN_ON_ERROR( AT_GetIntMin(H, L"AOITop", &aoiTopMin) );
        RETURN_ON_ERROR( AT_GetIntMax(H, L"AOITop", &aoiTopMax) );

        for( auto testNum : testsToTest )
        {
          std::cout << std::endl << std::string(100,'=') << std::endl;

          int left(1);
          int width(camInfo.getSensorWidth());
          int sensorHeight(camInfo.getSensorHeight());
          int halfHeight(sensorHeight / 2);
          std::vector<AOI> AOIs;
          std::vector<ROI> ROIs;
          auto testStart = std::chrono::steady_clock::now();

          std::ostringstream testDescription;
          testDescription << "Test #" << std::left << std::setw(2) << testNum << ": ";
          if( testNum < aoiTestDescriptions.size() + 1 )
            testDescription << aoiTestDescriptions[testNum-1];
          else
            testDescription << mtTestDescriptions[testNum - aoiTestDescriptions.size() - 1];

          int err;
          Counts counts;
          switch( testNum ) {
            //================================= AOI TESTS
            //
            case 1:
            {
              // Full Sensor - Sweep AOIHeight from Max to Min, Top fixed at 1
              int top(1);
              for( int height=aoiHeightMax; height>=aoiHeightMin; height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 2:
            {

              // Full Sensor - Sweep AOIHeight from Min to Max, Top fixed at 1";
              int top(1);
              for( int height=aoiHeightMin; height<=aoiHeightMax; height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 3:
            {
              // 1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at 1
              int top(1);
              for( int height=aoiHeightMax/2; height>=aoiHeightMin; height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 4:
            {
              // 1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at 1
              int top(1);
              for( int height=aoiHeightMin; height<=aoiHeightMax/2; height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 5:
            {
              // 1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at Max/2 + 1
              int top(aoiHeightMax/2+1);
              for( int height=aoiHeightMax/2; height>=aoiHeightMin; height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 6:
            {
              // 1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at Max/2 + 1
              int top(aoiHeightMax/2+1);
              for( int height=aoiHeightMin; height<=aoiHeightMax/2; height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 7:
            {
              // Full Sensor - Sweep AOITop from 1 to (HeightMax-HeightMin+1), Height adjusted from Max to Min
              int height(aoiHeightMax);
              for( int top=1; top<=aoiHeightMax-aoiHeightMin+1; top++, height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 8:
            {
              // Full Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to 1, Height adjusted from Min to Max
              int height(aoiHeightMin);
              for( int top=aoiHeightMax-aoiHeightMin+1; top>=1; top--, height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 9:
            {
              // 1/2  Sensor - Sweep AOITop from 1 to HeightMax/2, Height adjusted from Max/2 to Min
              int height(aoiHeightMax/2);
              int topMin(1);
              int topMax(aoiHeightMax/2-aoiHeightMin+1);
              for( int top=topMin; top<=topMax; top++, height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 10:
            {
              // 1/2  Sensor - Sweep AOITop from (HeightMax/2-HeightMin+1) to 1, Height adjusted from Min to Max/2
              int height(aoiHeightMin);
              int topMin(1);
              int topMax(aoiHeightMax/2-aoiHeightMin+1);
              for( int top=topMax; top>=topMin; top--, height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 11:
            {
              // 1/2  Sensor - Sweep AOITop from (HeightMax/2+1) to (HeightMax-HeightMin+1), Height adjusted from Max/2 to Min
              int height(aoiHeightMax/2);
              int topMin(aoiHeightMax/2+1);
              int topMax(aoiHeightMax-aoiHeightMin+1);
              for( int top=topMin; top<=topMax; top++, height-- )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 12:
            {
              // 1/2  Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to (HeightMax/2+1), Height adjusted from Min to Max/2
              int height(aoiHeightMin);
              int topMin(aoiHeightMax/2+1);
              int topMax(aoiHeightMax-aoiHeightMin+1);
              for( int top=topMax; top>=topMin; top--, height++ )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 13:
            {
              // Variable - Shrink AOI verticallly staying centered on sensor, Top increment by 1, Height decrement by 2
              for( int height=aoiHeightMax, top=1; height>=aoiHeightMin; top++, height-=2 )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 14:
            {
              // Variable - Shrink AOI vertically and horizontally staying centered on sensor, Top increment by 1, Height/width decrement by 2
              for( int height=aoiHeightMax, top=1; height>=aoiHeightMin; left++, top++, height-=2, width-=2 )
                AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setAoiAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            //================================= MULTITRACK TESTS
            //
            case 15:
            {
              // 2 Contiguous MTs - Sweep MT1 start up from Top=1, For each pair MT1 start++, height of MT1&MT2++
              AOI aoi1, aoi2;
              int top(1);
              int height(1);
              do {
                aoi1 = AOI( left, top, width, height, "MT1: " );
                aoi2 = AOI( left, aoi1.top + aoi1.height, width, height, "MT2: " );
                AOIs.push_back( aoi1 );
                AOIs.push_back( aoi2 );
                top++;
                height++;
              } while( aoi2.top+2 + aoi2.height+2 - 1 <= sensorHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 16:
            {
              // 2 Contiguous MTs - Sweep MT2 down from Top=Max, For each pair MT2 (start+height)--, height of MT1&MT2++
              AOI aoi1, aoi2;
              int top( sensorHeight );
              int height(1);
              do {
                aoi2 = AOI( left, top - height + 1, width, height, "MT2: " );
                aoi1 = AOI( left, aoi2.top - height, width, height, "MT1: " );
                AOIs.push_back( aoi1 );
                AOIs.push_back( aoi2 );
                top--;
                height++;
             } while( top - (height * 2) - 1 >= 1 );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 17:
            {
              // 2 MTs - MT1/MT2 pin to edges, H1/H2=1, grow number of rows by 1 towards center, total # rows even
              AOI aoi1, aoi2;
              int top1(1), top2(sensorHeight);
              int height(1);
              do {
                AOIs.emplace_back( left, top1, width, height, "MT1: " );
                AOIs.emplace_back( left, top2, width, height, "MT2: " );
                height++;
                top2--;
              } while( top1+height-1 <= halfHeight && top2 > halfHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 18:
            {
              // 2 MTs - MT1/MT2 pin to edges, H1=1,H2=2, grow number of rows by 1 towards center, total # rows odd
              AOI aoi1, aoi2;
              int height1(1), height2(2);
              int top1(1), top2(sensorHeight-height2+1);
              do {
                AOIs.emplace_back( left, top1, width, height1, "MT1: " );
                AOIs.emplace_back( left, top2, width, height2, "MT2: " );
                height1++;
                height2++;
                top2--;
              } while( top1+height1-1 <= halfHeight && top2 > halfHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 19:
            {
              // 2 MTs - MT1/MT2 height=sensorHeight/2, shrink both by 1 towards edges
              AOI aoi1, aoi2;
              int height(halfHeight);
              int top1(1), top2(sensorHeight-height+1);
              do {
                AOIs.emplace_back( left, top1, width, height, "MT1: " );
                AOIs.emplace_back( left, top2, width, height, "MT2: " );
                height--;
                top2++;
              } while( height >= 1 );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 20:
            {
              // 2 MTs - MT1/MT2 height=1, start at center and grow size by 1 towards edges
              AOI aoi1, aoi2;
              int height(1);
              int top1(halfHeight), top2(halfHeight + 1);
              do {
                AOIs.emplace_back( left, top1, width, height, "MT1: " );
                AOIs.emplace_back( left, top2, width, height, "MT2: " );
                top1--;
                height++;
              } while( top1 >= 1 && top2+height-1 <= sensorHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 21:
            {
              // 2 MTs - MT1/MT2 height=sensorHeight/2, shrink height/top together to 1 row @ 1/4 & 3/4 height (even rows)
              AOI aoi1, aoi2;
              int height(halfHeight);
              int top1(1), top2(halfHeight + 1);
              // Both MTs top++ while shrinking size by 2 (even rows)
              do {
                AOIs.emplace_back( left, top1, width, height, "MT1: " );
                AOIs.emplace_back( left, top2, width, height, "MT2: " );
                top1++;
                top2++;
                height -= 2;
              } while( height >= 1 );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 22:
            {
              // 2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT1 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)
              int height1(halfHeight), height2(halfHeight);
              int top1(1), top2(halfHeight + 1);
              // Both MTs top++ while shrinking MT1 size by 3 and MT2 size by 2 (odd rows)
              do {
                AOIs.emplace_back( left, top1, width, height1, "MT1: " );
                AOIs.emplace_back( left, top2, width, height2, "MT2: " );
                top1++;
                top2++;
                height1 -= 3;
                height2 -= 2;
              } while( height1 >= 1 && height2 >= 1 );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 23:
            {
              // 2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT2 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)
              int height1(halfHeight), height2(halfHeight);
              int top1(1), top2(halfHeight + 1);
              // Both MTs top++ while shrinking MT1 size by 2 and MT2 size by 3 (odd rows)
              do {
                AOIs.emplace_back( left, top1, width, height1, "MT1: " );
                AOIs.emplace_back( left, top2, width, height2, "MT2: " );
                top1++;
                top2++;
                height1 -= 2;
                height2 -= 3;
              } while( height1 >= 1 && height2 >= 1 );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 24:
            {
              // 2 MTs - Start/End MT1/MT2 at edges, H1/H2=1, size++ towards center, 10x MT1 Start++, MT2 End--
              AOI aoi1, aoi2;
              for( int i=0; i<10; i++ )
              {
                // Move starting row for MT1 and ending row for MT2 in by one for each set
                int top1(i+1), top2(sensorHeight-i);
                int height(1);
                do {
                  AOIs.emplace_back( left, top1, width, height, "MT1: " );
                  AOIs.emplace_back( left, top2, width, height, "MT2: " );
                  height++;
                  top2--;
                } while( top1+height-1 <= halfHeight && top2 > halfHeight );
              }

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 25:
            {
              // 2 MTs - MT1/MT2 centered, H1/H2=1, height++ towards edges, total # rows even
              AOI aoi1, aoi2;
              int top1(halfHeight), top2(halfHeight+1);
              int height(1);
              do {
                AOIs.emplace_back( left, top1, width, height, "MT1: " );
                AOIs.emplace_back( left, top2, width, height, "MT2: " );
                top1--;
                height++;
              } while( top1 >= 1 && top2+height-1 <= sensorHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 26:
            {
              // 2 MTs - MT1/MT2 centered, H1=2,H2=1, height++ towards edges, total # rows odd 
              AOI aoi1, aoi2;
              int height1(2), height2(1);
              int top1(halfHeight-height1+1), top2(halfHeight+1);
              do {
                AOIs.emplace_back( left, top1, width, height1, "MT1: " );
                AOIs.emplace_back( left, top2, width, height2, "MT2: " );
                top1--;
                height1++;
                height2++;
              } while( top1 >= 1 && top2+height2-1 <= sensorHeight );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 27:
            {
              // 2 MTs - Explicit ViSP Use Cases
              ROIs.push_back( ROI( 0, 0,    2560, 1000, "ROI1: " ) );
              ROIs.push_back( ROI( 0, 1160, 2560, 1000, "ROI2: " ) );

              ROIs.push_back( ROI( 0, 447,  2560, 186,  "ROI1: " ) );
              ROIs.push_back( ROI( 0, 1527, 2560, 186,  "ROI2: " ) );

              ROIs.push_back( ROI( 0, 0,    2560, 890,  "ROI1: " ) );
              ROIs.push_back( ROI( 0, 1270, 2560, 890,  "ROI2: " ) );

              ROIs.push_back( ROI( 0, 447,  2560, 186,  "ROI1: " ) );
              ROIs.push_back( ROI( 0, 1527, 2560, 186,  "ROI2: " ) );

              // Theoreticals for ViSP
              ROIs.push_back( ROI( 0, 0,    2560, 50,   "ROI1: " ) );  // Edges
              ROIs.push_back( ROI( 0, 2110, 2560, 50,   "ROI2: " ) );

              ROIs.push_back( ROI( 0, 1230, 2560, 50,   "ROI1: " ) );  // Center
              ROIs.push_back( ROI( 0, 1280, 2560, 50,   "ROI2: " ) );

              ROIs.push_back( ROI( 0, 515,  2560, 50,   "ROI1: " ) );  // Centered in half
              ROIs.push_back( ROI( 0, 1595, 2560, 50,   "ROI2: " ) );

              ROIs.push_back( ROI( 0, 1230, 2560, 50,   "ROI1: " ) );  // Center
              ROIs.push_back( ROI( 0, 1595, 2560, 50,   "ROI2: " ) );  // Centered in half

              err = setMTAndCheckCalculated( camInfo, zylaCalc, ROIs, testDescription.str(), counts );
              break;
            }

            case 28:
            {
              // Specific sensor edge/near sensor edge test cases, top1=1
              AOI aoi1, aoi2;
              int top1, top2, height;

              int maxHeight(10);
              for( int sep=0; sep<10; sep++ )
              {
                for( int top1=1; top1<10; top1++ )
                {
                  for( int height=1; height<maxHeight-top1+1; height++ )
                  {
                    AOIs.emplace_back( left, top1, width, height, "MT1: " );
                    AOIs.emplace_back( left, top1+height+sep, width, height, "MT2: " );
                  }
                }
              }

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 29:
            {
              // Specific sensor edge/near sensor edge test cases, top2+height=sensorHeight
              AOI aoi1, aoi2;
              int maxHeight(10);

              for( int sep=0; sep<10; sep++ )
              {
                for( int end2=sensorHeight; end2>sensorHeight-10; end2-- )
                {
                  for( int height=1; height<10; height++ )
                  {
                    int start2 = end2 - height + 1;
                    int end1   = start2 - 1;
                    int start1 = end1 - height + 1;
                    AOIs.emplace_back( left, start1, width, height, "MT1: " );
                    AOIs.emplace_back( left, start2, width, height, "MT2: " );
                  }
                }
              }

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

            case 30:
            {
              // Random MTs
              AOI aoi1, aoi2;
              int top1, top2, height;

              std::srand(std::time(nullptr)); // use current time as seed for random generator
              int random_variable = std::rand();
              std::cout << "Random value on [0 " << RAND_MAX << "]: "
                        << random_variable << '\n';

              for( int i=0; i<500; i++)
              {
                int maxValue = sensorHeight - 1;
                int start1 = (std::rand() % maxValue) + 1;

                maxValue = sensorHeight - start1;
                int height1 = (std::rand() % maxValue) + 1;

                AOIs.emplace_back( left, start1, width, height1, "MT1: " );

                maxValue = sensorHeight - (start1 + height1 - 1);
                int start2 = start1 + height1 + (std::rand() % maxValue);

                maxValue = sensorHeight - start2 + 1;
                int height2 = (std::rand() % maxValue) + 1;

                AOIs.emplace_back( left, start2, width, height2, "MT2: " );
              }

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }

/*            case 31:
            {
              // SOME MULTITRACK TEST NAME
              AOI aoi1, aoi2;
              int top(1);
              int height(1);
              // do {
              //   aoi1 = AOI( left, top, width, height, "MT1: " );
              //   aoi2 = AOI( left, aoi1.top + aoi1.height, width, height, "MT2: " );
              //   AOIs.emplace_back( aoi1 );
              //   AOIs.emplace_back( aoi2 );
              //   top++;
              //   height++;
              // } while( aoi2.top+2 + aoi2.height+2 - 1 <= sensorHeight );

              aoi1 = AOI( left, 24, width, 10, "MT1: " );
              aoi2 = AOI( left, 34, width, 10, "MT2: " );
              AOIs.emplace_back( aoi1 );
              AOIs.emplace_back( aoi2 );

  //            for( int height=1; height<aoiHeightMax; height>=aoiHeightMin; height-- )
  //              AOIs.emplace_back( left, top, width, height, "AOI: " );

              err = setMTAndCheckCalculated( camInfo, zylaCalc, AOIs, testDescription.str(), counts );
              break;
            }
*/
            default:
              std::cout << "NO TEST DEFINED FOR TEST #" << testNum << std::endl;
              return !AT_SUCCESS;
          }

          // Increment total number of pass/warn/fail test counters
          totalCounts.tests += counts.tests;
          totalCounts.pass  += counts.pass;
          totalCounts.fail  += counts.fail;
          totalCounts.warn  += counts.warn;

          auto diff = std::chrono::steady_clock::now() - testStart;

          const auto hrs = std::chrono::duration_cast<std::chrono::hours>(diff);
          const auto mins = std::chrono::duration_cast<std::chrono::minutes>(diff - hrs);
          const auto secs = std::chrono::duration_cast<std::chrono::seconds>(diff - hrs - mins);
          const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(diff - hrs - secs);

          std::cout << "Test Duration: "
                    << std::setfill('0') << std::setw(2) << hrs.count()  << ":"
                    << std::setfill('0') << std::setw(2) << mins.count() << ":"
                    << std::setfill('0') << std::setw(2) << secs.count() << "."
                    << ms.count()   << std::endl;

          if( AT_SUCCESS == err )
          {
            if( counts.warn )
            {
              std::cout << "Completed successfully with warnings!";
              testSummary.push_back( "WARN, " + testDescription.str() );
            }
            else
            {
              std::cout << "Completed succssfully!";
              testSummary.push_back( "PASS, " + testDescription.str() );
            }
          }
          else
          {
            std::cout << "Failed!";
            testSummary.push_back( "FAIL, " + testDescription.str() );
            success = false;
          }
        }
      }
    }
  }

  auto diff = std::chrono::steady_clock::now() - totalTestDurationStart;

  const auto hrs = std::chrono::duration_cast<std::chrono::hours>(diff);
  const auto mins = std::chrono::duration_cast<std::chrono::minutes>(diff - hrs);
  const auto secs = std::chrono::duration_cast<std::chrono::seconds>(diff - hrs - mins);
  const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(diff - hrs - secs);

  std::cout << std::endl << std::endl
            << std::string(100,'=') << std::endl << std::endl;


  std::cout << "SUMMARY REPORT:" << std::endl
            << "Total # Tests Executed: " << totalCounts.tests << std::endl
            << "              # Passed: " << totalCounts.pass << std::endl
            << "    # Passed w/Warning: " << totalCounts.warn << std::endl
            << "            # Failures: " << totalCounts.fail << std::endl
            << "         Total Runtime: "
            << std::setfill('0') << std::setw(2) << hrs.count()  << ":"
            << std::setfill('0') << std::setw(2) << mins.count() << ":"
            << std::setfill('0') << std::setw(2) << secs.count() << "." << ms.count()
            << std::endl;

  for( auto summary : testSummary )
    std::cout << summary << std::endl;

  return (success) ? AT_SUCCESS : !AT_SUCCESS;
}
// End of Run()


//------------------------ setMTAndCheckCalculated() ---------------------------
//
int setMTAndCheckCalculated(
  const CamInfo& camInfo,
  ZylaCalculator& zylaCalc,
  const std::vector<ROI>& ROIs, // ROIs to test
  const std::string& testDescription,
  Counts& counts )
{
  std::vector<AOI> vAOI;

  if( ROIs.size() < 2 )
  {
    std::cout << "ERROR: setMTAndCheckCalculated(ROIs): ROIs size="
              << ROIs.size() << std::endl;
    return !AT_SUCCESS;
  }

  for( int i=0; i<ROIs.size()-1; i+=2 )
  {
    // ROIs are increasing origin order while AOIs need to be in increasing
    // top order. Pairs need to be converted from ROI to AOI while being
    // swapped.
    vAOI.push_back( CamUtil::Roi2Aoi( ROIs[i+1], camInfo.getSensorHeight() ) );
    vAOI.push_back( CamUtil::Roi2Aoi( ROIs[i],   camInfo.getSensorHeight() ) );
  }

  return setMTAndCheckCalculated( camInfo, zylaCalc, vAOI, testDescription, counts );
}
// End of setMTAndCheckCalculated(ROI)


//------------------------ setMTAndCheckCalculated() ---------------------------
//
int setMTAndCheckCalculated(
  const CamInfo& camInfo,
  ZylaCalculator& zylaCalc,
  const std::vector<AOI>& AOIs, // AOIs to test
  const std::string& testDescription,
  Counts& counts )
{
  AT_H H(camInfo.getHandle());                       // For convenience
  AT_64 aoiStride;
  AT_64 expectedImageSizeBytes(0);
  AT_BOOL multitrackBinned = (forceError) ? AT_TRUE : AT_FALSE;
  int testNumber(0);
  double readoutTime[2];      // Calculated and actual ReadoutTime
  int    numRowReads[2];      // Calculated and actual number of row reads
  double let[2];              // Calculated and actual LongExposureTransition
  AT_64  imageSizeBytes[2];   // Calculated and actual ImageSizeBytes
  std::string pathTaken;      // For tracking what path calculateNumRowReads takes

  if( AOIs.size() < 2 )
  {
    std::cout << "ERROR: setMTAndCheckCalculated(AOIs): AOIs size="
              << AOIs.size() << std::endl;
    return !AT_SUCCESS;
  }

  std::cout 
    << testDescription << std::endl
    << "Starting " << AOIs[0] << std::endl
    << "  Ending " << AOIs[AOIs.size()-1] << std::endl << std::endl
    << "     |     Multitrack #1     |      Multitrack #2    |  Calc  | NumRowReads |          Frame Read Time              |   ImageSizeBytes  |"
    << std::endl;
  if( debugEnabled )
    std::cout << "      OrigX,OrigY,SizeX,SizeY,OrigX,OrigY,SizeX,SizeY" << std::endl;
  std::cout
    << "Test , Left, Top , Wdth, Hght, Left, Top , Wdth, Hght,  Path  , Calc, Actual,       Calc.       ,      Actual       ,  Calc.  ,  Actual , Status"
    << std::endl;

  // All AOI definitions have the same width/left value so set them once here.
  RETURN_ON_ERROR( AT_SetEnumString(H, L"AoiLayout", L"Multitrack") );
  RETURN_ON_ERROR( AT_SetInt(H, L"AoiWidth", AOIs[0].width) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AoiLeft",  AOIs[0].left) );
  RETURN_ON_ERROR( AT_GetInt(H, L"AOIStride", &aoiStride) );

  // Reset multi-track
  RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackCount", 1) );
  RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackSelector", 0) );
  RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackStart", 1) );
  RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackEnd",   camInfo.getSensorHeight()) );

  for( int i=0; i<AOIs.size(); i+=2 )
  {
    testNumber++;

    // If startWith > 0 then skip tests until we get to the specified test number
    if( testNumber < startWith )
      continue;

    // If quickCheck > 0 then only run the first 'quickCheck' tests
    if( quickCheck > 0 && (testNumber > (startWith + quickCheck - 1)) )
      break;

    counts.tests++;           // Increment test counter

    // Create vector with the next two AOI definitions. These are the next two
    // multi-tracks to test.
    std::vector<AOI> mt;
    mt.emplace_back(AOIs[i]);
    mt.emplace_back(AOIs[i+1]);
    
    std::ostringstream testStr;
    testStr << std::right << std::setw(5) << testNumber << ", "
            << mt[0].toSimpleString() << ", "
            << mt[1].toSimpleString() << ", ";

    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackCount", 2) );

    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackSelector", 0) );
    RETURN_ON_ERROR( AT_SetBool(H, L"MultitrackBinned", AT_FALSE) );
    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackStart", mt[0].top) );
    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackEnd",   mt[0].top + mt[0].height - 1) );

    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackSelector", 1) );
    RETURN_ON_ERROR( AT_SetBool(H, L"MultitrackBinned", AT_FALSE) );
    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackStart", mt[1].top) );
    RETURN_ON_ERROR( AT_SetInt(H, L"MultitrackEnd",   mt[1].top + mt[1].height - 1) );

    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime[_ACTUAL]) );
    numRowReads[_ACTUAL] = std::round(readoutTime[_ACTUAL] / zylaCalc.getRowReadTime() );
    RETURN_ON_ERROR( AT_GetFloat(H, L"LongExposureTransition", &let[_ACTUAL]));
    RETURN_ON_ERROR( AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes[_ACTUAL]) );

    numRowReads[_CALC] = zylaCalc.calculateNumRowReads( mt, pathTaken);

    readoutTime[_CALC] = zylaCalc.calculateReadoutTime( numRowReads[_CALC] );

    let[_CALC] = zylaCalc.calculateLET( readoutTime[_CALC] );

    imageSizeBytes[_CALC] = zylaCalc.calculateImageSizeBytes( mt, aoiStride );

    testStr << pathTaken
            << ", " << std::setw(4) << numRowReads[_CALC]
            << ", " << std::setw(5) << numRowReads[_ACTUAL] << " "
            << std::fixed << std::setprecision(16)
            << ", " << readoutTime[_CALC]
            << ", " << readoutTime[_ACTUAL]
            << ", " << std::setw(8) << imageSizeBytes[_CALC]
            << ", " << std::setw(8) << imageSizeBytes[_ACTUAL];

    std::ostringstream warnings;  // calculated value > actual, ok
    std::ostringstream whatFailed;
    int maxRowReadsOver(4);
    if( numRowReads[_CALC] != numRowReads[_ACTUAL] )
    {
      // There may be a discrepancy with the calculated number of rowReads.
      // Allow the calculated value to be upto 4 rowReads greater than what
      // is obtained from the camera. This will ultimately result in a max
      // exposure rate that is slower than what the camera can actually do.
      // Once some of the boundary conditions on calcualting the number of
      // row reads is sorted out this cam be removed.
      if( numRowReads[_CALC] > numRowReads[_ACTUAL] &&
          numRowReads[_CALC] - numRowReads[_ACTUAL] <= maxRowReadsOver )
        warnings << "NumRowReads +"
                    << numRowReads[_CALC] - numRowReads[_ACTUAL]
                    << ", max=+" << maxRowReadsOver;
      else
        whatFailed << "NumRowReads ";
    }
    if( readoutTime[_CALC] < readoutTime[_ACTUAL] )
    {
      // Readout time follows number of row reads so we will only note a
      // failure when the calculated < the readout time obtained from the
      // caemra.
      whatFailed << "ReadoutTime ";
    }
    if( imageSizeBytes[_CALC] != imageSizeBytes[_ACTUAL] )
      whatFailed << "ImageSizeBytes, diff="
                 << (imageSizeBytes[_CALC] - imageSizeBytes[_ACTUAL]) << " ";

    if( whatFailed.str().length() == 0 )
    {
      if( warnings.str().length() == 0 )
      {
        testStr << ", PASS";
        counts.pass++;
      }
      else
      {
        testStr << ", WARN: " << warnings.str();
        counts.warn++;
      }
    }
    else
    {
      testStr << ", FAIL: " << whatFailed.str();
      counts.fail++;
    }

    std::cout << testStr.str() << std::endl;

    if( debugEnabled )
      std::cout << std::string(100,'-') << std::endl;

  }

  if( counts.fail )
    return !AT_SUCCESS;
  else
    return AT_SUCCESS;
}
// End of setMTAndCheckCalculated()


//----------------------- setAoiAndCheckCalculated() ---------------------------
//
int setAoiAndCheckCalculated( 
  const CamInfo& camInfo,
  ZylaCalculator& zylaCalc,
  const std::vector<AOI>& AOIs, // AOIs to test
  const std::string& testDescription,
  Counts& counts )
{
  AT_H H(camInfo.getHandle());                       // For convenience
  AT_64 aoiStride;
  AT_BOOL multitrackBinned = (forceError) ? AT_TRUE : AT_FALSE;
  int testNumber(0);
  double readoutTime[2];      // Calculated and actual ReadoutTime
  int    numRowReads[2];      // Calculated and actual number of row reads
  double let[2];              // Calculated and actual LongExposureTransition
  AT_64  imageSizeBytes[2];   // Calculated and actual ImageSizeBytes
  std::string pathTaken;      // For tracking what path calculateNumRowReads takes


  std::cout 
    << testDescription << std::endl
    << "Starting " << AOIs[0] << std::endl
    << "  Ending " << AOIs[AOIs.size()-1] << std::endl << std::endl
    << "                             |  Calc  | NumRowReads |          Frame Read Time              |   ImageSizeBytes  |"
    << std::endl;
  if( debugEnabled )
    std::cout << "      OrigX,OrigY,SizeX,SizeY" << std::endl;
  std::cout
    << " Test, Left, Top , Wdth, Hght,  Path  , Calc, Actual,       Calc.       ,      Actual       ,  Calc.  ,  Actual , Status"
    << std::endl;

  // All AOI definitions have the same width/left value so set them once here.
  RETURN_ON_ERROR( AT_SetEnumString(H, L"AoiLayout", L"Image") );
  RETURN_ON_ERROR( AT_SetInt(H, L"AoiWidth", AOIs[0].width) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AoiLeft",  AOIs[0].left) );
  RETURN_ON_ERROR( AT_GetInt(H, L"AOIStride", &aoiStride) );

  for( AOI aoi: AOIs )
  {
    testNumber++;

    // If startWith > 0 then skip tests until we get to the specified test number
    if( testNumber < startWith )
      continue;

    // If quickCheck > 0 then only run the first 'quickCheck' tests
    if( quickCheck > 0 && (testNumber > (startWith + quickCheck - 1)) )
      break;

    counts.tests++;           // Increment test counter

    std::ostringstream testStr;
    testStr << std::right << std::setw(5) << testNumber << ", "
            << aoi.toSimpleString() << ", ";
    
    RETURN_ON_ERROR( AT_SetInt(H, L"AoiHeight", aoi.height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AoiTop",    aoi.top) );

    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime[_ACTUAL]) );
    numRowReads[_ACTUAL] = std::round(readoutTime[_ACTUAL] / zylaCalc.getRowReadTime() );
    RETURN_ON_ERROR( AT_GetFloat(H, L"LongExposureTransition", &let[_ACTUAL]));
    RETURN_ON_ERROR( AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes[_ACTUAL]) );

    numRowReads[_CALC] = zylaCalc.calculateNumRowReads( aoi, pathTaken);

    readoutTime[_CALC] = zylaCalc.calculateReadoutTime( numRowReads[_CALC] );

    let[_CALC] = zylaCalc.calculateLET( readoutTime[_CALC] );

    imageSizeBytes[_CALC] = zylaCalc.calculateImageSizeBytes( aoi, aoiStride );

    testStr << pathTaken
            << ", " << std::setw(4) << numRowReads[_CALC]
            << ", " << std::setw(5) << numRowReads[_ACTUAL] << " "
            << std::fixed << std::setprecision(16)
            << ", " << readoutTime[_CALC]
            << ", " << readoutTime[_ACTUAL]
            << ", " << std::setw(8) << imageSizeBytes[_CALC]
            << ", " << std::setw(8) << imageSizeBytes[_ACTUAL];

    std::ostringstream warnings;  // calculated value > actual, ok
    std::ostringstream whatFailed;
    int maxRowReadsOver(4);
    if( numRowReads[_CALC] != numRowReads[_ACTUAL] )
    {
      // There may be a discrepancy with the calculated number of rowReads.
      // Allow the calculated value to be upto 4 rowReads greater than what
      // is obtained from the camera. This will ultimately result in a max
      // exposure rate that is slower than what the camera can actually do.
      // Once some of the boundary conditions on calcualting the number of
      // row reads is sorted out this cam be removed.
      if( numRowReads[_CALC] > numRowReads[_ACTUAL] &&
          numRowReads[_CALC] - numRowReads[_ACTUAL] <= maxRowReadsOver )
        warnings << "NumRowReads +"
                    << numRowReads[_CALC] - numRowReads[_ACTUAL]
                    << ", max=+" << maxRowReadsOver;
      else
        whatFailed << "NumRowReads ";
    }
    if( readoutTime[_CALC] < readoutTime[_ACTUAL] )
    {
      // Readout time follows number of row reads so we will only note a
      // failure when the calculated < the readout time obtained from the
      // caemra.
      whatFailed << "ReadoutTime ";
    }
    if( imageSizeBytes[_CALC] != imageSizeBytes[_ACTUAL] )
      whatFailed << "ImageSizeBytes, diff="
                 << (imageSizeBytes[_CALC] - imageSizeBytes[_ACTUAL]) << " ";

    if( whatFailed.str().length() == 0 )
    {
      if( warnings.str().length() == 0 )
      {
        testStr << ", PASS";
        counts.pass++;
      }
      else
      {
        testStr << ", WARN: " << warnings.str();
        counts.warn++;
      }
    }
    else
    {
      testStr << ", FAIL: " << whatFailed.str();
      counts.fail++;
    }

    std::cout << testStr.str() << std::endl;

    if( debugEnabled )
      std::cout << std::string(100,'-') << std::endl;

  }

  if( counts.fail )
    return !AT_SUCCESS;
  else
    return AT_SUCCESS;
}
// End of setAoiAndCheckCalculated()


//----------------------------- AcquireFrames() --------------------------------
//
int AcquireFrames(AT_H H, int frameCount, AT_64 bytesPerFrame) {
  std::vector<image_t> images;

  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[bytesPerFrame]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(bytesPerFrame));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount,bytesPerFrame);

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));
  return err;
}
// End of AcquireFrames()


//---------------------------- AcquisitionLoop() -------------------------------
//
int AcquisitionLoop(AT_H H, int frameCount, AT_64 bytesPerFrame) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;

      if( image_size != bytesPerFrame )
      {
        std::cout << "FAILURE: AT_WaitBuffer - "
                  << "Expected imageSize=" << bytesPerFrame
                  << ", Actual imageSize=" << image_size << std::endl;
        return !AT_SUCCESS;
      }

      if(1==frame) {
        std::cout << "Got first frame" << std::endl;
      }

      if((frame % UPDATEINTERVALFRAMES == 0) && printFrameData) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
        std::cout << "Acquisition of " << frame << " frames took " << duration 
                  << "s, effective frame-rate " << frameRate << "fps" 
                  << std::endl;

        CamUtil::PrintMono16Frame(
          frame, image_data, image_size, static_cast<int>(aoiStride),
          pixelsPerRowToPrint, rowsToPrint);
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::cout << "AT_WaitBuffer when waiting for frame "<< (frame+1) 
                << " return code: " << err << std::endl<<std::endl;
      return err;
    }
  }
  std::cout << std::endl;
  return AT_SUCCESS;
}
// End of AcquisitionLoop()


