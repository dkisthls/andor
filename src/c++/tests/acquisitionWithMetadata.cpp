
#include "atcore.h"
#include "AndorMetadata.h"
#include "CamUtil.h"
#include "constants.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>

//static const int BUFFERCOUNT=10;
static const int FRAMESTOACQUIRE = 10;
//static const int TIMEOUTMS = 10000;
//static const int UPDATEINTERVALFRAMES=1;
static const int ROWSTOPRINT=5;
static const int PIXELSPERROWTOPRINT=10;
//static const int PAUSEAFTER = false;

static const AT_WC* TRIGGERMODE = L"Internal";

typedef std::unique_ptr<AT_U8[]> image_t;

AT_64 timestampClockFrequency;
double timestampClockPeriod;
std::uint64_t prevMetadataTimestamp(0);

int Run();

int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);

int main()
{
  int err = Run();
  if(AT_SUCCESS==err) {
    std::wcout<<L"Completed sucessfully!"<<std::endl;
  }
  else {
    std::wcout<<L"Failed!"<<std::endl;
  }
  
  if (PAUSEAFTER) {
    std::wcout << L"Press enter to exit ..." << std::endl;
    getchar();
  }
  return err;
}

int Run() {
  int err = AT_SUCCESS;
  auto timenow =
    std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  std::cout << "Date: " << std::ctime(&timenow);

  CamInfo camInfo;
  if( AT_SUCCESS == CamUtil::OpenAndIdentify( camInfo ) )
  {
    AT_H H = camInfo.getHandle();

    CamUtil::WaitForSensorInitialisation(H);

    CamUtil::PrintAndSetEnumFeature(H,L"TriggerMode",TRIGGERMODE);
    CamUtil::PrintAndSetEnumFeature(H,L"CycleMode",L"Fixed");
    CamUtil::PrintAndSetIntFeature(H,L"FrameCount", FRAMESTOACQUIRE);
    CamUtil::PrintAndSetBoolFeature(H,L"MetadataEnable",AT_TRUE);
    CamUtil::PrintAndSetBoolFeature(H,L"MetadataFrameInfo",AT_TRUE);
    CamUtil::PrintAndSetBoolFeature(H,L"MetadataTimestamp",AT_TRUE);
    CamUtil::PrintIntFeature(H,L"ImageSizeBytes");
    CamUtil::PrintIntFeature(H,L"AOIStride");
    RETURN_ON_ERROR(AT_GetInt(H,L"TimestampClockFrequency",&timestampClockFrequency));
    std::wcout << L"TimestampClockFrequency: " << timestampClockFrequency << std::endl;
    timestampClockPeriod = 1.0/static_cast<double>(timestampClockFrequency);
    std::wcout.precision(15);
    std::wcout << L"TimestampClockPeriod: " << timestampClockPeriod << std::endl;

    err = AcquireFrames(H, FRAMESTOACQUIRE);

    WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
    WARN_ON_ERROR(AT_FinaliseLibrary());
  }

  return err;
}


int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::wcout << L"ImageSizeBytes: " << imageSizeBytes <<L"B"<< std::endl;

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }
  std::wcout << std::endl;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));
  return err;
}


int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;
  
        AT_BOOL metadataEnabled;
        RETURN_ON_ERROR(AT_GetBool(H,L"MetadataEnable",&metadataEnabled));
        if( metadataEnabled )
        {
          Metadata metadata = AndorMetadata::parse(image_data, image_size, true);
          if( prevMetadataTimestamp != 0 )
          {
            std::uint64_t diff = metadata.timestamp - prevMetadataTimestamp;
            prevMetadataTimestamp = metadata.timestamp;
            double diffInSec = static_cast<double>(diff) / static_cast<double>(timestampClockFrequency);
            std::wcout.precision(12);
            std::wcout << L"dt=" << std::fixed << diffInSec << std::endl;
          }
          else
            prevMetadataTimestamp = metadata.timestamp;

          CamUtil::PrintMono16Frame(frame, image_data, image_size, metadata.frameInfo.field.stride, PIXELSPERROWTOPRINT,ROWSTOPRINT);
        }
        else
          CamUtil::PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}

