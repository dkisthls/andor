
#include "atcore.h"

#include "CmdLineUtil.h"
#include "CamUtil.h"
#include "constants.h"
#include "MasterListOfFeatures.h"
#include "ReadWriteReadOnly.h"
#include "Timer.h"

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <thread>
#include <vector>

typedef std::unique_ptr<AT_U8[]> image_t;

int Run( const CamInfo& camInfo, const bool& printErrOnlyEnb );

void usage(char *progname);
bool executeTests(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode );

bool doTestReadWriteReadOnly(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& outputPrefix,
  const std::string& feature,
  const R_W_RO& expected );

bool testReadWriteReadOnlyStopped(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& when,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode );

bool testReadWriteReadOnlyRunning(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string when,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode );

int PassNumber;      // 1: Query readable/writable/readonly after AcquisitionStart and after AcquistionStop
                     // 2: Query readable/writable/readonly before and after AcquistionStart and after AcquisitionStop

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
       << "Tests the IsReadable, IsWritable, and IsReadOnly flags for each\n"
       << "feature of the camera.\n"
       << std::endl
       << "Usage: " << progname << " [options]\n"
       << "     --help\n"
       << "       This usage help\n"
       << std::endl
       << "     --printErrorsOnly\n"
       << "       Reduces output to only print r/w/ro tests that fail.\n"
       << std::endl;
}
// End of usage()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  bool success(false);
  bool printErrOnlyEnb(false);   // Enabled - only print tests that fail
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "printErrorsOnly" )
        {
          printErrOnlyEnb = true;
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          CmdLineUtil::cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( CommandLineError& err ) {
    errMsg << err.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  }

  auto timenow =
    std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::cout << "Date: " << std::ctime(&timenow);

  CamInfo camInfo;
  if( AT_SUCCESS == CamUtil::OpenAndIdentify( camInfo ) )
  {
    MasterListOfFeatures masterListOfFeatures;

    // std::cout << "*********************************\n";
    // for(std::string feature : allFeatures )
    //   CamUtil::PrintIsImplemented(
    //     camInfo.getHandle(), CamUtil::string2wcs(feature).c_str() );
    // std::cout << "*********************************\n";

    if( AT_SUCCESS == CamUtil::SetBasicConfiguration( camInfo ) )
    {
      auto start = std::chrono::steady_clock::now();

      int sts = Run( camInfo, printErrOnlyEnb );
      success = ((AT_SUCCESS == sts) ? true : false);

      auto end = std::chrono::steady_clock::now();
      auto duration = std::chrono::duration<double>(end-start);

      std::cout.precision(2);
      std::cout << std::endl
                << "Overall Status: " << ((success) ? "SUCCESS" : "FAILURE")
                << ", total runtime=" << std::fixed << duration.count() << "s"
                << std::endl;
    }

    WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
    WARN_ON_ERROR(AT_FinaliseLibrary());
  }


  return (success ? 0 : 1);
}
// End of main()


//---------------------------------- Run() -------------------------------------
// camInfo - Contains camera information
// printErrOnlyEnb, -,  If true, only output tests that fail
int Run( const CamInfo& camInfo, const bool& printErrOnlyEnb )
{
  bool overallSuccess(true);
  int err(AT_SUCCESS);
  int sts;                       // for SDK call retur
  AT_H H(camInfo.getHandle());

  for( PassNumber=1; PassNumber<=1; PassNumber++ )
  {
    std::cout << std::endl << std::string(100,'=') << std::endl;

    if( PassNumber==1 )
      std::cout << "PASS #1: Query readable/writable/readonly after AcquisitionStart and after AcquisitionStop"
                << std::endl;
    else if( PassNumber==2 )
      std::cout << "PASS #2: Query readable/writable/readonly before and after AcquisitionStart and after AcquisitionStop"
                << std::endl;
    else {
      std::cout << "DON'T KNOW HOW TO HANDLE TEST #" << PassNumber << " - ABORTING!" << std::endl;
      return false;
    }

    int testNumber(0);
    for( std::string triggerMode : camInfo.getAllTriggerModes() )
    {
      for( std::string shutterMode : camInfo.getShutterModes() )
      {
        for( std::string gainMode : camInfo.getGainModes() )
        {
          if( camInfo.getCameraType() == ::CameraType::BALOR &&
              shutterMode == "Global" &&
              gainMode == "Medium Gain (12-bit)" )
          {
            // Global shutter w/"Medium Gain (12-bit)" gain mode is not supported
            continue;
          }

          testNumber++;
          std::cout << std::endl << std::string(100,'=') << std::endl
                    << "PASS #" << PassNumber << ", TEST #" << testNumber
                    << ": TriggerMode=\"" << triggerMode << "\""
                    << ", ShutterMode=\"" << shutterMode << "\""
                    << ", GainMode=\"" << gainMode << "\""
                    << std::endl;

          if( CamUtil::PrintAndSetEnumFeature( H, L"ElectronicShutteringMode",
            CamUtil::string2wcs(shutterMode).c_str()) != AT_SUCCESS )
          {
            overallSuccess &= false;
          }

          std::wstring gainFeature;
          if( camInfo.getCameraType() == ::CameraType::BALOR )
            gainFeature = L"GainMode";
          else
            gainFeature = L"SimplePreAmpGainControl";

          if( CamUtil::PrintAndSetEnumFeature( H,
            gainFeature.c_str(), CamUtil::string2wcs(gainMode).c_str() ) )
          {
            overallSuccess &= false;
          }

          if( CamUtil::PrintAndSetEnumFeature(H, L"TriggerMode",
            CamUtil::string2wcs(triggerMode).c_str()) )
          {
            overallSuccess &= false;
          }

          auto start = std::chrono::steady_clock::now();

          bool testSuccess = executeTests(
            camInfo, printErrOnlyEnb, shutterMode, triggerMode, gainMode );

          auto end = std::chrono::steady_clock::now();
          auto duration = std::chrono::duration<double>(end - start);

          std::cout.precision(2);
          std::cout << "Pass #" << PassNumber << ", Test #" << testNumber
                    << ": " << ((testSuccess) ? "SUCCESS" : "FAILURE")
                    << ", test duration=" << std::fixed << duration.count() << "s"
                    <<std::endl;

          overallSuccess &= testSuccess;
        }
      }
    }
  }

  return (overallSuccess ? AT_SUCCESS : !AT_SUCCESS);
}
// End of Run()


//----------------------------- executeTests() ---------------------------------
//
bool executeTests(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode )
{
  bool success(true);
  std::string when;                // Identifies when check is performed
  std::wstring featureUnderTest;
  AT_H H(camInfo.getHandle());
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::cout << "AT_QueueBuffer return code" << err << std::endl;
    }

    images.push_back(move(image));
  }

  if( PassNumber == 2 )
  {
    when = "Before AcquisitionStart";
    success &= testReadWriteReadOnlyStopped(
      camInfo, printErrOnlyEnb, when, shutterMode, triggerMode, gainMode );
    if( !printErrOnlyEnb || (printErrOnlyEnb && !success) )
      std::cout << std::endl;
  }

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  when = "After AcquisitionStart";
  success &= testReadWriteReadOnlyRunning(
    camInfo, printErrOnlyEnb, when, shutterMode, triggerMode, gainMode );
  if( !printErrOnlyEnb || (printErrOnlyEnb && !success) )
    std::cout << std::endl;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  when = "After AcquisitionStop";
  success &= testReadWriteReadOnlyStopped(
    camInfo, printErrOnlyEnb, when, shutterMode, triggerMode, gainMode );
  
  return success;
}
// End of executeTests()


//------------------------ doTestReadWriteReadOnly() ----------------------------
//
bool doTestReadWriteReadOnly(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& outputPrefix,
  const std::string& feature,
  const R_W_RO& expected )
{
  int failcount(0);
  AT_H H(camInfo.getHandle());
  AT_BOOL isReadable;
  AT_BOOL isWritable;
  AT_BOOL isReadOnly;
  std::ostringstream testMsg;

  testMsg << outputPrefix << "Testing readable"
          << "(" << expected.readable << ")/writable(" << expected.writable
          << ")/readonly(" << expected.readonly << ")...";

  std::wstring wname( CamUtil::string2wcs(feature) );
  WARN_ON_ERROR( AT_IsReadable(H, wname.c_str(), &isReadable) );
  WARN_ON_ERROR( AT_IsWritable(H, wname.c_str(), &isWritable) );
  WARN_ON_ERROR( AT_IsReadOnly(H, wname.c_str(), &isReadOnly) );
  if( isReadable != expected.readable ||
      isWritable != expected.writable ||
      isReadOnly != expected.readonly )
  {
    testMsg << "FAILURE: ";
    if( isReadable != expected.readable )
    {
      testMsg  << "isReadable: " << isReadable;
      failcount++;
    }
    if( isWritable != expected.writable )
    {
      if( failcount > 0 ) testMsg << ", ";
      testMsg << "isWritable: " << isWritable;
      failcount++;
    }
    if( isReadOnly != expected.readonly )
    {
      if( failcount > 0 ) testMsg << ", ";
      testMsg << "isReadOnly: " << isReadOnly;
      failcount++;
    }
  }
  else
  {
    testMsg << "OK!";
  }

  if( !printErrOnlyEnb || (printErrOnlyEnb && failcount > 0) )
    std::cout << testMsg.str() << std::endl;

  return (failcount == 0) ? true : false;
}
// End of doTestReadWriteReadOnly()


//--------------------- testReadWriteReadOnlyStopped() -------------------------
//
bool testReadWriteReadOnlyStopped(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string& when,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode )
{
  bool success(true);
  AT_H H(camInfo.getHandle());

  // This vector contains all feature names being tested for both the
  // Balor and the Zyla. Not all features are implemented on both cameras.
  // The method AT_IsImplemented() will be used on each feature to determine
  // whether it is implemented on the camera being tested. If it is, then
  // the feature will be checked. If it is not implemented then it will be
  // skipped.
  std::map<std::string, R_W_RO> allFeatures {
    {"AccumulateCount", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"AcquisitionStart", {AT_TRUE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"AcquisitionStop", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"AlternatingReadoutDirection", {AT_TRUE, AT_TRUE, AT_FALSE}},    //   -  , Zyla
    {"AOIBinning", {AT_TRUE, AT_TRUE, AT_FALSE}},                     // Balor, Zyla
    {"AOIHBin", {AT_TRUE, AT_TRUE, AT_FALSE}},                        // Balor, Zyla
    {"AOIHeight", {AT_TRUE, AT_TRUE, AT_FALSE}},                      // Balor, Zyla
    {"AOILayout", {AT_TRUE, AT_TRUE, AT_FALSE}},                      // Balor, Zyla
    {"AOILeft", {AT_TRUE, AT_TRUE, AT_FALSE}},                        // Balor, Zyla
    {"AOIStride", {AT_TRUE, AT_FALSE, AT_TRUE}},                      // Balor, Zyla
    {"AOITop", {AT_TRUE, AT_TRUE, AT_FALSE}},                         // Balor, Zyla
    {"AOIVBin", {AT_TRUE, AT_TRUE, AT_FALSE}},                        // Balor, Zyla
    {"AOIWidth", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"AuxiliaryOutSource", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"AuxOutSourceTwo", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"Baseline", {AT_TRUE, AT_FALSE, AT_TRUE}},                       // Balor, Zyla
    {"BaselineClamp", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor, Zyla
    {"BiasLevelEnable", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor  -     Engineering
    {"BitDepth", {AT_TRUE, AT_FALSE, AT_TRUE}},                       // Balor, Zyla
    {"BufferOverflowEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},            // Balor, Zyla  BOTH REPORT NI
    {"BytesPerPixel", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"CameraAcquiring", {AT_TRUE, AT_FALSE, AT_TRUE}},                // Balor, Zyla
    {"CameraBuildVersion", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor, Zyla
    {"CameraFamily", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"CameraInformation", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor,  -
    {"CameraModel", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"CameraName", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"CameraPresent", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"CameraStatus", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor,  -   <-- Rev dependent, see below
    {"ControllerID", {AT_TRUE, AT_FALSE, AT_TRUE}},                   //   -  , Zyla
    {"CorrectionApplying", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor,  -
    {"CXPEventsSupported", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor,  -
    {"CycleMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                      // Balor, Zyla
    {"DeviceVideoIndex", {AT_TRUE, AT_FALSE, AT_TRUE}},               //   -  , Zyla
    {"DirectQueueing", {AT_TRUE, AT_TRUE, AT_FALSE}},                 // Balor, Zyla
    {"ElectronicShutteringMode", {AT_TRUE, AT_TRUE, AT_FALSE}},       // Balor, Zyla
    {"EventEnable", {AT_TRUE, AT_TRUE, AT_FALSE}},                    // Balor, Zyla
    {"EventSelector", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor, Zyla
    {"EventsMissedEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},              // Balor, Zyla
    {"ExposedPixelHeight", {AT_FALSE, AT_FALSE, AT_FALSE}},           //   -  , Zyla <-- Shutter/Trigger Mode Dependent, see below
    {"ExposureEndEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"ExposureStartEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"ExposureTime", {AT_FALSE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"ExternalTriggerDelay", {AT_FALSE, AT_FALSE, AT_FALSE}},         // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"FanSpeed", {AT_TRUE, AT_TRUE, AT_FALSE}},                       //   -  , Zyla
    {"FanSpeedRPM", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor,  -
    {"FastAOIFrameRateEnable", {AT_TRUE, AT_TRUE, AT_FALSE}},         //   -  , Zyla
    {"FirmwareVersion", {AT_TRUE, AT_FALSE, AT_TRUE}},                // Balor, Zyla
    {"FrameCount", {AT_TRUE, AT_TRUE, AT_FALSE}},                     // Balor, Zyla
    {"FrameGenFixedPixelValue", {AT_TRUE, AT_TRUE, AT_FALSE}},        // Balor,  -
    {"FrameGenMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                   // Balor,  -
    {"FrameRate", {AT_FALSE, AT_FALSE, AT_FALSE}},                    // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"FullAOIControl", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"GainCorrection", {AT_TRUE, AT_TRUE, AT_FALSE}},                 // Balor, Zyla
    {"GainMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"HDRTestMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                    // Balor,  -
    {"ImageSizeBytes", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"InterfaceType", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"IOInvert", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"IOSelector", {AT_TRUE, AT_TRUE, AT_FALSE}},                     // Balor, Zyla
    {"IRIGClockFrequency", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor,  -
    {"LinearityOffsetCorrection", {AT_TRUE, AT_TRUE, AT_FALSE}},      // Balor,  -
    {"LineScanSpeed", {AT_TRUE, AT_FALSE, AT_TRUE}},                  //   -  , Zyla
    {"LogLevel", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"LongExposureTransition", {AT_TRUE, AT_FALSE, AT_TRUE}},         // Balor, Zyla
    {"MaxInterfaceTransferRate", {AT_TRUE, AT_FALSE, AT_TRUE}},       // Balor, Zyla
    {"MetadataEnable", {AT_TRUE, AT_TRUE, AT_FALSE}},                 // Balor, Zyla
    {"MetadataFrame", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"MetadataFrameInfo", {AT_TRUE, AT_TRUE, AT_FALSE}},              // Balor,  -
    {"MetadataIrig", {AT_TRUE, AT_TRUE, AT_FALSE}},                   // Balor,  -
    {"MetadataSize", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor,  -
    {"MetadataTimestamp", {AT_TRUE, AT_TRUE, AT_FALSE}},              // Balor, Zyla
    {"MicrocodeVersion", {AT_TRUE, AT_TRUE, AT_FALSE}},               //   -  , Zyla
    {"MultitrackBinned", {AT_TRUE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"MultitrackCount", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"MultitrackEnd", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor, Zyla
    {"MultitrackSelector", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"MultitrackStart", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"Overlap", {AT_FALSE, AT_FALSE, AT_FALSE}},                      //   -  , Zyla <-- Shutter/Trigger Mode Dependent, see below
    {"PixelEncoding", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor, Zyla
    {"PixelHeight", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"PixelProcCorrection", {AT_TRUE, AT_TRUE, AT_FALSE}},            // Balor,  -
    {"PixelReadoutRate", {AT_TRUE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"PixelWidth", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"ReadTemperatureChannel", {AT_TRUE, AT_TRUE, AT_FALSE}},         // Balor,  -    Engineering
    {"ReadTemperatureValue", {AT_TRUE, AT_TRUE, AT_FALSE}},           // Balor,  -    Engineering (WRITABLE SHOULD BE FALSE)
    {"ReadoutTime", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"ResetToDefaultValue", {AT_TRUE, AT_FALSE, AT_TRUE}},            // Balor,  -
    {"RollingShutterGlobalClear", {AT_TRUE, AT_TRUE, AT_FALSE}},      //   -  , Zyla
    {"RowNExposureEndEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},           // Balor, Zyla
    {"RowNExposureStartEvent", {AT_TRUE, AT_TRUE, AT_FALSE}},         // Balor, Zyla
    {"RowReadTime", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"ScanSpeedControlEnable", {AT_TRUE, AT_TRUE, AT_FALSE}},         //   -  , Zyla
    {"SensorCooling", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor, Zyla
    {"SensorHeight", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"SensorInitialised", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor,  -
    {"SensorReadoutMode", {AT_TRUE, AT_TRUE, AT_FALSE}},              //   -  , Zyla
    {"SensorTemperature", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor, Zyla
    {"SensorType", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"SensorWidth", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"SerialNumber", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"ShutterMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                    // Balor, Zyla
    {"ShutterOutputMode", {AT_TRUE, AT_TRUE, AT_FALSE}},              //   -  , Zyla
    {"ShutterTransferTime", {AT_FALSE, AT_FALSE, AT_FALSE}},          // Balor, Zyla <-- Cam/Shutter/Trigger Mode Dependent, see below
    {"SimplePreAmpGainControl", {AT_TRUE, AT_TRUE, AT_FALSE}},        // Balor, Zyla
    {"SoftwareTrigger", {AT_FALSE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"SpuriousNoiseFilter", {AT_TRUE, AT_TRUE, AT_FALSE}},            // Balor, Zyla
    {"StaticBlemishCorrection", {AT_TRUE, AT_TRUE, AT_FALSE}},        // Balor, Zyla
    {"TargetSensorTemperature", {AT_TRUE, AT_TRUE, AT_FALSE}},        // Balor, Zyla
    {"TemperatureControl", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"TemperatureStatus", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor, Zyla
    {"TECCurrent", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor,  -    Engineering
    {"TECVoltage", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor,  -    Engineering
    {"TimestampClock", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"TimestampClockFrequency", {AT_TRUE, AT_FALSE, AT_TRUE}},        // Balor, Zyla
    {"TimestampClockReset", {AT_FALSE, AT_TRUE, AT_FALSE}},           // Balor, Zyla
    {"TriggerMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                    // Balor, Zyla
    {"TriggerSource", {AT_TRUE, AT_TRUE, AT_FALSE}},                  // Balor,  -
    {"VerticallyCentreAOI", {AT_TRUE, AT_TRUE, AT_FALSE}}             // Balor, Zyla
  };

  // Add Balor/Zyla/Shutter/Trigger mode dependent features
  if( camInfo.getCameraType() == ::CameraType::BALOR )
  {
    // CameraStatus should be r/w/ro=1/1/0 and will be fixed with Rev C Balor.
    // NOTE: When Rev C Balor is released, delete 'CameraStatus' here!
    allFeatures["CameraStatus"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

    // Balor restricts this feature to only be writable while in “Auto” ShutterMode
    // and TriggerMode is not “External Exposure” and not during acquisitions.
    // We aren't using this feature nor "ShutterMode" so we should expect
    // writable to always be false.
    allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    // In "Medium Gain (12-bit)" gain mode, HDRTestMode (High Dynamic Range),
    // is not avaialable since only one gain stage is being used.
    // Overwrite so that writable is false.
    if( gainMode == "Medium Gain (12-bit)" )
      allFeatures["HDRTestMode"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    // Events are not really supported under Linux and what the camera is
    // reported with the 1.0.3.12 build differs from what the Zyla reports.
    // To avoid any confusion regarding Event related features, they will be
    // excluded from the test.
    allFeatures.erase( allFeatures.find( "BufferOverflowEvent" ) );
    allFeatures.erase( allFeatures.find( "CXPEventsSupported" ) );
    allFeatures.erase( allFeatures.find( "EventEnable" ) );
    allFeatures.erase( allFeatures.find( "EventSelector" ) );
    allFeatures.erase( allFeatures.find( "EventsMissedEvent" ) );
    allFeatures.erase( allFeatures.find( "ExposureEndEvent" ) );
    allFeatures.erase( allFeatures.find( "ExposureStartEvent" ) );
    allFeatures.erase( allFeatures.find( "RowNExposureEndEvent" ) );
    allFeatures.erase( allFeatures.find( "RowNExposureStartEvent" ) );
  }
  else  // camInfo.getCameraType() == ::CameraType::ZYLA
  {
    // Zyla 'ExposedPixelHeight' has both a shutter and trigger mode dependancy.
    // We'll handle this feature here.
    if( shutterMode == "Global" )
      allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
    else if( shutterMode == "Rolling" )
    {
      if( triggerMode != "External Exposure" )
        allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
      else
        allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
    }

    if( shutterMode == "Global" )
    {
      // Overlap available for all Trigger Modes
      allFeatures["Overlap"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
    }
    else if( shutterMode == "Rolling" )
    {
      // Overlap differs depending on Trigger Mode
      if( triggerMode == "Software" || triggerMode == "External" )
        allFeatures["Overlap"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
      else  // triggerMode == "Internal", "External Exposure", "External Start"
        allFeatures["Overlap"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
    }

    // Zyla 'ShutterTransferTime' is not writable when TriggerMode='External Exposure'
    if( triggerMode != "External Exposure" )
      allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
    else
      allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
  }

  //======================== TRIGGER MODE DEPENDENT ============================
  // Common between Balor and Zyla but differ depending on trigger mode

  if( triggerMode == "External Exposure" )
    allFeatures["ExposureTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
  else  // triggerMode == "Internal", "Software", "External", "External Start"
    allFeatures["ExposureTime"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

  if( triggerMode == "Internal" || triggerMode == "External Exposure" )
    allFeatures["ExternalTriggerDelay"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
  else  // triggerMode == "Software", "External", "External Start"
    allFeatures["ExternalTriggerDelay"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

  if( triggerMode == "Internal" || triggerMode == "External Start" )
    allFeatures["FrameRate"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE);
  else  // triggerMode == "Software", "External", "External Exposure"
    allFeatures["FrameRate"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

  //
  //=================== END OF TRIGGER MODE DEPENDENT =========================

  // Find feature with longest name length. It is needed for output formatting.
  int max=0;
  std::map<std::string, R_W_RO>::iterator it;
  for( it=allFeatures.begin(); it!=allFeatures.end(); ++it)
    if( it->first.size() > max ) max = it->first.size();

  // Test r/w/ro for all features contained in 'allFeatures'...
  for( it=allFeatures.begin(); it!=allFeatures.end(); ++it)
  {
    std::string feature = it->first;

    int diff = max - feature.size();
    std::string dots(diff+3, '.');

    AT_BOOL isImplemented;
    WARN_ON_ERROR( AT_IsImplemented(H,
      CamUtil::string2wcs(feature).c_str(), &isImplemented) );

    if( isImplemented == AT_TRUE )
    {
      std::ostringstream prefix;
      prefix << when << ", " << feature << dots;
      success &= doTestReadWriteReadOnly(
        camInfo, printErrOnlyEnb, prefix.str(), feature, it->second );
    }
  }

  return success;
}
// End of testReadWriteReadOnlyStopped()


//--------------------- testReadWriteReadOnlyRunning() -------------------------
//
bool testReadWriteReadOnlyRunning(
  const CamInfo& camInfo,
  const bool& printErrOnlyEnb,
  const std::string when,
  const std::string& shutterMode,
  const std::string& triggerMode,
  const std::string& gainMode )
{
  bool success(true);
  AT_H H(camInfo.getHandle());

  // This vector contains all feature names being tested for both the
  // Balor and the Zyla. Not all features are implemented on both cameras.
  // The method AT_IsImplemented() will be used on each feature to determine
  // whether it is implemented on the camera being tested. If it is, then
  // the feature will be checked. If it is not implemented then it will be
  // skipped.
  std::map<std::string, R_W_RO> allFeatures {
    {"AccumulateCount", {AT_TRUE, AT_FALSE, AT_FALSE}},               // Balor, Zyla
    {"AcquisitionStart", {AT_TRUE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"AcquisitionStop", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"AlternatingReadoutDirection", {AT_TRUE, AT_TRUE, AT_FALSE}},    //   -  , Zyla
    {"AOIBinning", {AT_TRUE, AT_FALSE, AT_FALSE}},                    // Balor, Zyla
    {"AOIHBin", {AT_TRUE, AT_FALSE, AT_FALSE}},                       // Balor, Zyla
    {"AOIHeight", {AT_TRUE, AT_FALSE, AT_FALSE}},                     // Balor, Zyla
    {"AOILayout", {AT_TRUE, AT_FALSE, AT_FALSE}},                     // Balor, Zyla
    {"AOILeft", {AT_TRUE, AT_FALSE, AT_FALSE}},                       // Balor, Zyla
    {"AOIStride", {AT_TRUE, AT_FALSE, AT_TRUE}},                      // Balor, Zyla
    {"AOITop", {AT_TRUE, AT_FALSE, AT_FALSE}},                        // Balor, Zyla
    {"AOIVBin", {AT_TRUE, AT_FALSE, AT_FALSE}},                       // Balor, Zyla
    {"AOIWidth", {AT_TRUE, AT_FALSE, AT_FALSE}},                      // Balor, Zyla
    {"AuxiliaryOutSource", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"AuxOutSourceTwo", {AT_TRUE, AT_TRUE, AT_FALSE}},                // Balor, Zyla
    {"Baseline", {AT_TRUE, AT_FALSE, AT_TRUE}},                       // Balor, Zyla
    {"BaselineClamp", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla
    {"BiasLevelEnable", {AT_TRUE, AT_FALSE, AT_FALSE}},               // Balor,  -    Engineering
    {"BitDepth", {AT_TRUE, AT_FALSE, AT_TRUE}},                       // Balor, Zyla
    {"BufferOverflowEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},           // Balor, Zyla
    {"BytesPerPixel", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"CameraAcquiring", {AT_TRUE, AT_FALSE, AT_TRUE}},                // Balor, Zyla
    {"CameraBuildVersion", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor
    {"CameraFamily", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"CameraInformation", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor,  -
    {"CameraModel", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"CameraName", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"CameraPresent", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"CameraStatus", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor,  -   <-- Rev dependent, see below
    {"ControllerID", {AT_TRUE, AT_FALSE, AT_TRUE}},                   //   -  , Zyla
    {"CorrectionApplying", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor,  -
    {"CXPEventsSupported", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor,  -
    {"CycleMode", {AT_TRUE, AT_FALSE, AT_FALSE}},                     // Balor, Zyla
    {"DeviceVideoIndex", {AT_TRUE, AT_FALSE, AT_TRUE}},               //   -  , Zyla
    {"DirectQueueing", {AT_TRUE, AT_TRUE, AT_FALSE}},                 // Balor, Zyla
    {"ElectronicShutteringMode", {AT_TRUE, AT_FALSE, AT_FALSE}},      // Balor, Zyla
    {"EventEnable", {AT_TRUE, AT_FALSE, AT_FALSE}},                   // Balor, Zyla
    {"EventSelector", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla
    {"EventsMissedEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},             // Balor, Zyla
    {"ExposedPixelHeight", {AT_FALSE, AT_FALSE, AT_FALSE}},           //   -  , Zyla <-- Shutter/Trigger Mode Dependent, see below
    {"ExposureEndEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},              // Balor, Zyla
    {"ExposureStartEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},            // Balor, Zyla
    {"ExposureTime", {AT_FALSE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"ExternalTriggerDelay", {AT_FALSE, AT_FALSE, AT_FALSE}},         // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"FanSpeed", {AT_TRUE, AT_TRUE, AT_FALSE}},                       //   -  , Zyla
    {"FanSpeedRPM", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor,  -
    {"FastAOIFrameRateEnable", {AT_TRUE, AT_FALSE, AT_FALSE}},        //   -  , Zyla
    {"FirmwareVersion", {AT_TRUE, AT_FALSE, AT_TRUE}},                // Balor, Zyla
    {"FrameCount", {AT_TRUE, AT_FALSE, AT_FALSE}},                    // Balor, Zyla
    {"FrameGenFixedPixelValue", {AT_TRUE, AT_FALSE, AT_FALSE}},       // Balor, Zyla
    {"FrameGenMode", {AT_TRUE, AT_FALSE, AT_FALSE}},                  // Balor, Zyla
    {"FrameRate", {AT_FALSE, AT_FALSE, AT_FALSE}},                    // Balor, Zyla <-- Trigger Mode Dependent, see below
    {"FullAOIControl", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"GainCorrection", {AT_FALSE, AT_FALSE, AT_FALSE}},               // Balor, Zyla <-- Camera Dependent, see below
    {"GainMode", {AT_TRUE, AT_FALSE, AT_FALSE}},                      // Balor, Zyla
    {"HDRTestMode", {AT_TRUE, AT_FALSE, AT_FALSE}},                   // Balor,  -
    {"ImageSizeBytes", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"InterfaceType", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"IOInvert", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"IOSelector", {AT_TRUE, AT_TRUE, AT_FALSE}},                     // Balor, Zyla
    {"IRIGClockFrequency", {AT_TRUE, AT_FALSE, AT_TRUE}},             // Balor,  -
    {"LinearityOffsetCorrection", {AT_TRUE, AT_FALSE, AT_FALSE}},     // Balor,  -
    {"LineScanSpeed", {AT_TRUE, AT_FALSE, AT_TRUE}},                  //   -  , Zyla
    {"LogLevel", {AT_TRUE, AT_TRUE, AT_FALSE}},                       // Balor, Zyla
    {"LongExposureTransition", {AT_TRUE, AT_FALSE, AT_TRUE}},         // Balor, Zyla
    {"MaxInterfaceTransferRate", {AT_TRUE, AT_FALSE, AT_TRUE}},       // Balor, Zyla
    {"MetadataEnable", {AT_TRUE, AT_FALSE, AT_FALSE}},                // Balor, Zyla
    {"MetadataFrame", {AT_TRUE, AT_FALSE, AT_TRUE}},                  // Balor, Zyla
    {"MetadataFrameInfo", {AT_TRUE, AT_FALSE, AT_FALSE}},             // Balor,  -
    {"MetadataIrig", {AT_TRUE, AT_FALSE, AT_FALSE}},                  // Balor,  -
    {"MetadataSize", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor,  -
    {"MetadataTimestamp", {AT_TRUE, AT_FALSE, AT_FALSE}},             // Balor, Zyla
    {"MicrocodeVersion", {AT_TRUE, AT_FALSE, AT_FALSE}},              //   -  , Zyla
    {"MultitrackBinned", {AT_TRUE, AT_FALSE, AT_FALSE}},              // Balor, Zyla
    {"MultitrackCount", {AT_TRUE, AT_FALSE, AT_FALSE}},               // Balor, Zyla
    {"MultitrackEnd", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla
    {"MultitrackSelector", {AT_TRUE, AT_TRUE, AT_FALSE}},             // Balor, Zyla
    {"MultitrackStart", {AT_TRUE, AT_FALSE, AT_FALSE}},               // Balor, Zyla
    {"Overlap", {AT_TRUE, AT_FALSE, AT_FALSE}},                       //   -  , Zyla
    {"PixelEncoding", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla
    {"PixelHeight", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"PixelProcCorrection", {AT_TRUE, AT_FALSE, AT_FALSE}},           // Balor,  -
    {"PixelReadoutRate", {AT_TRUE, AT_FALSE, AT_FALSE}},              // Balor, Zyla
    {"PixelWidth", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"ReadTemperatureChannel", {AT_TRUE, AT_TRUE, AT_FALSE}},         // Balor,  -    Engineering
    {"ReadTemperatureValue", {AT_TRUE, AT_TRUE, AT_FALSE}},           // Balor,  -    Engineering (WRITABLE SHOULD BE FALSE)
    {"ReadoutTime", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"ResetToDefaultValue", {AT_TRUE, AT_FALSE, AT_TRUE}},            // Balor,  -
    {"RollingShutterGlobalClear", {AT_TRUE, AT_FALSE, AT_FALSE}},     //   -  , Zyla
    {"RowNExposureEndEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},          // Balor, Zyla
    {"RowNExposureStartEvent", {AT_TRUE, AT_FALSE, AT_FALSE}},        // Balor, Zyla
    {"RowReadTime", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"ScanSpeedControlEnable", {AT_TRUE, AT_FALSE, AT_FALSE}},        //   -  , Zyla
    {"SensorCooling", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor, Zyla
    {"SensorHeight", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"SensorInitialised", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor,  -
    {"SensorReadoutMode", {AT_TRUE, AT_FALSE, AT_FALSE}},             //   -  , Zyla
    {"SensorTemperature", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor, Zyla
    {"SensorType", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor, Zyla
    {"SensorWidth", {AT_TRUE, AT_FALSE, AT_TRUE}},                    // Balor, Zyla
    {"SerialNumber", {AT_TRUE, AT_FALSE, AT_TRUE}},                   // Balor, Zyla
    {"ShutterMode", {AT_TRUE, AT_TRUE, AT_FALSE}},                    // Balor, Zyla
    {"ShutterOutputMode", {AT_TRUE, AT_TRUE, AT_FALSE}},              //   -  , Zyla
    {"ShutterTransferTime", {AT_FALSE, AT_FALSE, AT_FALSE}},          // Balor, Zyla <-- Cam/Shutter/Trigger Mode Dependent, see below
    {"SimplePreAmpGainControl", {AT_TRUE, AT_FALSE, AT_FALSE}},       // Balor, Zyla
    {"SoftwareTrigger", {AT_FALSE, AT_TRUE, AT_FALSE}},               // Balor, Zyla
    {"SpuriousNoiseFilter", {AT_TRUE, AT_FALSE, AT_FALSE}},           // Balor, Zyla
    {"StaticBlemishCorrection", {AT_TRUE, AT_FALSE, AT_FALSE}},       // Balor, Zyla
    {"TargetSensorTemperature", {AT_FALSE, AT_FALSE, AT_FALSE}},      // Balor, Zyla <-- Camera specific, see below
    {"TemperatureControl", {AT_TRUE, AT_FALSE, AT_FALSE}},            // Balor, Zyla
    {"TemperatureStatus", {AT_TRUE, AT_FALSE, AT_TRUE}},              // Balor, Zyla
    {"TECCurrent", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor,  -    Engineering
    {"TECVoltage", {AT_TRUE, AT_FALSE, AT_TRUE}},                     // Balor,  -    Engineering
    {"TimestampClock", {AT_TRUE, AT_FALSE, AT_TRUE}},                 // Balor, Zyla
    {"TimestampClockFrequency", {AT_TRUE, AT_FALSE, AT_TRUE}},        // Balor, Zyla
    {"TimestampClockReset", {AT_FALSE, AT_TRUE, AT_FALSE}},           // Balor, Zyla
    {"TriggerMode", {AT_TRUE, AT_FALSE, AT_FALSE}},                   // Balor, Zyla
    {"TriggerSource", {AT_TRUE, AT_FALSE, AT_FALSE}},                 // Balor,  -
    {"VerticallyCentreAOI", {AT_TRUE, AT_FALSE, AT_FALSE}}            // Balor, Zyla
  };

  // Add Balor/Zyla/Shutter/Trigger mode dependent features
  if( camInfo.getCameraType() == ::CameraType::BALOR )
  {
    // CameraStatus should be r/w/ro=1/1/0.
    // This will be fixed with Rev C Balor.
    // NOTE: When Rev C Balor is released This line will only apply to Rev A
    allFeatures["CameraStatus"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

    // Balor and Zyla differ on r/w/ro when acquisition is stopped
    allFeatures["GainCorrection"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    // Balor restricts this feature to only be writable while in “Auto” ShutterMode
    // and TriggerMode is not “External Exposure” and not during acquisitions.
    // We aren't using this feature nor "ShutterMode" so we should expect
    // writable to always be false.
    allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    // Balor has limitations that prevents this feature being writable during
    // acquisitions however Zyla does not so this feature is writable during
    // acquisitions for Zyla but not for Balor
    // NOTE: When Rev C Balor is released This line will only apply to Rev A
    allFeatures["TargetSensorTemperature"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    // Events are not really supported under Linux and what the camera is
    // reported with the 1.0.3.12 build differs from what the Zyla reports.
    // To avoid any confusion regarding Event related features, they will be
    // excluded from the test.
    allFeatures.erase( allFeatures.find( "BufferOverflowEvent" ) );
    allFeatures.erase( allFeatures.find( "CXPEventsSupported" ) );
    allFeatures.erase( allFeatures.find( "EventEnable" ) );
    allFeatures.erase( allFeatures.find( "EventSelector" ) );
    allFeatures.erase( allFeatures.find( "EventsMissedEvent" ) );
    allFeatures.erase( allFeatures.find( "ExposureEndEvent" ) );
    allFeatures.erase( allFeatures.find( "ExposureStartEvent" ) );
    allFeatures.erase( allFeatures.find( "RowNExposureEndEvent" ) );
    allFeatures.erase( allFeatures.find( "RowNExposureStartEvent" ) );
  }
  else  // camInfo.getCameraType() == ::CameraType::ZYLA
  {
    // Zyla 'ExposedPixelHeight' has both a shutter and trigger mode dependancy.
    // We'll handle this feature here.
    if( shutterMode == "Global" )
      allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
    else if( shutterMode == "Rolling" )
    {
      if( triggerMode != "External Exposure" )
        allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
      else
        allFeatures["ExposedPixelHeight"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
    }

    allFeatures["GainCorrection"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

    // Zyla 'ShutterTransferTime' is not writable when TriggerMode='External Exposure'
    if( triggerMode != "External Exposure" )
      allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
    else
      allFeatures["ShutterTransferTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

    allFeatures["TargetSensorTemperature"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );
  }


  //======================== TRIGGER MODE DEPENDENT ============================
  // Common between Balor and Zyla but differ depending on trigger mode

  if( triggerMode == "External Exposure" )
    allFeatures["ExposureTime"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
  else  // triggerMode == "Internal", "Software", "External", "External Start"
    allFeatures["ExposureTime"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

  if( triggerMode == "Internal" || triggerMode == "External Exposure" )
    allFeatures["ExternalTriggerDelay"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );
  else  // triggerMode == "Software", "External", "External Start"
    allFeatures["ExternalTriggerDelay"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE );

  if( triggerMode == "Internal" || triggerMode == "External Start" )
    allFeatures["FrameRate"] = R_W_RO( AT_TRUE, AT_TRUE, AT_FALSE);
  else  // triggerMode == "Software", "External", "External Exposure"
    allFeatures["FrameRate"] = R_W_RO( AT_TRUE, AT_FALSE, AT_FALSE );

  //
  //=================== END OF TRIGGER MODE DEPENDENT =========================

  // Find feature with longest name length. It is needed for output formatting.
  int max=0;
  std::map<std::string, R_W_RO>::iterator it;
  for( it=allFeatures.begin(); it!=allFeatures.end(); ++it)
    if( it->first.size() > max ) max = it->first.size();

  // Test r/w/ro for all features contained in 'allFeatures'...
  for( it=allFeatures.begin(); it!=allFeatures.end(); ++it)
  {
    std::string feature = it->first;

    int diff = max - feature.size();
    std::string dots(diff+3, '.');

    AT_BOOL isImplemented;
    WARN_ON_ERROR( AT_IsImplemented(H,
      CamUtil::string2wcs(feature).c_str(), &isImplemented) );

    if( isImplemented == AT_TRUE )
    {
      std::ostringstream prefix;
      prefix << when << ", " << feature << dots;
      success &= doTestReadWriteReadOnly(
        camInfo, printErrOnlyEnb, prefix.str(), feature, it->second );
    }
  }

  return success;
}
// End of testReadWriteReadOnlyRunning()
