
#include "atcore.h"

#include "CamUtil.h"
#include "CmdLineUtil.h"
#include "constants.h"
#include "MasterListOfFeatures.h"
#include "Timer.h"

#include <iostream>
#include <iomanip>                     // std::setw, std::right
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <string>

bool printErrorsOnly(false);   // command line option --printErrorsOnly
bool printMasterAll(false);    // command line option --printMasterAll
bool printMasterCam(false);    // command line opiton --printMasterCam
bool queryAll(false);          // command line option --queryAll
bool queryCam(false);          // command line option --queryCam
bool testAll(true);            // command line option --testAll
bool testCam(false);           // command line option --testCam

typedef std::unique_ptr<AT_U8[]> image_t;

void usage(char *progname);
bool Run( const CamInfo& camInfo );
int doPrintMaster( const CamInfo& camInfo );
int doQuery( const CamInfo& camInfo );
int doTest( const CamInfo& camInfo );


//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
       << "1) Use to print the master list of features for both the Balor and Zyla\n"
       << "   or for just one camera.\n"
       << "2) Use to print the feature name and the IsImplemented flag from the camera\n"
       << "   for all featues in the master list of features for only those that\n"
       << "   are valid for the current camera.\n"
       << "3) Use to compare the IsImplemented flag from the camera with the expected\n"
       << "   value as contained in the master list of features for all features in \n"
       << "   list or only those that are valid for the current camera.\n"
       << "NOTE: Command line flag processing is not very robust. Use only one at a time!\n"
       << std::endl
       << "Usage: " << progname << " [options]\n"
       << "     --help\n"
       << "       This usage help\n"
       << std::endl
       << "     --printErrorsOnly\n"
       << "       Reduces test output to only print tests that fail.\n"
       << std::endl
       << "     --printMasterAll\n"
       << "       Print the complete MasterListOfFeatures.\n"
       << "       No check between camera and master list is performed!\n"
       << std::endl
       << "     --printMasterCam\n"
       << "       Print only the features from the MasterListOfFeatures for the\n"
       << "       current camera.\n"
       << "       No check between camera and master list is performed!\n"
       << std::endl
       << "     --queryAll\n"
       << "       Print the feature name and \"IsImplemented\" result from the\n"
       << "       camera for all features contained in the MasterListOfFeatures.\n"
       << "       No check between camera and master list is performed!\n"
       << std::endl
       << "     --queryCam\n"
       << "       Print the feature name and \"IsImplemented\" result from the\n"
       << "       camera for only those features from the MasterListOfFeatures\n"
       << "       that indicate they are implemented in the current camera.\n"
       << "       No check between camera and master list is performed!\n"
       << std::endl
       << "     --testAll\n"
       << "       THIS IS THE DEFAULT!\n"
       << "       Test \"IsImplemented\" for all features from the\n"
       << "       MasterListOfFeatures against what the camera reports.\n"
       << std::endl
       << "     --testCam\n"
       << "       Test \"IsImplemented\" for all camera specific features from\n"
       << "       the MasterListOfFeatures against what the camera reports.\n"
       << std::endl;
}
// End of usage()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  bool success(false);

  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }

        if( option == "printMasterAll" )
        {
          // Print all feature names and "IsImplemented" from the
          // MasterListOfFeatures. The camera is not accessed and no check
          // between camera and master list is performed!
          printMasterAll = true;
        }
        else if( option == "printMasterCam" )
        {
          // Print camera specific feature names and "IsImplemented" from the
          // MasterListOfFeatures. The camera is not accessed and no check
          // between camera and master list is performed!
          printMasterCam = true;
        }
        else if( option == "queryAll" )
        {
          // Print the feature name and "IsImplemented" result from the
          // camera for all features contained in the MasterListOfFeatures.
          // No check between camera and master list is performed!
          queryAll = true;
        }
        else if( option == "queryCam" )
        {
          // Print the feature name and "IsImplemented" result from the
          // camera for only those features from the MasterListOfFeatures
          // that indicate they are implemented in the current camera.
          // No check between camera and master list is performed!
          queryCam = true;
        }
        else if( option == "testAll" )
        {
          // Test "IsImplemented" for all features from the MasterListOfFeatures
          // against what the camera reports. THIS IS THE DEFAULT.
          testAll = true;
        }
        else if( option == "testCam" )
        {
          // Test "IsImplemented" for all camera specific features from the
          // MasterListOfFeatures against what the camera reports.
          testCam = true;
        }
        else if( option == "printErrorsOnly" )
        {
          printErrorsOnly = true;
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          CmdLineUtil::cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( CommandLineError& err ) {
    errMsg << err.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    CmdLineUtil::cmdLineError( progName, errMsg );
  }

  auto timenow =
    std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  std::cout << "Date: " << std::ctime(&timenow);

  CamInfo camInfo;
  if( AT_SUCCESS == CamUtil::OpenAndIdentify( camInfo ) )
  {
    auto start = std::chrono::steady_clock::now();

    success = Run( camInfo );
//    success = ((AT_SUCCESS == sts) ? true : false);

    auto end = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration<double, std::milli>(end-start);

    std::cout.precision(2);
    std::cout << std::endl
              << "Overall Status: " << ((success) ? "SUCCESS" : "FAILURE")
              << ", total runtime=" << std::fixed << duration.count() << "ms"
              << std::endl;

    WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
    WARN_ON_ERROR(AT_FinaliseLibrary());
  }


  return success ? 0 : 1;
}
// End of main()


//---------------------------------- Run() -------------------------------------
// camInfo - Contains camera information
// printErrOnlyEnb, -,  If true, only output tests that fail
bool Run( const CamInfo& camInfo )
{

  std::cout << "\n*********************************\n";

  int sts;
  if( printMasterAll || printMasterCam )
    sts = doPrintMaster( camInfo );
  else if( queryAll || queryCam )
    sts = doQuery( camInfo );
  else if( testAll || testCam )
    sts = doTest( camInfo );

  std::cout << "\n*********************************\n";

  return ((AT_SUCCESS == sts) ? true : false);
}
// End of Run()


//-------------------------------- doPrint() -----------------------------------
//
int doPrintMaster( const CamInfo& camInfo )
{
  MasterListOfFeatures masterList;

  // Get the appropriate list of features, either ALL features for both
  // types of cameras or just the camera specific features...
  std::vector<AndorFeature> listOfFeatures =
    ((printMasterAll) ? masterList.getAllFeatures() :
                        masterList.getCamFeatures( camInfo ));

  for( AndorFeature feature : listOfFeatures )
    std::cout << feature << std::endl;

  std::cout << "Total number of features: " << listOfFeatures.size()
            << std::endl;

  return AT_SUCCESS;
}
// End of doPrint()


//-------------------------------- doQuery() -----------------------------------
//
int doQuery( const CamInfo& camInfo )
{
  MasterListOfFeatures masterList;

  // Get the appropriate list of features, either ALL features for both
  // types of cameras or just the camera specific features...
  std::vector<AndorFeature> listOfFeatures =
    ((queryAll) ? masterList.getAllFeatures() :
                  masterList.getCamFeatures( camInfo ));

  for( AndorFeature feature : listOfFeatures )
    RETURN_ON_ERROR( CamUtil::PrintIsImplemented(
      camInfo.getHandle(), CamUtil::string2wcs(feature.name()).c_str() ) );

  std::cout << "Total number of features: " << listOfFeatures.size()
            << std::endl;

  return AT_SUCCESS;
}
// End of doQuery()


//-------------------------------- doTest() ------------------------------------
//
int doTest( const CamInfo& camInfo )
{
  int errCnt(0);
  MasterListOfFeatures masterList;

  // Get the appropriate list of features, either ALL features for both
  // types of cameras or just the camera specific features...
  std::vector<AndorFeature> listOfFeatures =
    ((testAll) ? masterList.getAllFeatures() :
                masterList.getCamFeatures( camInfo ));

  AndorFeature::ImplementedOn camMask;
  if( camInfo.getCameraType() == ::CameraType::ZYLA )
    camMask.item.zyla = 1;
  else if( camInfo.getCameraType() == ::CameraType::BALOR )
  {
    camMask.item.balor_revA = static_cast<int>(camInfo.isBalorRevA());
    camMask.item.balor_revB = static_cast<int>(camInfo.isBalorRevB());
  }

  // Note: listOfFeatures either contains all features or just the
  // camera specific ones.
  for( AndorFeature feature : listOfFeatures )
  {
    AT_BOOL fromCamera;
    RETURN_ON_ERROR( AT_IsImplemented(
      camInfo.getHandle(),
      CamUtil::string2wcs(feature.name()).c_str(),
      &fromCamera ) );

    bool actual = (fromCamera == AT_TRUE) ? true : false;
    bool expected(false);

    if( testAll )
      expected = static_cast<bool>(feature.getCameras().word & camMask.word);
    else  // testCam
      // If we're just checking those feature valid for the current
      // camera then the expected value for all features is IsImplemented=true
      expected = true;

    bool okay = (expected == actual);
    if( !okay ) errCnt++;
    if( !printErrorsOnly || (printErrorsOnly && !okay) )
      std::cout << feature << ", Checking camera...Expected="
                << std::setw(5) << std::left << std::boolalpha << expected
                << ", Actual="
                << std::setw(5) << std::left << std::boolalpha << actual
                << " - " << ((okay) ? "OK" : "FAILURE") << std::endl;
  }

  std::cout << "Total number of features: " << listOfFeatures.size()
            << std::endl;
  std::cout << "Total number of failures: " << errCnt
            << std::endl;

  return (errCnt == 0) ? AT_SUCCESS : !AT_SUCCESS;
}
// End of doTest()
