#ifndef ANDOR_CONSTANTS_H
#define ANDOR_CONSTANTS_H

#include "atcore.h"
#include <string>
#include <vector>

enum CameraType { UNKNOWN, BALOR, ZYLA };

static const std::vector<std::string> balorShutterModes={
//  "Global", "Global - 100% Duty Cycle", "Rolling", "Rolling - 100% Duty Cycle"};
  "Global", "Rolling"
};

static const std::vector<std::string> balorGainModes={
  "Standard (16-bit)", "Medium Gain (12-bit)"
};

static const std::vector<std::string> zylaShutterModes={
  "Global", "Rolling"
};

static const std::vector<std::string> zylaGainModes={
  "16-bit (low noise & high well capacity)",
  "12-bit (high well capacity)"
};

// All trigger modes supported by the Andor cameras
static const std::vector<std::string> allAndorTriggerModes=
  {"Internal", "Software", "External", "External Exposure", "External Start"};

// Trigger modes currently being supported by CSS
static const std::vector<std::string> cssSupportedTriggerModes=
  {"Internal", "Software", "External"};

static const int BUFFERCOUNT=12;
static const int TIMEOUTMS = 10000;
static const int UPDATEINTERVALFRAMES=1;
static const int PAUSEAFTER = false;
static const int IDX_MIN=0;
static const int IDX_MAX=1;
static const int SHORT = 0;
static const int LONG = 1;

static const bool    DEFAULT_PRINTFRAMEDATA=false;
static const int     DEFAULT_ROWSTOPRINT=5;
static const int     DEFAULT_PIXELSPERROWTOPRINT=10;
static const int     DEFAULT_FRAMESTOACQUIRE = 10; 
static const double  DEFAULT_EXPOSURETIME=0.001;
static const double  DEFAULT_FRAMERATE = 10.0;
static const AT_BOOL DEFAULT_DIRECTQUEUEING=AT_TRUE;
static const AT_BOOL DEFAULT_FASTAOI=AT_FALSE;

static const AT_WC* DEFAULT_AOILAYOUT = L"Image";
static const AT_WC* DEFAULT_CYCLEMODE = L"Fixed";
static const AT_WC* DEFAULT_PIXELENCODING=L"Mono16";
static const AT_WC* DEFAULT_SHUTTERMODE=L"Global";
static const AT_WC* DEFAULT_TRIGGERMODE = L"Internal";

#endif

