#ifndef READ_WRITE_READONLY_H
#define READ_WRITE_READONLY_H

#include "atcore.h"

#include <ostream>
#include <string>

//==============================================================================
// CLASS: R_W_RO
//==============================================================================

struct R_W_RO {
  AT_BOOL readable;
  AT_BOOL writable;
  AT_BOOL readonly;

  /** Default constructor. */
  R_W_RO();

  /** Std constructor. */
  R_W_RO(
    const AT_BOOL r, const AT_BOOL w, const AT_BOOL ro);

 /** Copy constructor. */
  R_W_RO( const R_W_RO& lhs );

  /** Assignment operator. */
  R_W_RO& operator=( const R_W_RO &rhs );

};

#endif    // End of READ_WRITE_READONLY_H

//------------------------------------------------------------------------------
// END OF FILE: ReadWriteReadOnly.h
//------------------------------------------------------------------------------

