
#include <iostream>

#include "CamInfo.h"
#include "CamUtil.h"
#include "constants.h"

//==============================================================================
// CLASS: CamInfo
//==============================================================================

//------------------------------------------------------------------------------
// CamInfo: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//

CamInfo::CamInfo() :
  H(0),
  cameraType(::CameraType::UNKNOWN),
  sensorWidth(0),
  sensorHeight(0) {}
// End of Constructor #1

//============================= CONSTRUCTOR #2 =================================
//
CamInfo::CamInfo( AT_H handle, const ::CameraType& type, const int w, const int h ) :
  H(handle),
  cameraType(type),
  sensorWidth(w),
  sensorHeight(h),
  balorRevA(false),
  balorRevB(false),
  gainModes( (type==BALOR) ? ::balorGainModes : ::zylaGainModes ),
  shutterModes( (type==BALOR) ? ::balorShutterModes : ::zylaShutterModes ),
  allTriggerModes( ::allAndorTriggerModes ),
  cssTriggerModes( ::cssSupportedTriggerModes )
{
  fullFrameAOI = AOI(1,1,w,h,"AOI");
  fullFrameROI = ROI(0,0,w,h,"ROI");
  if( type == BALOR )
  {
    // Determine the Camera Revision base on the serial number
    // Get serial number from the camera
    std::string sn;
    if( AT_SUCCESS != CamUtil::GetString(H, L"SerialNumber", sn) )
      return;

    // Look up in both the RevA and RevB Serial Number sets...
    auto revA = balorRevASerialNumbers.find(sn);
    auto revB = balorRevBSerialNumbers.find(sn);

    if( revA != balorRevASerialNumbers.end() )
      balorRevA = true;
    else if( revB != balorRevBSerialNumbers.end() )
      balorRevB = true;
    else
      std::cout << "ERROR - CANNOT FIND CAMERA SERIAL NUMBER " << sn
                << " IN CamInfo::balorRevASerialNumbers or"
                << " CamInfo::balorRevBSerialNumbers!";

    if( balorRevA ) std::cout << "+++++++++++++ REV A\n";
    if( balorRevB ) std::cout << "+++++++++++++ REV B\n";
  }
}
// End of Constructor #2

// Access methods
::CameraType CamInfo::getCameraType() const
{
  return cameraType;
}

AT_H CamInfo::getHandle() const
{
  return H;
}

std::vector<std::string> CamInfo::getGainModes() const
{
  return gainModes;
}

std::vector<std::string> CamInfo::getShutterModes() const
{
  return shutterModes;
}

std::vector<std::string> CamInfo::getAllTriggerModes() const
{
  return allTriggerModes;
}

std::vector<std::string> CamInfo::getCssTriggerModes() const
{
  return cssTriggerModes;
}

int CamInfo::getSensorWidth() const
{
  return sensorWidth;
}

int CamInfo::getSensorHeight() const
{
  return sensorHeight;
}

AOI CamInfo::getFullFrameAOI() const
{
  return fullFrameAOI;
}

ROI CamInfo::getFullFrameROI() const
{
  return fullFrameROI;
}

bool CamInfo::isBalorRevA() const
{
  return ((getCameraType() == ::CameraType::BALOR) ? balorRevA : false);
}

bool CamInfo::isBalorRevB() const
{
  return ((getCameraType() == ::CameraType::BALOR) ? balorRevB : false);
}

//------------------------------------------------------------------------------
// END OF FILE: CamInfo.cpp
//------------------------------------------------------------------------------
