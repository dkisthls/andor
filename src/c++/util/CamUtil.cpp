#include "CamUtil.h"
#include "constants.h"
#include "Timer.h"

#include <iostream>
#include <iomanip>

//==============================================================================
// CLASS: AOI
//==============================================================================

//------------------------------------------------------------------------------
// AOI: PUBLIC
//------------------------------------------------------------------------------

//------------------------------ string2wcs() ----------------------------------
// Utility to convert std::string to wide character string
//
std::wstring CamUtil::string2wcs( const std::string& val )
{
  std::unique_ptr<wchar_t> wcs(new wchar_t[val.length()+1]);
  std::mbstowcs(wcs.get(), val.c_str(), val.length()+1);
  return wcs.get();
}
// End of CamUtil::string2wcs()


//------------------------------ wcs2string() ----------------------------------
// Utility to convert wide character string to std::string
//
std::string CamUtil::wcs2string( const ::AT_WC* wcs )
{
  std::wstring ws( wcs );
  return std::string( ws.begin(), ws.end() );
}
// End of CamUtil::wcs2string()

//------------------------ AllocateAndQueueBuffers() ---------------------------
//
int CamUtil::AllocateAndQueueBuffers( AT_H H, std::vector<image_t>& frameBuffers )
{
  int sts(AT_SUCCESS);

  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    sts = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if( sts != AT_SUCCESS) {
      std::cout << "AT_QueueBuffer return code" << sts << std::endl;
      break;
    }

    frameBuffers.push_back(move(image));
  }
  return sts;
}
// End of CamUtil::AllocateAndQueueBuffers()

int CamUtil::DoGenericConfiguration(AT_H H,
                                    const std::string& gainMode,
                                    const std::string& shutterMode)
{
  return AT_SUCCESS;
}

//-------------------------------- Aoi2Roi() -----------------------------------
// Converts Andor AOI(top,left,width,height) to 
//            CSS ROI(originX,originY,sizeX,sizeY).
ROI CamUtil::Aoi2Roi(const AOI& aoi, int sensorHeight)
{
  int originX( aoi.left - 1 );
  int originY( sensorHeight - aoi.height - aoi.top + 1 );
  int sizeX( aoi.width );
  int sizeY( aoi.height );

  return ROI(originX, originY, sizeX, sizeY, aoi.desc);
  
}
// End of CamUtil::Aoi2Roi()


//-------------------------------- Roi2Aoi() -----------------------------------
// Converts CSS ROI to Andor AOI(top,left,width,height) to 
//            CSS ROI(originX,originY,sizeX,sizeY).
AOI CamUtil::Roi2Aoi(const ROI& roi, const int sensorHeight)
{
  int left( roi.originX + 1 );
  int top( sensorHeight - (roi.originY + roi.sizeY) + 1 );
  int width( roi.sizeX );
  int height( roi.sizeY );

  return AOI(left, top, width, height, roi.desc);
  
}
// End of CamUtil::Roi2Aoi()


//---------------------------- IdentifyCamera() --------------------------------
//
int CamUtil::IdentifyCamera(AT_H H, ::CameraType& cameraType)
{
  std::string cameraName;
  RETURN_ON_ERROR( GetString(H, L"CameraName", cameraName) );
  if( cameraName.find("Balor") != std::string::npos )
  {
    cameraType = ::CameraType::BALOR;
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraInformation") );
  }
  else if( cameraName.find("Zyla") != std::string::npos )
  {
    cameraType = ::CameraType::ZYLA;
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraModel") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraName") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"SerialNumber") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"FirmwareVersion") );
    RETURN_ON_ERROR( PrintIntFeature(H, L"SensorHeight") );
    RETURN_ON_ERROR( PrintIntFeature(H, L"SensorWidth") );
  }
  else
  {
    std::cout << "DON'T KNOW HOW TO HANDLE CAMERA NAME \"" 
              << cameraName << "\"!" << std::endl;
    return 1;
  }
 
 return AT_SUCCESS;
}
// End of CamUtil::IdentifyCamera()


//---------------------------- OpenAndIdentify() -------------------------------
//
int CamUtil::OpenAndIdentify( CamInfo& info )
{
  int err;

  RETURN_ON_ERROR(AT_InitialiseLibrary());

  AT_64 devcount;
  WARN_ON_ERROR(AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount", &devcount));
  std::cout << "Found " << devcount << " devices" << std::endl;

  AT_H H;
  err = AT_Open(0, &H);

  if( AT_SUCCESS == err )
    std::cout << "Successfully Opened Camera"<< std::endl;
  else
  {
    std::cout << "AT_Open return code: =" << err << std::endl;
    WARN_ON_ERROR(AT_FinaliseLibrary());
    return err;
  }

  ::CameraType cameraType;
  RETURN_ON_ERROR( IdentifyCamera(H, cameraType) );

  AT_64 w,h;
  RETURN_ON_ERROR( AT_GetInt(H, L"SensorWidth", &w) );
  RETURN_ON_ERROR( AT_GetInt(H, L"SensorHeight", &h) );

  info = CamInfo( H, cameraType, w, h );

  return AT_SUCCESS;
}
// End of CamUtil::OpenAndIdentify()

//------------------------- SetBasicConfiguration() ----------------------------
//
int CamUtil::SetBasicConfiguration(const CamInfo& camInfo)
{
  AT_H H( camInfo.getHandle() );

  std::cout << "-------- Setting Basic Configuration --------" 
            << std::endl;

  // if( camInfo.getCameraType() == ::CameraType::ZYLA )
  // {
  //   RETURN_ON_ERROR( PrintAndSetBoolFeature(H, L"Overlap", AT_FALSE) );
  // }
  RETURN_ON_ERROR( AT_SetBool(H, L"DirectQueueing", AT_TRUE) );
  RETURN_ON_ERROR( PrintBoolFeature(H, L"DirectQueueing") );
  std::cout << "Number of Buffers: " << BUFFERCOUNT << std::endl;
  
  WaitForSensorInitialisation(H);

  RETURN_ON_ERROR( PrintEnumFeature(H, L"PixelReadoutRate") );
  RETURN_ON_ERROR( PrintFloatFeature(H, L"RowReadTime", 6) );
  RETURN_ON_ERROR( PrintFloatFeatureMinMax(H, L"ExposureTime", 6) );
  RETURN_ON_ERROR( PrintFloatFeatureMinMax(H, L"FrameRate", 17) );

  AT_BOOL isImplemented;
  RETURN_ON_ERROR( AT_IsImplemented(H,L"FastAOIFrameRateEnable",&isImplemented) );
  if( AT_TRUE == isImplemented )
    RETURN_ON_ERROR( PrintBoolFeature(H, L"FastAOIFrameRateEnable") );

  RETURN_ON_ERROR( PrintBoolFeature(H,L"FullAOIControl") );
  RETURN_ON_ERROR( PrintEnumFeature(H,L"AOIBinning") );
  RETURN_ON_ERROR( PrintIntFeature(H,L"AOIHBin") );
  RETURN_ON_ERROR( PrintIntFeature(H,L"AOIVBin") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOILeft") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOITop") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOIWidth") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOIHeight") );
  RETURN_ON_ERROR( PrintIntFeature(H,L"AOIStride") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"MultitrackStart") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"MultitrackEnd") );

  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", DEFAULT_SHUTTERMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"TriggerMode", DEFAULT_TRIGGERMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"CycleMode", DEFAULT_CYCLEMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"PixelEncoding", DEFAULT_PIXELENCODING) );
  RETURN_ON_ERROR( PrintAndSetIntFeature(H,L"FrameCount", DEFAULT_FRAMESTOACQUIRE) );
  RETURN_ON_ERROR( PrintAndSetFloatFeature(H,L"ExposureTime", DEFAULT_EXPOSURETIME) );

//  RETURN_ON_ERROR( PrintAndSetFloatFeature(H, L"ExposureTime", DEFAULT_EXPOSURETIME) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"AOILayout", DEFAULT_AOILAYOUT ) );

  std::cout << "-------- Setting Basic Configuration Complete! --------" 
            << std::endl << std::endl;

  return AT_SUCCESS;
}
// End of CamUtil::SetBasicConfiguration()


//--------------------------- PrintMono16Frame() -------------------------------
//
void CamUtil::PrintMono16Frame( int frameNumber, unsigned char* frameP,
                                int imageSizeBytes, int aoistride,
                                int pixelCount, int rowCount )
{
  std::cout << "  Frame " << frameNumber 
            << ", ImageSize " << imageSizeBytes << std::endl;

  for(int row=0; row<rowCount; row++) {
    std::cout << "    Row " << std::dec 
              << std::setfill(' ') << std::setw(4) << (row+1) << ": " << std::hex;
    unsigned short *pixels = reinterpret_cast<unsigned short *>(frameP+ aoistride*row);
    for (int i = 0; i < pixelCount; i++) {
      std::cout << "0x" << std::setfill('0') << std::setw(4) << *pixels++;
      if (i < pixelCount-1) {
        std::cout << ", ";
      }
    }
    std::cout << std::endl;
  }

  std::cout << "    Last Row End Pixels: ";

  unsigned short *pixels = reinterpret_cast<unsigned short*>(frameP+imageSizeBytes-pixelCount*2);
  for (int i=0; i<pixelCount; i++) {
    std::cout << "0x" << std::setfill('0') << std::setw(4) << *pixels++;
    if (i<pixelCount-1) {
      std::cout << ", ";
    }
  }

  std::cout << std::endl << std::dec;
}
// End of CamUtil::PrintMono16Frame()


int CamUtil::PrintBoolFeature(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_GetBool(H, feature.c_str(), &value));

  wMsgStr << feature << L": " << value << std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintEnumFeature(AT_H H, const AT_WC* feature) {
  int enumIndex;
  std::wostringstream wMsgStr;

  RETURN_ON_ERROR(AT_GetEnumIndex(H, feature, &enumIndex));

  AT_WC enumString[40];
  RETURN_ON_ERROR(AT_GetEnumStringByIndex(H, feature, enumIndex, enumString, 40));

  wMsgStr<<feature<<L": "<<enumString<<L"["<<enumIndex<<"]"<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintEnumFeatureIndexInfo(AT_H H, const AT_WC* feature, int enumIndex) {
  std::wostringstream wMsgStr;
  AT_BOOL isAvailable(AT_FALSE);
  AT_BOOL isImplemented(AT_FALSE);
  AT_WC enumString[40];

  RETURN_ON_ERROR(AT_IsEnumIndexImplemented(H, feature, enumIndex, &isImplemented));
  if( isImplemented )
  {
    RETURN_ON_ERROR(AT_IsEnumIndexAvailable(H, feature, enumIndex, &isAvailable));
    RETURN_ON_ERROR(AT_GetEnumStringByIndex(H, feature, enumIndex, enumString, 40));
  }

  wMsgStr << feature 
          << L": index=" << enumIndex
          << L", isImplemented=" << isImplemented
          << L", isAvailable=" << isAvailable;
  if( isImplemented )
    wMsgStr << L", value=\"" << enumString << L"\"" << std::endl;
  else
    wMsgStr << L", NO VALUE" << std::endl;

  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision) {
  double value;
  std::ostringstream msgStr;

  RETURN_ON_ERROR(AT_GetFloat(H, feature, &value));

  msgStr << std::fixed << std::setprecision(precision) 
         << wcs2string(feature) << ": " << value << std::endl;
  std::cout << msgStr.str() << std::flush;

  return AT_SUCCESS;
}

int CamUtil::PrintFloatFeatureMinMax(AT_H H, const AT_WC* feature, const int precision) {
  double min, max;
  std::ostringstream msgStr;

  RETURN_ON_ERROR(AT_GetFloatMin(H, feature, &min));
  RETURN_ON_ERROR(AT_GetFloatMax(H, feature, &max));

  msgStr << std::fixed << std::setprecision(precision)
         << wcs2string(feature) << ": Min="  << min <<", Max=" << max <<std::endl;
  std::cout << msgStr.str() << std::flush;

  return AT_SUCCESS;
}

int CamUtil::PrintIntFeature(AT_H H, const AT_WC* feature) {
  int length = 0;
  std::wostringstream wMsgStr;
  AT_64 value;

  RETURN_ON_ERROR(AT_GetInt(H, feature, &value));

  wMsgStr<<feature<<L": "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintIntFeatureMinMax(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  AT_64 min, max;

  RETURN_ON_ERROR(AT_GetIntMin(H, feature, &min));
  RETURN_ON_ERROR(AT_GetIntMax(H, feature, &max));

  wMsgStr<<feature<<L": Min="<<min<<", Max=" <<max<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::GetString(AT_H H, const AT_WC* feature, std::string& value)
{
  std::wostringstream wMsgStr;
  int length = 0;

  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));
  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  value = wcs2string(&stringValue[0]);
  return AT_SUCCESS;
}

int CamUtil::PrintStringFeature(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  int length = 0;

  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));

  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  wMsgStr<<feature<<L": "<<&stringValue[0]<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetBool(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int CamUtil::PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetEnumString(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int CamUtil::PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetFloat(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int CamUtil::PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value)
{
  return PrintAndSetIntFeature(H, feature, value, false);
}

int CamUtil::PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value, const bool inHex)
{
  std::wostringstream wMsgStr;

  wMsgStr << L"Setting " << feature << L" to ";
  if( inHex ) 
    wMsgStr << L"0x" << std::hex;
  else
    wMsgStr << std::dec;

  wMsgStr << value << " ... ";

  int err = AT_SetInt(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int CamUtil::PrintIsImplemented(AT_H H, const std::wstring& feature)
{
  return PrintIsImplemented(H, feature.c_str());
}

int CamUtil::PrintIsImplemented(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsImplemented(H, feature, &value));

  wMsgStr<<feature<<L", isImplemented="<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintIsReadable(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsReadable(H, feature.c_str(), &value));

  bool val = (value == AT_TRUE) ? true : false;
  wMsgStr<<feature<<L" isReadable: "<<std::boolalpha<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintIsReadOnly(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsReadOnly(H, feature.c_str(), &value));

  wMsgStr<<feature<<L" isReadOnly: "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int CamUtil::PrintIsWritable(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsWritable(H, feature.c_str(), &value));

  wMsgStr<<feature<<L" isWritable: "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

// --------------------- WaitForSensorInitialisation() -------------------------
//
int CamUtil::WaitForSensorInitialisation(AT_H H) {
  int status(AT_SUCCESS);
  AT_BOOL sensorInitialised(AT_FALSE);
  AT_BOOL sensorInitialisedImplemented(AT_FALSE);

  AT_IsImplemented(H, L"SensorInitialised", &sensorInitialisedImplemented);
  if (sensorInitialisedImplemented == AT_TRUE)
  {
    std::wcout << L"Waiting for SensorInitialised.";
    std::wcout.flush();

    Timer t(10000);
    t.start();
    double elapsedDotsTime(0.5);
    do {
      if( t.get_elapsed() >= elapsedDotsTime )
      {
        std::wcout << L".";
        std::wcout.flush();
        elapsedDotsTime += 0.5;
      }

      RETURN_ON_ERROR( status = AT_GetBool(H, L"SensorInitialised", &sensorInitialised) );

    } while( sensorInitialised == AT_FALSE && !t.didTimeout() );

    if( t.didTimeout() )
      status = AT_ERR_TIMEDOUT;
    else if( status == AT_SUCCESS )
    {
      t.stop();
      std::wcout << L"OK, completed in " << std::fixed << t.get_dt() << L"sec" << std::endl;
    }
  }

  return status;
}
// End of CamUtil::WaitForSensorInitialisation()


// ----------------------- WaitForCorrectionApplying() --------------------------
//
int CamUtil::WaitForCorrectionApplying(AT_H H, int timeout_ms)
{
  int status(AT_SUCCESS);
  double elapsedDotsTime(0.5); 
  AT_BOOL correction_applying(AT_TRUE);
  Timer t(timeout_ms);

  std::wcout << L"Waiting for CorrectionApplying.";
  std::wcout.flush();

  t.start();
  do {
    if( t.get_elapsed() >= elapsedDotsTime )
    {
      std::wcout << L".";
      std::wcout.flush();
      elapsedDotsTime += 0.5;
    }

    if( elapsedDotsTime > 0.5 )
      RETURN_ON_ERROR(AT_GetBool(H, L"CorrectionApplying", &correction_applying));

  } while( correction_applying == AT_TRUE && !t.didTimeout() );

  if( t.didTimeout() )
  {
    std::wcout << L"FAILURE: Timeout after " << timeout_ms << L"ms, ";
    PrintBoolFeature(H, L"CorrectionApplying");
    status = AT_ERR_TIMEDOUT;
  }
  else
  {
    t.stop();
    std::wcout << L"OK, completed in " << t.get_dt() << L"sec." << std::endl;
  }
  return status;
}
// End of CamUtil::WaitForCorrectionApplying()

//------------------------------------------------------------------------------
// END OF FILE: CamUtil.cpp
//------------------------------------------------------------------------------
