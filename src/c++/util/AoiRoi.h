#ifndef AOI_ROI_H
#define AOI_ROI_H

#include "atcore.h"

#include <ostream>
#include <string>

//==============================================================================
// CLASS: AOI
//==============================================================================

class AOI {
  public:
    int left;
    int top;
    int width;
    int height;
    std::string desc;
    AOI();
    AOI( const int l, const int t, const int w, const int h, const std::string& d);

    friend std::ostream& operator<<( std::ostream& os, const AOI& rhs );
    AOI& operator=( const AOI& rhs );
    std::string toSimpleString();
};


//==============================================================================
// CLASS: ROI
//==============================================================================

class ROI {
  public:
    int originX;
    int originY;
    int sizeX;
    int sizeY;
    std::string desc;
    ROI();
    ROI( const int oX, const int oY, const int sX, const int sY, const std::string& d);

    friend std::ostream& operator<<( std::ostream& os, const ROI& rhs );
    ROI& operator=(const ROI& rhs);
    std::string toSimpleString();
};

#endif    // End of AOI_ROI_H

//------------------------------------------------------------------------------
// END OF FILE: AoiRoi.h
//------------------------------------------------------------------------------
