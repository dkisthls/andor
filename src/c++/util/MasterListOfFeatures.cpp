#include "MasterListOfFeatures.h"
#include "constants.h"

//==============================================================================
// CLASS: MasterListOfFeatures
//==============================================================================

// NOTE 1: Only on Zyla 4.2 PLUS, not Zyla 5.5
// NOTE 2: Not supported by Bitflow Linux SDK, Only Windows
// NOTE 3: FanSpeed is a Zyla feature only. The SDK has no way to determine if
//         a Zyla is air or water cooled. As such, this feature is always
//         implemented even if the camera is water cooled.
// NOTE 4: Balor feature that is only valid with the USB interface. We only use
//         a CXP interface so it is marked as NONE.
// NOTE 5: Balor Rev A camera firmware reports the feature as implemented but
//         it is not actually usable from Linux.


// Constants definitions
const std::vector<AndorFeature> MasterListOfFeatures::listOfFeatures = {
  {AndorFeature( "AccumulateCount",             _ALL_CAMERAS )},
  {AndorFeature( "AcquisitionStart",            _ALL_CAMERAS )},
  {AndorFeature( "AcquisitionStop",             _ALL_CAMERAS )},
  {AndorFeature( "AlternatingReadoutDirection", _NONE )},         // See NOTE 1
  {AndorFeature( "AOIBinning",                  _ALL_CAMERAS )},
  {AndorFeature( "AOIHBin",                     _ALL_CAMERAS )},
  {AndorFeature( "AOIHeight",                   _ALL_CAMERAS )},
  {AndorFeature( "AOILayout",                   _ALL_CAMERAS )},
  {AndorFeature( "AOILeft",                     _ALL_CAMERAS )},
  {AndorFeature( "AOIStride",                   _ALL_CAMERAS )},
  {AndorFeature( "AOITop",                      _ALL_CAMERAS )},
  {AndorFeature( "AOIVBin",                     _ALL_CAMERAS )},
  {AndorFeature( "AOIWidth",                    _ALL_CAMERAS )},
  {AndorFeature( "AuxiliaryOutSource",          _ALL_CAMERAS )},
  {AndorFeature( "AuxOutSourceTwo",             _ALL_CAMERAS )},
  {AndorFeature( "Baseline",                    _ALL_CAMERAS )},
  {AndorFeature( "BaselineClamp",               _ALL_CAMERAS )},
  {AndorFeature( "BiasLevelEnable",             _ALL_BALORS )},
  {AndorFeature( "BitDepth",                    _ALL_CAMERAS )},
  {AndorFeature( "BufferOverflowEvent",         _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "BytesPerPixel",               _ALL_CAMERAS )},
  {AndorFeature( "CameraAcquiring",             _ALL_CAMERAS )},
  {AndorFeature( "CameraBuildVersion",          _ALL_BALORS )},
  {AndorFeature( "CameraFamily",                _ALL_CAMERAS )},
  {AndorFeature( "CameraInformation",           _ALL_BALORS )},
  {AndorFeature( "CameraModel",                 _ALL_CAMERAS )},
  {AndorFeature( "CameraName",                  _ALL_CAMERAS )},
  {AndorFeature( "CameraPresent",               _ALL_CAMERAS )},
  {AndorFeature( "CameraStatus",                _ALL_BALORS )},
  {AndorFeature( "ControllerID",                _ZYLA )},
  {AndorFeature( "CorrectionApplying",          _ALL_BALORS )},
  {AndorFeature( "CXPEventsSupported",          _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "CXPFlowControlEnable",        _ALL_BALORS )},
  {AndorFeature( "CycleMode",                   _ALL_CAMERAS )},
  {AndorFeature( "DeviceVideoIndex",            _ZYLA )},
  {AndorFeature( "DirectQueueing",              _ALL_CAMERAS )},
  {AndorFeature( "ElectronicShutteringMode",    _ALL_CAMERAS )},
  {AndorFeature( "EventEnable",                 _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "EventSelector",               _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "EventsMissedEvent",           _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "ExposedPixelHeight",          _ZYLA )},
  {AndorFeature( "ExposureEndEvent",            _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "ExposureStartEvent",          _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "ExposureTime",                _ALL_CAMERAS )},
  {AndorFeature( "ExternalTriggerDelay",        _ALL_CAMERAS )},
  {AndorFeature( "FanSpeed",                    _ZYLA )},         // See NOTE 3
  {AndorFeature( "FanSpeedRPM",                 _ALL_BALORS )},
  {AndorFeature( "FastAOIFrameRateEnable",      _ZYLA )},
  {AndorFeature( "FirmwareVersion",             _ALL_CAMERAS )},
  {AndorFeature( "FrameCount",                  _ALL_CAMERAS )},
  {AndorFeature( "FrameGenFixedPixelValue",     _ALL_BALORS )},
  {AndorFeature( "FrameGenMode",                _ALL_BALORS )},
  {AndorFeature( "FrameRate",                   _ALL_CAMERAS )},
  {AndorFeature( "FullAOIControl",              _ALL_CAMERAS )},
  {AndorFeature( "GainCorrection",              _ALL_CAMERAS )},
  {AndorFeature( "GainMode",                    _ALL_BALORS )},
  {AndorFeature( "GlobalMultiplierEnable",      _ALL_BALORS )},
  {AndorFeature( "GlobalShutterShortExposureExteralTriggerReferenceRead", _ALL_BALORS )},
  {AndorFeature( "HDRTestMode",                 _ALL_BALORS )},
  {AndorFeature( "ImageSizeBytes",              _ALL_CAMERAS )},
  {AndorFeature( "InterfaceType",               _ALL_CAMERAS )},
  {AndorFeature( "IOInvert",                    _ALL_CAMERAS )},
  {AndorFeature( "IOSelector",                  _ALL_CAMERAS )},
  {AndorFeature( "IRIGClockFrequency",          _ALL_BALORS )},
  {AndorFeature( "LinearityOffsetCorrection",   _ALL_BALORS )},
  {AndorFeature( "LineScanSpeed",               _ZYLA )},
  {AndorFeature( "LogLevel",                    _ALL_BALORS )},
  {AndorFeature( "LongExposureTransition",      _ALL_CAMERAS )},
  {AndorFeature( "MaxInterfaceTransferRate",    _ALL_CAMERAS )},
  {AndorFeature( "MetadataEnable",              _ALL_CAMERAS )},
  {AndorFeature( "MetadataFrame",               _ALL_CAMERAS )},
  {AndorFeature( "MetadataFrameInfo",           _ALL_BALORS )},
  {AndorFeature( "MetadataIrig",                _ALL_BALORS )},
  {AndorFeature( "MetadataSize",                _ALL_BALORS )},
  {AndorFeature( "MetadataTimestamp",           _ALL_CAMERAS )},
  {AndorFeature( "MicrocodeVersion",            _NONE )},         // See NOTE 4
  {AndorFeature( "MultitrackBinned",            _ALL_CAMERAS )},
  {AndorFeature( "MultitrackCount",             _ALL_CAMERAS )},
  {AndorFeature( "MultitrackEnd",               _ALL_CAMERAS )},
  {AndorFeature( "MultitrackSelector",          _ALL_CAMERAS )},
  {AndorFeature( "MultitrackStart",             _ALL_CAMERAS )},
  {AndorFeature( "Overlap",                     _ZYLA )},
  {AndorFeature( "PixelEncoding",               _ALL_CAMERAS )},
  {AndorFeature( "PixelHeight",                 _ALL_CAMERAS )},
  {AndorFeature( "PixelProcCorrection",         _ALL_BALORS )},
  {AndorFeature( "PixelReadoutRate",            _ALL_CAMERAS )},
  {AndorFeature( "PixelWidth",                  _ALL_CAMERAS )},
  {AndorFeature( "ReadTemperatureChannel",      _ALL_BALORS )},
  {AndorFeature( "ReadTemperatureValue",        _ALL_BALORS )},
  {AndorFeature( "ReadoutTime",                 _ALL_CAMERAS )},
  {AndorFeature( "ResetToDefaultValues",        _ALL_BALORS )},
  {AndorFeature( "RollingShutterGlobalClear",   _NONE )},         // See NOTE 1
  {AndorFeature( "RowNExposureEndEvent",        _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "RowNExposureStartEvent",      _BALOR_REV_A )},  // See NOTE 2, 5
  {AndorFeature( "RowReadTime",                 _ALL_CAMERAS )},
  {AndorFeature( "ScanSpeedControlEnable",      _NONE )},         // See NOTE 1
  {AndorFeature( "SensorCooling",               _ALL_CAMERAS )},
  {AndorFeature( "SensorHeight",                _ALL_CAMERAS )},
  {AndorFeature( "SensorInitialised",           _ALL_BALORS )},
  {AndorFeature( "SensorReadoutMode",           _NONE )},         // See NOTE 1
  {AndorFeature( "SensorTemperature",           _ALL_CAMERAS )},
  {AndorFeature( "SensorType",                  _ALL_CAMERAS )},
  {AndorFeature( "SensorWidth",                 _ALL_CAMERAS )},
  {AndorFeature( "SerialNumber",                _ALL_CAMERAS )},
  {AndorFeature( "ShutterMode",                 _ALL_CAMERAS )},
  {AndorFeature( "ShutterOutputMode",           _ZYLA )},
  {AndorFeature( "ShutterTransferTime",         _ALL_CAMERAS )},
  {AndorFeature( "SimplePreAmpGainControl",     _ALL_CAMERAS )},
  {AndorFeature( "SoftwareTrigger",             _ALL_CAMERAS )},
  {AndorFeature( "SpuriousNoiseFilter",         _ALL_CAMERAS )},
  {AndorFeature( "StaticBlemishCorrection",     _ALL_CAMERAS )},
  {AndorFeature( "TargetSensorTemperature",     _ALL_CAMERAS )},
  {AndorFeature( "TemperatureControl",          _ALL_CAMERAS )},
  {AndorFeature( "TemperatureStatus",           _ALL_CAMERAS )},
  {AndorFeature( "TECCurrent",                  _ALL_BALORS )},
  {AndorFeature( "TECVoltage",                  _ALL_BALORS )},
  {AndorFeature( "TimestampClock",              _ALL_CAMERAS )},
  {AndorFeature( "TimestampClockFrequency",     _ALL_CAMERAS )},
  {AndorFeature( "TimestampClockReset",         _ALL_CAMERAS )},
  {AndorFeature( "TriggerMode",                 _ALL_CAMERAS )},
  {AndorFeature( "TriggerSource",               _ALL_BALORS )},
  {AndorFeature( "VerticallyCentreAOI",         _ALL_CAMERAS )}
};


//------------------------------------------------------------------------------
// MasterListOfFeatures: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//

//---------------------------- getAllFeatures() --------------------------------
//
const std::vector<AndorFeature> MasterListOfFeatures::getAllFeatures()
{
  return listOfFeatures;
}
// End of AndorFeature::getAllFeatures()


//---------------------------- getCamFeatures() --------------------------------
//
const std::vector<AndorFeature> MasterListOfFeatures::getCamFeatures(
  const CamInfo& camInfo )
{
  std::vector<AndorFeature> list;

  AndorFeature::ImplementedOn camMask;
  if( camInfo.getCameraType() == ::CameraType::ZYLA )
    camMask.item.zyla = 1;
  else if( camInfo.getCameraType() == ::CameraType::BALOR )
    camMask.item.balor_revA = 1;

  for( AndorFeature feature : getAllFeatures() )
  {
    AndorFeature::ImplementedOn whichCameras = feature.getCameras();
    if( whichCameras.word & camMask.word )
      list.push_back( feature );
  }
  return list;
}
// End of AndorFeature::getCamFeatures()

//------------------------------------------------------------------------------
// END OF FILE: MasterListOfCameras.cpp
//------------------------------------------------------------------------------

