#ifndef ANDOR_METADATA_H
#define ANDOR_METADATA_H

#include <cstdint>

typedef union {
  std::uint64_t word;
  struct {
    std::uint32_t cid;
    std::uint32_t length;
  } field;
} MetadataHeader;

typedef union {
  std::uint64_t word;
  struct {
    std::uint16_t stride;
    std::uint8_t  pixelEncoding;
    std::uint8_t  unused;
    std::uint16_t aoiWidth;
    std::uint16_t aoiHeight;
  } field;
} MetadataFrameInfo;

typedef struct {
  std::uint64_t length;
} MetadataData;

typedef struct {
  MetadataData data;
  MetadataFrameInfo frameInfo;
  std::uint64_t timestamp;
} Metadata;

class AndorMetadata {
  private:
    // Metadata constants
    static const int LENGTH_FIELD_SIZE={4};
    static const int CID_FIELD_SIZE={4};
    static const int TIMESTAMP_FIELD_SIZE={8};
    static const int FRAMEINFO_FIELD_SIZE={8};
    static const int CID_FRAME={0};
    static const int CID_TIMESTAMP={1};
    static const int CID_FRAMEINFO={7};

  public:
    static Metadata parse(
      unsigned char *image_data, int image_size, bool verbose);
};


#endif    // End of ANDOR_METADATA_H

//------------------------------------------------------------------------------
// END OF FILE: Metadata.h
//------------------------------------------------------------------------------

