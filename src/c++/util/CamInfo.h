#ifndef CAMINFO_H
#define CAMINFO_H

#include "atcore.h"

#include "constants.h"
#include "AoiRoi.h"

#include <iomanip>
#include <set>
#include <string>
#include <vector>

//==============================================================================
// CLASS: CamInfo
//==============================================================================

class CamInfo
{
  private:
    AT_H H;                                     // Device handle
    ::CameraType cameraType;                    // General type of camera
    int sensorWidth;                            // As reported by camera
    int sensorHeight;                           // As reported by camera
    bool balorRevA;                             // Determined by S/N
    bool balorRevB;                             // Determined by S/N
    std::vector<std::string> gainModes;         // Supported/used by DKIST
    std::vector<std::string> shutterModes;      // Supported/used by DKIST
    std::vector<std::string> allTriggerModes;   // All modes of camera
    std::vector<std::string> cssTriggerModes;   // Supported/used by DKIST
    AOI fullFrameAOI;
    ROI fullFrameROI;
    std::set<std::string> balorRevASerialNumbers {
      "BSC-00014",
      "BSC-00024", "BSC-00026", "BSC-00027",
      "BSC-00041", "BSC-00049", "BSC-00050"
    };
    std::set<std::string> balorRevBSerialNumbers = {};

  public:
    CamInfo();
    CamInfo( AT_H handle, const ::CameraType& type, const int w, const int h );

    ::CameraType getCameraType() const;
    AT_H getHandle() const;
    std::vector<std::string> getGainModes() const;
    std::vector<std::string> getShutterModes() const;
    std::vector<std::string> getAllTriggerModes() const;
    std::vector<std::string> getCssTriggerModes() const;
    int getSensorWidth() const;
    int getSensorHeight() const;
    AOI getFullFrameAOI() const;
    ROI getFullFrameROI() const;
    bool isBalorRevA() const;
    bool isBalorRevB() const;
};


#endif    // End of CAMINFO_H

//------------------------------------------------------------------------------
// END OF FILE: CamInfo.h
//------------------------------------------------------------------------------
