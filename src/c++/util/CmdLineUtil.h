#include <sstream>
#include <vector>

class CommandLineError
{
  private: 
    std::string errMsg;

  public: 
    CommandLineError( std::string errMsg ) : errMsg(errMsg) { }
    std::string what() const { return errMsg; }
};

class CmdLineUtil
{
  public:
    static void cmdLineError(char* progname, std::string errMsg);

    static void cmdLineError(char* progname, std::ostringstream& errMsg);

    static std::vector<int> getIntChoices( 
      std::string option,    // The command line option flag
      std::string values,    // The string of values following the '=' sign
      int maxValue );        // The maximum number that can be specified

};