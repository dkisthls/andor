#include "AoiRoi.h"

#include <iomanip>
#include <sstream>

//==============================================================================
// CLASS: AOI
//==============================================================================

//------------------------------------------------------------------------------
// AOI: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//
AOI::AOI() :
  left(1), top(1), width(1), height(1), desc("UNDEF") {}
// End of Constructor #1


//============================= CONSTRUCTOR #2 =================================
//
AOI::AOI(
  const int l, const int t, const int w, const int h, const std::string& d) :
  left(l), top(t), width(w), height(h), desc(d) {}
// End of Constructor #2


//----------------------------- operator<<(AOI) --------------------------------
//
std::ostream& operator<<( std::ostream& os, const AOI& rhs )
{
  os << rhs.desc << ": "
     << "Left=" << std::right << std::setw(4) << rhs.left
     << ", Top=" << std::right << std::setw(4) << rhs.top
     << ", Width=" << std::right << std::setw(4) << rhs.width
     << ", Height=" << std::right << std::setw(4) << rhs.height;
  return os;
}
// End of operator<<(AOI)


//----------------------------- operator=(AOI) ---------------------------------
//
AOI& AOI::operator=(const AOI& rhs)
{
  if( this == &rhs )            // check for assignment to self
    return *this;
  left   = rhs.left;
  top    = rhs.top;
  width  = rhs.width;
  height = rhs.height;
  desc   = rhs.desc;
  return *this;
}
// End of AOI::operator=(AOI)


//- --------------------------- toSimpleString()--------------------------------
//
std::string AOI::toSimpleString()
{
  std::ostringstream msgStr;
  msgStr << std::right << std::setw(4) << left
         << ", " << std::right << std::setw(4) << top
         << ", " << std::right << std::setw(4) << width
         << ", " << std::right << std::setw(4) << height;
  return msgStr.str();
}
// End of AOI::toSimpleString()


//==============================================================================
// CLASS: ROI
//==============================================================================

//------------------------------------------------------------------------------
// ROI: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//
ROI::ROI() : originX(0), originY(0), sizeX(1), sizeY(1), desc("UNDEF") {}
// End of Constructor #1


//============================= CONSTRUCTOR #2 =================================
//
ROI::ROI(
  const int oX, const int oY, const int sX, const int sY, const std::string& d) :
  originX(oX), originY(oY), sizeX(sX), sizeY(sY), desc(d) {}
// End of Constructor #2


//----------------------------- operator<<(ROI) --------------------------------
//
std::ostream& operator<<( std::ostream& os, const ROI& rhs )
{
  os << rhs.desc << ": "
     << "OriginX=" << std::right << std::setw(4) << rhs.originX
     << ", OriginY=" << std::right << std::setw(4) << rhs.originY
     << ", SizeX=" << std::right << std::setw(4) << rhs.sizeX
     << ", SizeY=" << std::right << std::setw(4) << rhs.sizeY;
  return os;
}
// End of operator<<(ROI)


//----------------------------- operator=(ROI) ---------------------------------
//
ROI& ROI::operator=( const ROI& rhs )
{
  if( this == &rhs )            // check for assignment to self
    return *this;
  originX = rhs.originX;
  originY = rhs.originY;
  sizeX   = rhs.sizeX;
  sizeY   = rhs.sizeY;
  desc    = rhs.desc;
  return *this;
}
// End of ROI::operator=(ROI)


//- --------------------------- toSimpleString()--------------------------------
//
std::string ROI::toSimpleString()
{
  std::ostringstream msgStr;
  msgStr << std::right << std::setw(4) << originX
         << ", " << std::right << std::setw(4) << originY
         << ", " << std::right << std::setw(4) << sizeX
         << ", " << std::right << std::setw(4) << sizeY;
  return msgStr.str();
}
// End of ROI::toSimpleString()

//------------------------------------------------------------------------------
// END OF FILE: AoiRoi.cpp
//------------------------------------------------------------------------------
