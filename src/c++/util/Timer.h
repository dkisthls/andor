/*
 * $Id: Timer.h,v 1.1 2017/09/29 20:35:09 cberst Exp $
 *
 * Copyright 2013 Advanced Technology Solar Telescope,
 * National Solar Observatory, operated by the Association of Universities
 * for Research in Astronomy, Inc., under cooperative agreement with the
 * National Science Foundation.
 *
 * This copy of ATST software is licensed to you under the terms
 * described in the ATST_LICENSE file included in this distribution.
 *
 * Created: Sep 17, 2013
 *
 * Author: Chris Berst (cberst@nso.edu)
 */
#ifndef ATST_CSS_UTIL_TIMER_H
#define ATST_CSS_UTIL_TIMER_H

#include <chrono>

class Timer
{
  private:
    //!< The timeout value supplied when the timer was created.
    int timeout_value_in_ms;

    std::chrono::time_point<std::chrono::steady_clock> time_point_start;
    std::chrono::time_point<std::chrono::steady_clock> time_point_stop;

    //!< The difference between the stopPoint and startPoint.
    double diff;

  public:
    Timer();
    Timer(int timeout_ms);
    ~Timer() {;}

    std::chrono::time_point<std::chrono::steady_clock> curr()
    {
      return std::chrono::steady_clock::now();
    }
    void start(int timeout_ms = 0);
    void stop();
    double get_dt() { return diff; }
    double get_elapsed(); 
    bool didTimeout();
    void setTimeoutValue(int timeout_ms) {
      timeout_value_in_ms = timeout_ms; }
};

#endif    // End of ATST_CSS_UTIL_TIMER_H

//------------------------------------------------------------------------------
// END OF FILE: Timer.h
//------------------------------------------------------------------------------

