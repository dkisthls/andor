#ifndef ZYLA_CALCULATOR_H
#define ZYLA_CALCULATOR_H

#include "atcore.h"

#include "AoiRoi.h"
#include "CamInfo.h"

#include <cstdint>
#include <ostream>
#include <string>
#include <vector>

//==============================================================================
// CLASS: ZylaCalculator
//==============================================================================


class ZylaCalculator {
  private:
    // Zyla specific constants
    // From Andor email June 12, 2019
    static constexpr double _100MHz = {103.3};     // Actual clock speed for PixelReadoutRate = "100 MHz"
    static constexpr double _280MHz = {284.0};     // Actual clock speed for PixelReadoutRate = "280 MHz"
    // End from Andor email

    static constexpr double _CLOCK_PERIOD = {1.0 / (_280MHz * 1000000.0)};
    static constexpr double _ROW_READ_TIME = {2624 * _CLOCK_PERIOD};
    static constexpr double _INTERFRAME = {9 * _ROW_READ_TIME};
    static constexpr double _CHARGE_TRANSFER_TIME = {0.000002};


    bool debugEnabled;
    bool fastAOIFrameRateEnable;
    int sensorHeight;
    int sensorWidth;
    std::string shutterMode;
    std::string triggerMode;

  public:

    ZylaCalculator( const CamInfo& camInfo, bool debugEnabled );
    // End of Constructor #1

    std::int64_t calculateImageSizeBytes(
      const AOI& aoi,
      const std::int64_t aoiStride );

    std::int64_t calculateImageSizeBytes(
      const ROI& roi,
      const std::int64_t aoiStride );

    std::int64_t calculateImageSizeBytes(
      const std::vector<AOI>& vAoi,
      const std::int64_t aoiStride );

    std::int64_t calculateImageSizeBytes(
      const std::vector<ROI>& vRoi,
      const std::int64_t aoiStride );

    double calculateLET(
      const double& readoutTime_s );

    double calculateReadoutTime(
      const int& numRowReads );

    int calculateNumRowReads(
      const AOI& aoi,
      std::string& pathTaken );

    int calculateNumRowReads(
      const ROI& roi,
      std::string& pathTaken );

    int calculateNumRowReads(
      const std::vector<AOI>& vAoi,
      std::string& pathTaken );

    int calculateNumRowReads(
      const std::vector<ROI>& vRoi,
      std::string& pathTaken );

    double getRowReadTime() const;
    void setFastAOIFrameRateEnable( const bool enb );
    void setShutterMode( const std::string& mode );
    void setTriggerMode( const std::string& mode );
};

#endif    // End of ZYLA_CALCULATOR_H

//------------------------------------------------------------------------------
// END OF FILE: AoiRoi.h
//------------------------------------------------------------------------------
