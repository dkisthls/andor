#include "AndorFeature.h"

#include <iomanip>
#include <sstream>

//==============================================================================
// CLASS: AndorFeature
//==============================================================================

//------------------------------------------------------------------------------
// AndorFeature: PROTECTED
//------------------------------------------------------------------------------

//----------------- operator<<(AndorFeature::ImplementedOn) --------------------
//
std::ostream& operator<<(
  std::ostream& os, const AndorFeature::ImplementedOn& rhs )
{
  os << "Implemented On: " ;

  if( rhs.word == 0 )
    os << " -  ,        -       ";
  else
  {
    if( rhs.item.zyla )
      os << "Zyla";
    else
      os << " -  ";

    if( (rhs.word & ::_ALL_BALORS) == ::_ALL_BALORS )
      os << ",   All Balors   ";
    else if( rhs.item.balor_revA )
      os << ", Balor-RevA Only";
    else if( rhs.item.balor_revB )
      os << ", Balor-RevB Only";
    else
      os << ",        -       ";
  }
  return os;
}
// End of operator<<(AndorFeature::ImplementedOn)


//------------------------------------------------------------------------------
// AndorFeature: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//
// AndorFeature::AndorFeature( std::string name, ImplementedOn where ) :
//   featureName( name ),
//   where( where ) {};
AndorFeature::AndorFeature( std::string name, std::uint16_t where ) :
  featureName( name )
{
  this->where.word = where;
}
// End of Constructor #1


//------------------------ operator<<(AndorFeature) ----------------------------
//
std::ostream& operator<<( std::ostream& os, const AndorFeature& rhs )
{
  if( rhs.featureName ==
      "GlobalShutterShortExposureExteralTriggerReferenceRead" )
    os << "GlobalShutterShortExposure-" << std::endl
       << std::setw(27) << std::right << "ExteralTriggerReferenceRead, "
       << rhs.where;
  else
    os << std::setw(27) << std::right << rhs.featureName << ", " << rhs.where;
  return os;
}
// End of operator<<(AndorFeature)


//--------------------------------- name() -------------------------------------
//
const std::string AndorFeature::name()
{
  return featureName;
}
// End of AndorFeature::name()


//------------------------------ getCameras() ----------------------------------
//
const AndorFeature::ImplementedOn AndorFeature::getCameras()
{
  return where;
}
// End of AndorFeature::getCameras()

//------------------------------------------------------------------------------
// END OF FILE: MasterListOfCameras.cpp
//------------------------------------------------------------------------------
