#include "ReadWriteReadOnly.h"

#include <iostream>

//==============================================================================
// CLASS: R_W_RO
//==============================================================================

//------------------------------------------------------------------------------
// R_W_RO: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
// Default constructor.
//
R_W_RO::R_W_RO() :
  readable(AT_FALSE),
  writable(AT_FALSE),
  readonly(AT_FALSE) {}
// End of Constructor #1

//============================= CONSTRUCTOR #2 =================================
// Std constructor.
//
R_W_RO::R_W_RO(
  const AT_BOOL r, const AT_BOOL w, const AT_BOOL ro) :
  readable(r),
  writable(w),
  readonly(ro) {}
// End of Constructor #2

//============================= CONSTRUCTOR #3 =================================
// Copy constructor.
//
R_W_RO::R_W_RO( const R_W_RO& lhs )
{
  *this = lhs;
}
// End of Constructor #3

//------------------------------- operator=() ----------------------------------
//
R_W_RO& R_W_RO::operator=( const R_W_RO& rhs )
{
  if( this == &rhs )            // check for assignment to self
    return *this;

  readable = rhs.readable;
  writable = rhs.writable;
  readonly = rhs.readonly;

  return *this;
}
// End of R_W_RO::operator=()

//------------------------------------------------------------------------------
// END OF FILE: ReadWriteReadOnly.cpp
//------------------------------------------------------------------------------
