#ifndef ANDOR_FEATURE_H
#define ANDOR_FEATURE_H

#include <cstdint>         // std::uint16_t
#include <ostream>
#include <string>

//==============================================================================
// CLASS: AndorFeature
//==============================================================================


static const std::uint16_t _NONE        = 0;
static const std::uint16_t _BALOR_REV_A = 1;
static const std::uint16_t _BALOR_REV_B = 2;
static const std::uint16_t _ZYLA        = 4;
static const std::uint16_t _ALL_CAMERAS = _BALOR_REV_A + _BALOR_REV_B + _ZYLA;
static const std::uint16_t _ALL_BALORS  = _BALOR_REV_A + _BALOR_REV_B;


class AndorFeature {
  public:
    typedef union ImplementedOn {
      std::uint16_t word;
      struct {
        std::uint16_t balor_revA : 1;
        std::uint16_t balor_revB : 1;
        std::uint16_t zyla       : 1;
        std::uint16_t            : 13;   // UNUSED
      } item;
    } ImplementedOn;

  private:
    std::string featureName;
    ImplementedOn where;

  protected:
    /**
     * Inserts an enumerated ImplementedOn onto an output stream in a human
     * readable format.
     * @param os The output stream being inserted into.
     * @param rhs The AndorFeature to be inserted onto the output stream as a
     *            string.
     */
    friend std::ostream& operator<<(
      std::ostream& os, const AndorFeature::ImplementedOn& rhs );

  public:
    // AndorFeature(
    //   std::string name, AndorFeature::ImplementedOn where);
    AndorFeature( std::string name, std::uint16_t where );

    /**
     * Inserts a AndorFeature onto an output stream in a human readable format.
     * @param os The output stream being inserted into.
     * @param rhs The Featureto be inserted onto the output stream as a string.
     */
    friend std::ostream& operator<<( std::ostream& os, const AndorFeature& rhs );

    const std::string name();
    const AndorFeature::ImplementedOn getCameras();
};

#endif    // End of ANDOR_FEATURE_H

//------------------------------------------------------------------------------
// END OF FILE: AndorFeature.h
//------------------------------------------------------------------------------
