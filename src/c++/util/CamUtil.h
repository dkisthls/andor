#ifndef CAM_UTIL_H
#define CAM_UTIL_H

#include "atcore.h"
#include "constants.h"
#include "AoiRoi.h"
#include "CamInfo.h"

#include <iomanip>
#include <memory>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

typedef std::unique_ptr<AT_U8[]> image_t;

//uncomment the line below to print the SDK calls and results from each call
//#define PRINTSDKCALLS

#define CAT(x, y)  x##y
#define WIDE(x)    CAT(L,x)

#ifdef PRINTSDKCALLS
#define RETURN_ON_ERROR(command) { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result <<std::endl; if(result!=AT_SUCCESS) return result; }
#define WARN_ON_ERROR(command)   { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result<<std::endl;}
#else
#define RETURN_ON_ERROR(command) { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl; return result;} }
#define WARN_ON_ERROR(command)   { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl;} }
#endif

class CamUtil {
  public:
    static std::wstring string2wcs( const std::string& val );
    static std::string wcs2string( const AT_WC* wcs );

    static int AllocateAndQueueBuffers(AT_H H, std::vector<image_t>& frameBuffers);
    static ROI Aoi2Roi(const AOI& aoi, const int sensorHeight);
    static AOI Roi2Aoi(const ROI& roi, const int sensorHeight);
    static int DoGenericConfiguration(AT_H H, const std::string& gainMode,
                                              const std::string& shutterMode);

    static int IdentifyCamera(AT_H, ::CameraType& cameraType);
    static int OpenAndIdentify( CamInfo& info );

    static int SetBasicConfiguration(const CamInfo& camInfo);

    static int GetString(AT_H H, const AT_WC* feature, std::string& value);

    static void PrintMono16Frame(int frameNumber, unsigned char* frameP,
                                 int imageSizeBytes, int aoistride,
                                 int pixelCount, int rowCount);

    static int PrintBoolFeature(AT_H H, const std::wstring& feature);
    static int PrintEnumFeature(AT_H H, const AT_WC* feature);
    static int PrintEnumFeatureIndexInfo(AT_H H, const AT_WC* feature, int enumIndex);
    static int PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision=6);
    static int PrintFloatFeatureMinMax(AT_H H, const AT_WC* feature, const int precision=6);
    static int PrintIntFeature(AT_H H, const AT_WC* feature);
    static int PrintIntFeatureMinMax(AT_H H, const AT_WC* feature);
    static int PrintStringFeature(AT_H H, const AT_WC* feature);
    static int PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value);
    static int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value);
    static int PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value);
    static int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value);
    static int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value, const bool inHex);
    static int PrintIsReadable(AT_H H, const std::wstring& feature);
    static int PrintIsImplemented(AT_H H, const std::wstring& feature);
    static int PrintIsImplemented(AT_H H, const AT_WC* feature);
    static int PrintIsReadOnly(AT_H H, const std::wstring& feature);
    static int PrintIsWritable(AT_H H, const std::wstring& feature);

    static int WaitForCorrectionApplying(AT_H H, int timeout_ms);
    static int WaitForSensorInitialisation(AT_H H);
};
// End of class CamUtil

#endif    // End of CAM_UTIL_H

//------------------------------------------------------------------------------
// END OF FILE: CamUtil.h
//------------------------------------------------------------------------------

