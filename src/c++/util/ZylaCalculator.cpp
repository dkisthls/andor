#include "ZylaCalculator.h"
#include "CamUtil.h"

#include <iomanip>
#include <iostream>
#include <sstream>

//==============================================================================
// CLASS: ZylaCalculator
//==============================================================================

//------------------------------------------------------------------------------
// ZylaCalculator: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//
ZylaCalculator::ZylaCalculator(
  const CamInfo& camInfo,
  bool debugEnabled ) :
  debugEnabled( debugEnabled ),
  fastAOIFrameRateEnable( false ),
  sensorHeight( camInfo.getSensorHeight() ),
  sensorWidth( camInfo.getSensorWidth() ),
  shutterMode( "Global" ),
  triggerMode( "Internal" ) {};
// End of Constructor #1


//----------------------- calculateImageSizeBytes(AOI) -------------------------
//
std::int64_t ZylaCalculator::calculateImageSizeBytes(
  const AOI& aoi,
  const std::int64_t aoiStride )
{
  std::vector<ROI> vRoi( 1, CamUtil::Aoi2Roi( aoi, sensorHeight) );
  return calculateImageSizeBytes( vRoi, aoiStride );
}
// End of ZylaCalculator::calculateImageSizeBytes(AOI)


//----------------------- calculateImageSizeBytes(ROI) -------------------------
//
std::int64_t ZylaCalculator::calculateImageSizeBytes(
  const ROI& roi,
  const std::int64_t aoiStride )
{
  std::vector<ROI> vRoi( 1, roi );
  return calculateImageSizeBytes( vRoi, aoiStride );
}
// End of ZylaCalculator::calculateImageSizeBytes(ROI)


//------------------------ calculateImageSizeBytes() ---------------------------
//
std::int64_t ZylaCalculator::calculateImageSizeBytes(
  const std::vector<AOI>& vAoi,
  const std::int64_t aoiStride )
{
  if(vAoi.size() == 0 )
  {
    std::cout << "ERROR: calculateImageSizeBytes(vAoi): vAOI size="
              << vAoi.size() << std::endl;
    return !0;
  }

  // Create vector of ROIs from AOIs.
  // NOTE: The order is inverted due to CSS origin(0,0) vs Sensor origin(1,1)
  //       (CSS origin is lower left, Sensor origin is upper left)
  std::vector<ROI> vRoi;
  for( auto it=vAoi.rbegin(); it != vAoi.rend(); it++ )
    vRoi.emplace_back( CamUtil::Aoi2Roi( *it, sensorHeight ) );

  return calculateImageSizeBytes( vRoi, aoiStride );
}
// End of ZylaCalculator::calculateImageSizeBytes(vector<AOI>)


//------------------------ calculateImageSizeBytes() ---------------------------
//
std::int64_t ZylaCalculator::calculateImageSizeBytes(
  const std::vector<ROI>& vRoi,
  const std::int64_t aoiStride )
{
  int numRows(0);

  if( vRoi.size() == 0 )
  {
    std::cout << "ERROR: calculateImageSizeBytes(vRoi): vRoi size="
              << vRoi.size() << std::endl;
    return 0;
  }

  for( ROI roi : vRoi )
    numRows += roi.sizeY;

  if( numRows % 2 ) numRows++;

  return (numRows * aoiStride);
}
// End of ZylaCalculator::calculateImageSizeBytes(vector<AOI>)


//----------------------------- calculateLET() ---------------------------------
//
double ZylaCalculator::calculateLET( const double& readoutTime_s )
{
  if( debugEnabled )
  {
    std::cout << "calculateLET( "
              << std::fixed << std::setprecision(16)
              << "readoutTime_s=" << readoutTime_s << " )" << std::endl;
  }

  return readoutTime_s + (3.5 * _ROW_READ_TIME);
}
// End of ZylaCalculator::calculateLET()


//------------------------- calculateReadoutTime() -----------------------------
//
double ZylaCalculator::calculateReadoutTime( const int& numRowReads )
{
  if( debugEnabled )
  {
    std::cout << "calcuateReadoutTime( "
              << std::fixed << std::setprecision(16)
              << "numRowReads=" << numRowReads << " )" << std::endl;
  }

  return numRowReads * _ROW_READ_TIME;  // in seconds
}
// End of ZylaCalculator::calculateLET()


//------------------------ calculateNumRowReads(AOI) ---------------------------
//
int ZylaCalculator::calculateNumRowReads(
  const AOI& aoi,
  std::string& pathTaken )
{
  std::vector<ROI> ROIs( 1, CamUtil::Aoi2Roi( aoi, sensorHeight) );
  return calculateNumRowReads( ROIs, pathTaken );
}
// End of ZylaCalculator::calculateLET(AOI)


//------------------------ calculateNumRowReads(ROI) ---------------------------
//
int ZylaCalculator::calculateNumRowReads(
  const ROI& roi,
  std::string& pathTaken )
{
  std::vector<ROI> ROIs( 1, roi );
  return calculateNumRowReads( ROIs, pathTaken );
}
// End of ZylaCalculator::calculateLET(ROI)


//------------------------ calculateNumRowReads() ---------------------------
//
int ZylaCalculator::calculateNumRowReads(
  const std::vector<AOI>& vAoi,
  std::string& pathTaken )
{
  if(vAoi.size() == 0 )
  {
    std::cout << "ERROR: calculateNumRowReads(vAoi): vAOI size="
              << vAoi.size() << std::endl;
    return !0;
  }

  // Create vector of ROIs from AOIs.
  // NOTE: The order is inverted due to CSS origin(0,0) vs Sensor origin(1,1)
  //       (CSS origin is lower left, Sensor origin is upper left)
  std::vector<ROI> vRoi;
  for( auto it=vAoi.rbegin(); it != vAoi.rend(); it++ )
    vRoi.emplace_back( CamUtil::Aoi2Roi( *it, sensorHeight ) );

  return calculateNumRowReads( vRoi, pathTaken );
}
// End of ZylaCalculator::calculateLET(ROI)


//------------------------- calculateNumRowReads() -----------------------------
//
int ZylaCalculator::calculateNumRowReads(
  const std::vector<ROI>& vRoi,
  std::string& pathTaken )
{
  int half_1(0);
  int half_2(0);
  int halfHeight( sensorHeight/2 );
  int numRowReads(0);
  int numROIs = vRoi.size();
  ROI roi_1 = vRoi[0];
  ROI roi_N = vRoi[numROIs-1];

  if( vRoi.size() == 0 )
  {
    std::cout << "ERROR: calculateNumRowReads(vRoi): vRoi size="
              << vRoi.size() << std::endl;
    return 0;
  }

  // For purposes of this algorigm to calculate the number of rowReads,
  // we use the ROI rather than AOI. ROIs are required to be defined in
  // increasing row order. Look at the originY and swap if necessary.

  if( debugEnabled )
    std::cout << std::right << std::setw(4) << "ROI" << ", "
              << roi_1.toSimpleString() << ", "
              << roi_N.toSimpleString() << ", " << std::endl;

//========================= NEW
  // If number of ROIs == 1
  //   if origin + size < halfHeight || origin > halfHeight
  //     numRowReads == height
  //   else if origin + size > halfHeight
  //     numRowReads == max rows in sensor half
  if( numROIs == 1 )
  {
    if( roi_1.originY + roi_1.sizeY <= halfHeight ||
        roi_1.originY >= halfHeight + 1 )
    {
      // ROI is fully contained in 1/2 of the sensor so just that number of rows
      // will be read.
      pathTaken = "USING A";
      numRowReads = roi_1.sizeY;
      half_1 = numRowReads;            // Debug
    }
    else
    {
      // ROI spans across the middle of the sensor. Determine which half has
      // the most number of rows.
      pathTaken = "USING B";
      half_1 = std::abs( halfHeight - roi_1.originY );
      half_2 = std::abs( halfHeight - (roi_1.originY + roi_1.sizeY) );
      numRowReads = std::max( half_1, half_2);
    }

    // For the Zyla, when the number of rows read are < 1/2 SensorHeight
    // then the camera always reads an even number of rows.
    if( numRowReads%2 != 0 )
      numRowReads++;         // Increment to make it even

    if( !fastAOIFrameRateEnable && numRowReads < halfHeight )
      numRowReads += 2;      // 2 more row reads when FastAOI is not enabled

  }
  else
  {
    // If Multitracks are fully contained in 1/2 of the sensor then just that
    // number of rows will be read.
    bool firstHalf =
      (roi_N.originY + roi_N.sizeY <= halfHeight) ? true : false;
    bool secondHalf =
      (roi_1.originY >= halfHeight + 1) ? true : false;

    if( firstHalf || secondHalf )
    {
      pathTaken = "USING C";

      numRowReads = (roi_N.originY + roi_N.sizeY) - roi_1.originY;

      if( numRowReads < 8 )
        numRowReads = 8;

      if( !fastAOIFrameRateEnable )
      {
        numRowReads += numROIs * 2;
        if( firstHalf && roi_1.originY == 0 )
        {
// Not sure about this case
// With no adjustment, calculator may have +2 or +4 rows more than camera
//           numRowReads -= 2;
        }
        else if( roi_N.originY + roi_N.sizeY == sensorHeight )
          numRowReads -= 2;
      }
      half_1 = numRowReads;            // Debug
    }
    else
    {
      pathTaken = "USING D";
      // Multitracks spans across the middle of the sensor. Determine which half
      // has the most number of rows.
      half_1 = std::abs( halfHeight - roi_1.originY );
      half_2 = std::abs( halfHeight - (roi_N.originY + roi_N.sizeY) );
      numRowReads = std::max( half_1, half_2);

      if( numRowReads < 8 )
        numRowReads = 8;

//      if( !fastAOIFrameRateEnable )
//        numRowReads += 2;
      if( !fastAOIFrameRateEnable )
      {
        numRowReads += 2;
//        if( roi_N.originY + roi_N.sizeY - 1== sensorHeight )
        if( roi_1.originY < 2 )
          numRowReads -= 2;
      }
    }

    if( numRowReads%2 != 0)
      numRowReads++;
  }

  if( debugEnabled )
  {
    std::cout << "Rows in half_1=" << half_1
              << ", Rows in half_2=" << half_2
              << ", numRowReads=" << numRowReads << std::endl;
  }

  return numRowReads;
}
// End of ZylaCalculator::calculateNumRowReads


//---------------------------- getRowReadTime() --------------------------------
//
double ZylaCalculator::getRowReadTime() const
{
  return _ROW_READ_TIME;
}
// End of ZylaCalculator::getRowReadTime()


//----------------------- setFastAOIFrameRateEnable() --------------------------
//
void ZylaCalculator::setFastAOIFrameRateEnable( const bool enb )
{
  fastAOIFrameRateEnable = enb;
}
// End of ZylaCalculator::setFastAOIFrameRateEnable()


//---------------------------- setShutterMode() --------------------------------
//
void ZylaCalculator::setShutterMode( const std::string& mode )
{
  shutterMode = mode;
}
// End of ZylaCalculator::setShutterMode()


//---------------------------- setTriggerMode() --------------------------------
//
void ZylaCalculator::setTriggerMode( const std::string& mode )
{
  triggerMode = mode;
}
// End of ZylaCalculator::setTriggerMode()


//------------------------------------------------------------------------------
// END OF FILE: ZylaCalculator.cpp
//------------------------------------------------------------------------------
