#ifndef MASTER_LIST_OF_FEATURES_H
#define MASTER_LIST_OF_FEATURES_H

#include "AndorFeature.h"
#include "CamInfo.h"

#include <vector>

//==============================================================================
// CLASS: MasterListOfFeatures
//==============================================================================

class MasterListOfFeatures {
  private:
    static const std::vector<AndorFeature> listOfFeatures;

  public:
    MasterListOfFeatures() {;}

    const std::vector<AndorFeature> getAllFeatures();
    const std::vector<AndorFeature> getCamFeatures( const CamInfo& camInfo );
};

#endif    // End of MASTER_LIST_OF_FEATURES_H

//------------------------------------------------------------------------------
// END OF FILE: MasterListOfCameras.cpp
//------------------------------------------------------------------------------
