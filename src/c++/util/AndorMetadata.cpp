#include "AndorMetadata.h"

#include <iostream>

//==============================================================================
// CLASS: AndorMetadata
//==============================================================================

//------------------------------------------------------------------------------
// AndorMetadata: PUBLIC
//------------------------------------------------------------------------------

//--------------------------------- parse() ------------------------------------
//
Metadata AndorMetadata::parse(unsigned char *image_data, int image_size, bool verbose)
{

  unsigned char* metaData = image_data + image_size;

  // Read 1st Metadata blob from end of buffer using MetadataHeader union
  MetadataHeader mdHeader;
  Metadata metadata;

  do {
    // Get CID/Length field of metadata blob
    mdHeader.word = *(reinterpret_cast<std::uint64_t*>(metaData - (LENGTH_FIELD_SIZE + CID_FIELD_SIZE)));
    if( verbose )
      std::wcout << L"Metadata: CID=" << mdHeader.field.cid
                 << L" Length= " << mdHeader.field.length << std::endl;

    // Point to beginning of data for this metadata blob
    metaData = metaData - (mdHeader.field.length + LENGTH_FIELD_SIZE);

    switch( mdHeader.field.cid ) {
      case CID_FRAMEINFO:
        metadata.frameInfo.word = *(reinterpret_cast<std::uint64_t*>(metaData));
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=FrameInfo" << std::endl;
          std::wcout << L"  frameInfo.stride: " << metadata.frameInfo.field.stride << std::endl;
          std::wcout << L"  frameInfo.pixelEncoding: " << metadata.frameInfo.field.pixelEncoding << std::endl;
          std::wcout << L"  frameInfo.aoiWidth: " << metadata.frameInfo.field.aoiWidth << std::endl;
          std::wcout << L"  frameInfo.aoiHeight: " << metadata.frameInfo.field.aoiHeight << std::endl;
        }
        break;

      case CID_TIMESTAMP:
        metadata.timestamp = *(reinterpret_cast<std::uint64_t*>(metaData));
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=Timestamp" << std::endl;
          std::wcout << L"  timestamp: " << metadata.timestamp << std::endl;
        }
        break;

      case CID_FRAME:
        metadata.data.length = mdHeader.field.length - CID_FIELD_SIZE;
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=FrameData" << std::endl;
          std::wcout << L"  length=" << metadata.data.length << std::endl;
          std::wcout << L"  image_data=" << static_cast<void*>(image_data)
                     << " (original buffer pointer)" << std::endl;
          std::wcout << L"  metaData  =" << static_cast<void*>(metaData)
                     <<  " (ptr to beginning of CID 0 frame data)" << std::endl;
          if( image_data != metaData )
            std::wcout << L"ERROR - Mismatch between expected image data pointer"
                       << L"(image-data) and pointer derived from meta-data!";
        }
        break;

      default:
        if( verbose )
          std::wcout << L"Don't know how to handle CID=" << mdHeader.field.cid << std::endl;
    }
  } while( mdHeader.field.cid != 0 );

  return metadata;
}
// End of Metadata::parseMetadata()

