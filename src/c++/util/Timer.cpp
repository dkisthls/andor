/*
 * $Id: Timer.cpp,v 1.1 2017/09/29 20:35:09 cberst Exp $
 *
 * Copyright 2013 Advanced Technology Solar Telescope,
 * National Solar Observatory, operated by the Association of Universities
 * for Research in Astronomy, Inc., under cooperative agreement with the
 * National Science Foundation.
 *
 * This copy of ATST software is licensed to you under the terms
 * described in the ATST_LICENSE file included in this distribution.
 *
 * Created: Sep 17, 2013
 *
 * Author: Chris Berst (cberst@nso.edu)
 */

#include "Timer.h"

using namespace std::chrono;
using namespace std;

//==============================================================================
// CLASS: Timer
//
//------------------------------------------------------------------------------
//                          Timer: PRIVATE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//                          Timer: PROTECTED
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                          Timer: PUBLIC
//------------------------------------------------------------------------------

//------------------------------- Constructor ----------------------------------
//

Timer::Timer() : timeout_value_in_ms(0) { }
// End of Constructor #1


Timer::Timer(int timeout_ms) : timeout_value_in_ms(timeout_ms)
{
  // Initialize other data members...
  start();
}
// End of Constructor #2


//--------------------------------- start()  -----------------------------------
//

void Timer::start(int timeout_ms)
{
  if( timeout_ms > 0 )
    timeout_value_in_ms = timeout_ms;

  // Get steady_clock time-point and save.
  time_point_start = std::chrono::steady_clock::now();
}
// End of Timer::start()


//--------------------------------- stop()  ------------------------------------
//

void Timer::stop()
{
  // Get steady_clock time-point and save.
  time_point_stop = std::chrono::steady_clock::now();

  // Calculate difference in time. Result is in seconds
  diff = std::chrono::duration<double>(
      time_point_stop - time_point_start ).count();
}
// End of Timer::stop()


//------------------------------ didTimeout() -------------------------------
//

bool Timer::didTimeout()
{
  // Calculate difference in time between now and start...
  int dt = std::chrono::duration<double, std::milli>(
      std::chrono::steady_clock::now()- time_point_start ).count();

  return (dt > timeout_value_in_ms) ? true : false;
}
// End of Timer::didTimeout()


//----------------------------- get_elapsed() -------------------------------
//

double Timer::get_elapsed()
{
  // Calculate difference in time between now and start...
  double elapsed = std::chrono::duration<double>(
      std::chrono::steady_clock::now()- time_point_start ).count();

  return elapsed;
}
// End of Timer::get_elapsed()

//------------------------------------------------------------------------------
// END OF FILE: Timer.cpp
//------------------------------------------------------------------------------

