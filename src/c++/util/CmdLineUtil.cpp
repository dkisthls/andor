#include "CmdLineUtil.h"

#include <iostream>
#include <sstream>
#include <stdexcept>


//------------------------------ cmdLineError() --------------------------------
//
void CmdLineUtil::cmdLineError(char* progname, std::string errMsg)
{
  std::cout << std::endl <<  "*** Error - " << errMsg << " ***" << std::endl
            << "Use " << progname << " --help for valid options and values!"
            << std::endl << std::endl;
  exit(1);
}
// End of CmdLineUtil::cmdLineError()


//------------------------------ cmdLineError() --------------------------------
//
void CmdLineUtil::cmdLineError(char* progname, std::ostringstream& errMsg)
{
  cmdLineError( progname, errMsg.str() );
}
// End of CmdLineUtil::cmdLineError()


//------------------------------getIntChoices() --------------------------------
//
std::vector<int> CmdLineUtil::getIntChoices( 
  std::string option,    // The command line option flag
  std::string values,    // The string of values following the '=' sign
  int maxValue )         // The maximum number that can be specified
{
  int value;                            // A value to use
  char separator;                       // The separator between values
  std::ostringstream errMsg;            // For erro rreporting
  std::vector<int> choices;             // The return vector of specified values
  std::istringstream stream( values );  // Used to process command line string

  while( !(stream.fail() || stream.eof()) )
  {
    value = 0;
    stream >> separator >> value;
    if( value == 0 )
    {
      throw std::invalid_argument("Invalid or missing value(s)");
    }
    if( value < 1 || value > maxValue )
    {
      errMsg << "value(s) must be in the range 1 to " << maxValue;
      throw std::out_of_range( errMsg.str() );
    }
    choices.emplace_back( value );
  }
  if( stream.fail() )
  {
    throw std::invalid_argument("Invalid or missing value(s)");
  }

  return choices;
}
// End of CmdLineUtil::cmdLineGetIntChoices()


