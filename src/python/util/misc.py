#!/usr/bin/env python3

import math

def round_half_up(n, decimals=0):
    multiplier = 10.0 ** decimals
    return math.floor(n * multiplier + 0.5) / multiplier

