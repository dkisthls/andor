#!/usr/bin/env python3

#===============================================================================
# CLASS: Counts
# Simple class to encapsulate counts of number of tests, number of tests
# that pass, number of tests that fail, and number of tests with warnings.
#===============================================================================

class Counts():
    #--------------------------- __init__() ------------------------------------
    #
    def __init__(self):
        self.num_tests = 0
        self.num_pass = 0
        self.num_fail = 0
        self.num_warn = 0
    #
    # End of Counts.__init__()


    #--------------------------- __repr__() ------------------------------------
    #
    def __repr__(self):
        return f'Counts[{vars(self)}]'
    #
    # End of Counts.__repr__()

    #--------------------------- __str__() -------------------------------------
    #
    def __str__(self):
        return 'Counts: num_tests={}, num_pass={}, num_fail={}, num_warn={}'.format(
        self.num_tests, self.num_pass, self.num_fail, self.num_warn)
    #
    # End of Counts.__init__()

    #---------------------------- add_counts() ----------------------------------
    #
    def add_counts(self, counts):
        self.num_tests += counts.num_tests
        self.num_pass += counts.num_pass
        self.num_fail += counts.num_fail
        self.num_warn += counts.num_warn
    #
    # End of Counts.add_counts()


