#!/usr/bin/env python3

class Roi:
    def __init__(self, originX, originY, sizeX, sizeY):
        self.originX = originX
        self.originY = originY
        self.sizeX = sizeX
        self.sizeY = sizeY

    @classmethod
    def from_array(cls, ary):
        if not len(ary) == 4:
            raise IndexError("Array to create Roi must contain 4 elements!")

        return cls(originX=ary[0], originY=ary[1], sizeX=ary[2], sizeY=ary[3])

    @classmethod
    def from_aoi(cls, aoi, sensor_height):
        return cls(
            originX=aoi.left - 1,
            originY=sensor_height - aoi.height - aoi.top + 1,
            sizeX=aoi.width,
            sizeY=aoi.height)

    def __repr__(self):
        return f'Roi[{vars(self)}]'

    def __str__(self):
        return '({},{},{},{})'.format(
            self.originX, self.originY, self.sizeX, self.sizeY)

    def __format__(self, spec):
        if spec == '':
            return str(self)
        elif spec == 'cl':    # compact with legend
            return '({},{},{},{}) (originX,originY,sizeX,sizeY)'.format(
                self.originX, self.originY, self.sizeX, self.sizeY)
        elif spec == 'lp':    # left justified with padding
            return '{:<4}, {:<4}, {:<4}, {:<4}'.format(
                self.originX, self.originY, self.sizeX, self.sizeY)
        elif spec == 'rp':    # right justified with padding
            return '{:>4}, {:>4}, {:>4}, {:>4}'.format(
                self.originX, self.originY, self.sizeX, self.sizeY)
        elif spec == 'lpl':   # left justified with padding and legend
            return '{:<4}, {:<4}, {:<4}, {:<4} (originX,originY,sizeX,sizeY)'.format(
                self.originX, self.originY, self.sizeX, self.sizeY)
        else:
            raise NotImplementedError('unsupported format spec: {}'.format(spec))

    def to_array(self):
        return (self.originX, self.originY, self.sizeX, self.sizeY)


class Aoi:
    def __init__(self, left, top, width, height):
        self.left = left
        self.top = top
        self.width = width
        self.height = height

    @classmethod
    def from_array(cls, ary):
        if not len(ary) == 4:
            raise IndexError("Array to create Aoi must contain 4 elements!")

        return cls(left=ary[0], top=ary[1], width=ary[2], height=ary[3])

    @classmethod
    def from_roi(cls, roi, sensor_height):
        return cls(
            left=roi.originX + 1,
            top=sensor_height - (roi.originY + roi.sizeY) + 1,
            width=roi.sizeX,
            height=roi.sizeY)

    def __repr__(self):
        return f'Aoi[{vars(self)}]l'

    def __str__(self):
        return '({},{},{},{})'.format(
            self.left, self.top, self.width, self.height)

    def __format__(self, spec):
        if spec == '':
            return str(self)
        elif spec == 'cl':    # compact with legend
            return '({},{},{},{}) (left,top,width,height)'.format(
                self.left, self.top, self.width, self.height)
        elif spec == 'lp':    # left justified with padding
            return '{:<4}, {:<4}, {:<4}, {:<4}'.format(
                self.left, self.top, self.width, self.height)
        elif spec == 'rp':    # right justified with padding
            return '{:>4}, {:>4}, {:>4}, {:>4}'.format(
                self.left, self.top, self.width, self.height)
        elif spec == 'lpl':   # left justified with padding and legend
            return '{:<4}, {:<4}, {:<4}, {:<4} (left,top,width,height)'.format(
                self.left, self.top, self.width, self.height)
        else:
            raise NotImplementedError('unsupported format spec: {}'.format(spec))

    def to_array(self):
        return (self.left, self.top, self.width, self.height)
