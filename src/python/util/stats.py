import sys
import numpy as py

#===============================================================================
# CLASS: Statistics
# Simple class to encapsulate basic statics for min, max, avg, and some piece of
# data associated with the min/max values.
#===============================================================================

class Statistics():
    #--------------------------- __init__() ------------------------------------
    #
    def __init__(self):
        self.reset()
    #
    # End of Statistics.__init__()


    #--------------------------- __repr__() ------------------------------------
    #
    def __repr__(self):
        return f'Statistics[{vars(self)}]'
    #
    # End of Statistics.__repr__()


    #--------------------------- __str__() -------------------------------------
    #
    def __str__(self):
        return 'Statistics: min={}, max={}, sum={}, cnt={}, avg={}, min data={}, max data={}'.format(
        self._min, self._max, self._sum, self._cnt, (self._sum/self._cnt),
        self._data_for_min, self._data_for_max)
    #
    # End of Statistics.__init__()


    #------------------------------ reset() ------------------------------------
    #
    def reset(self):
        self._min = sys.float_info.max
        self._max = 0.0
        self._sum = 0.0
        self._cnt = 0
        self._data_for_min = 0.0
        self._data_for_max = 0.0
    #
    # End of Statistics.reset()


    #---------------------------- aggregate() ----------------------------------
    #
    def aggregate(self, val, data_for_val=0.0):
        if val < self._min:
            self._min = val
            self._data_for_min = data_for_val
        if val > self._max:
            self._max = val
            self._data_for_max = data_for_val
        self._sum += val
        self._cnt += 1
    #
    # End of Statistics.aggregate()


    #----------------------------- get_min() -----------------------------------
    #
    def get_min(self):
        return self._min
    #
    # End of Statistics.get_min()


    #----------------------------- get_max() -----------------------------------
    #
    def get_max(self):
        return self._max
    #
    # End of Statistics.get_max()


    #----------------------------- get_avg() -----------------------------------
    #
    def get_avg(self):
        return self._sum / self._cnt
    #
    # End of Statistics.get_avg()


    #------------------------ get_data_for_min() -------------------------------
    #
    def get_data_for_min(self):
        return self._data_for_min
    #
    # End of Statistics.get_data_for_min()


    #------------------------ get_data_for_max() -------------------------------
    #
    def get_data_for_max(self):
        return self._data_for_max
    #
    # End of Statistics.get_data_for_max()
