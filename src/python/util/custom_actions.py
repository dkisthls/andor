#!/usr/bin/env python3

import argparse

# Custom smart formatter that will allow newlines in formatted help.
# If help starts with 'R|' then the text is treated as "raw".
class SmartFormatter(argparse.HelpFormatter):
    def _split_lines(self, text, width):
        if text.startswith('R|'):
            return text[2:].splitlines()
        # this is the RawTextHelpFormatter._split_lines
        return argparse.HelpFormatter._split_lines(self, text, width)

class MultiFormatter(argparse.ArgumentDefaultsHelpFormatter,SmartFormatter):
    pass

# Custom action that can be used by the argument parser to check whether a
# supplied value is >= 0.
# Note that geq 0 is equivalent to using HasLimitsAction where
# minium=0, maximum=None. This is just clearer (I think).
class GEQZeroAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        if not value >= 0:
            raise argparse.ArgumentError(self, 'time value must be >= 0')
        setattr(namespace, self.dest, value)


# Custom action that can be used by the argument parser to check whether a
# supplied value is in defined bounds:
#  - If minimum != None and maximum != None then check: minimum <= value <= maximum
#  - If minimum == None then check: value <= maximum
#  - If maximum == None then check: value >= minimum
# Note that checks are separated to allow for custom error response based on
# whether this custom action is being used with a defined minimum, maximum or both.
class HasLimitsAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, const=None, default=None,\
                 type=None, choices=None, required=False, help=None, metavar=None,\
                 minimum=None, maximum=None):
        self.min = minimum
        self.max = maximum
        super(HasLimitsAction, self).__init__(\
            option_strings, dest, nargs, const, default,\
            type, choices, required, help, metavar)

    def __call__(self, parser, namespace, values, option_string=None):
        if not self.min is None and not self.max is None:
            if not values >= self.min or not values <= self.max:
                raise argparse.ArgumentError(self,
                    'value must be in the range {} <= value <= {}'.format(self.min,self.max))
        elif self.min is None and not self.max is None:
            if not values <= self.max:
                raise argparse.ArgumentError(self,
                    'value must be <= {}'.format(self.max))
        elif not self.min is None and self.max is None:
            if not values >= self.min:
                raise argparse.ArgumentError(self,
                    'value must be >= {}'.format(self.min))
        setattr(namespace, self.dest, values)


# Custom action that can be used by the argument parser to check whether a
# supplied value is a proper fraction in the range:
#        0.0 < value < 1.0
class PositiveFractionAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        if not (value > 0.0 and value < 1.0):
            raise argparse.ArgumentError(self,
                'value must be in the range 0.0 < value < 1.0')
        setattr(namespace, self.dest, value)


# Custom action that can be used by the argument parser to add an integer
# argument >= 0 to a dictionary. Use to process arguments of the form:
#     --arg KEY=VALUE [KEY=VALUE]...
class ParseIntDictGEQZeroAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        d = getattr(namespace, self.dest, {})

        if values:
            for item in values:
                split_items = item.split('=', 1)      # split key and value
                key = split_items[0].strip()          # keys are upper case
                value = int(split_items[1])           # convert to integer

                # Is this a valid key for the current argument?
                if not key in keys.CMDLINE_ARGS[self.dest]:
                    raise argparse.ArgumentError(self,
                        'invalid key for argument, valid keys are {}'.format(
                            keys.CMDLINE_ARGS[self.dest]))

                # Is value geq 0?
                if not value >= 0:
                    raise argparse.ArgumentError(self,
                        'time value must be >= 0, {}={}'.format(key, value))

                d[key] = value
        setattr(namespace, self.dest, d)

