###------------------------------ tcOptions ---------------------------------
class tcOptions:
    BALOR = 'balor'
    ZYLA  = 'zyla'
    CAMERAS = [BALOR, ZYLA]

    EXPOSURE_TIME = 'exposure_time'
    READOUT_TIME = 'readout_time'
    JIRA = 'jira'
    WHAT_TO_TEST = [EXPOSURE_TIME, READOUT_TIME, JIRA]

    TRIG_MODE_EXTERNAL          = 'External'
    TRIG_MODE_EXTERNAL_EXPOSURE = 'External Exposure'
    TRIG_MODE_EXTERNAL_START    = 'External Start'
    TRIG_MODE_INTERNAL          = 'Internal'
    TRIG_MODE_SOFTWARE          = 'Software'
    TRIGGER_MODES = [TRIG_MODE_INTERNAL, TRIG_MODE_SOFTWARE, TRIG_MODE_EXTERNAL]

###------------------------------ tcConstants ----------------------------------
class tcConstants:
    MIN_QUICK_CHECK = 0
    MAX_QUICK_CHECK = 100
    IDX_CALC = 0
    IDX_ACTUAL = 1
    DKIST_MAX_EXP_TIME = 30.0  # PER SPEC-0100 [3.6.1-0225], Exceeds SPEC by 20s

###------------------------------ tcDefaults -----------------------------------
class tcDefaults:
    QUICK_CHECK = 0
    START_WITH = 1
    TRIGGER_MODE = [tcOptions.TRIG_MODE_EXTERNAL]
    AOILAYOUT = 'Image'
    TRIGGER_DELAY = 0.0
    EXPOSURE_TIME = 0.1
