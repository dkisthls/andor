#!/usr/bin/env python3

import importlib

def load_calc_test_args(parser, test):
    module = importlib.import_module(test)
    module.add_arguments(parser)

