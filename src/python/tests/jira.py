#!/usr/bin/env python3

import sys
sys.path.append('../util')

import datetime
import time

from jira_tests import hlsops_1828
from aoi_roi import Aoi, Roi
from counts import Counts                              # Encapsulates test counters
from custom_actions import PositiveFractionAction
from tcDefined import tcConstants as const
from tcDefined import tcOptions as options

# For HLSOPS-1828: Exposure times will be stepped in fractions of RwoReadTime
# The default exposure time step size is (DEFAULT_STEP_SIZE * RowReadTime)
DEFAULT_STEP_SIZE = 0.1

# The set of JIRA test.
# JIRA tests are tests that were developed as a result of some issue
# encountered that required writing a specific test to illustrate the issue.
JIRA_DESCRIPTIONS = [
    'HLSOPS-1828 Balor: Camera/SDK reports incorrect Max FrameRate when Exposure Time == LongExposureTransition'
]

# Complete list of all test scenarios
ALL_JIRA_DESCRIPTIONS = JIRA_DESCRIPTIONS

# Scenarios are accessed by their integer place in the cumulative list.
# Get the total count and this will server as the default scenarios to test.
JIRAS_TO_TEST = range(1, len(ALL_JIRA_DESCRIPTIONS) + 1)

def add_arguments(parser):
    global JIRA_DESCRIPTIONS
    global JIRASS_TO_TEST

    jira_group = parser.add_argument_group('jira',
        description=('Arguments specific for executing tests that have been'
                     'captured in an HLSOPS JIRA ticket.\n'))

    help_str = ('R|Used to select one or more specific JIRA tests.\n'
                'Use the integer index value for making a selection:\n')

    help_str += 'JIRA TESTS\n'
    for index, value in enumerate(ALL_JIRA_DESCRIPTIONS):
        help_str += '    {}) {}\n'.format(index+1, value)

    jira_group.add_argument('--jiras', type=int,
        nargs='+', action='store', default=JIRAS_TO_TEST,
        help=help_str)

    jira_group.add_argument('--roi', type=int,
        nargs=4, action='append', default=None,
        help='R|Use to specify an ROI as a list of 4 integers. Include this\n'
             'argument multiple times to test with different ROIs.\n'
             'By default, this argument is set to \'None\' which will result\n'
             'in executing the test with a full-frame ROI. An ROI is defined\n'
             'as follows: originX originY sizeX sizeY\n'
             'NOTE: This option may not be applicable for all JIRA tests!')

    jira_group.add_argument('--stepSize', type=float,
        action=PositiveFractionAction, default=DEFAULT_STEP_SIZE,
        help='R|For HLSOPS-1828, this specifies the exposure time step size '
             'as a fraction of the RowReadTime.\n' )


class JiraTests():
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, camera, cmdLineArgs):
        self._andor = camera        # The encapsulating camera class
        self._cam = camera.cam      # The SDK3 camera object
        self._cla = cmdLineArgs     # The command line arguments
        self._calc = None           # The camera calculator
    #
    # End of ReadoutTimeTests.__init__()


    #----------------------------- execute() -----------------------------------
    #
    def execute(self, calculator):
        global ALL_SCENARIO_DESCRIPTIONS

        success = True
        test_summary = []
        total_counts = Counts()
        num_jira_tests = len(ALL_JIRA_DESCRIPTIONS)

        self._calc = calculator     # Save calculator for use during tests

        jira_template = 'JIRA #{j:02}: {d:}'

        # If ROIs have been defined, convert from cmd line format of list of lists
        # to a list of arrays. Otherwise just test full frame...
        rois_to_test = []
        if self._cla.roi == None:
            rois_to_test.append(self._andor.full_frame_roi)
        else:
            for r in self._cla.roi:
                rois_to_test.append(Roi(r[0], r[1], r[2], r[3]))

        for jira_idx in self._cla.jiras:

            # Format test description with current scenario
            test_description = jira_template.format(
                j=jira_idx,
                d=ALL_JIRA_DESCRIPTIONS[jira_idx-1])

            print(test_description)

            # Clear counters and start timer
            test_counts = Counts()
            test_started_at = time.perf_counter()

            if 'HLSOPS-1828' in ALL_JIRA_DESCRIPTIONS[jira_idx-1]:
                # This JIRA does not apply to Rolling Shutter
                if 'Rolling' in self._andor.cam.ElectronicShutteringMode:
                    print('Skipping Setup: Rolling Shutter is not applicable!\n')
                    test_summary.append(
                        '  SKIP, ' + test_description)
                    continue

                test = hlsops_1828.HLSOPS_1828(self._andor, self._cla)
                (success, test_counts) = test.execute(calculator, rois_to_test)

            # Stop test timer and output test duration
            test_completed_at = time.perf_counter()
            test_duration = test_completed_at - test_started_at
            print('Test Duration: {}'.format(
                datetime.timedelta(seconds=test_duration)))

            # Increment total counts by test counts
            total_counts.add_counts(test_counts)

            # Output overall status of test and save to summary list
            if success:
                if test_counts.num_warn > 0:
                    print('Completed successfully with warnings!\n')
                    test_summary.append(
                        '  WARN, ' + test_description)
                else:
                    print('Completed successfully!\n')
                    test_summary.append(
                        '  PASS, ' + test_description)
            else:
                print('Failed\n')
                test_summary.append(
                    '  FAIL, ' + test_description)
                success = False

        return (success, test_summary, total_counts)
    #
    # End of JiraTests.execute()


