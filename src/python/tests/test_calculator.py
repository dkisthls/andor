#!/usr/bin/env python3

import sys
sys.path.append('../cameras')
sys.path.append('../util')
sys.path.append('../calculators/balor')
sys.path.append('../calculators/zyla')

import argparse
import cffi
import datetime
import numpy as np
import time

from camera_args_loader import load_camera_args        # Loads balor/zyla args
from calc_test_args_loader import load_calc_test_args  # Loads expTime/readoutTime args
from jira import JiraTests                             # Tests from specific JIRAs
from readout_time import ReadoutTimeTests              # Readout time tests
from exposure_time import ExposureTimeTests            # Exposure time tests
from counts import Counts                              # Encapsulates test counters
from custom_actions import GEQZeroAction
from custom_actions import HasLimitsAction
from custom_actions import ParseIntDictGEQZeroAction
from custom_actions import MultiFormatter
from tcDefined import tcConstants as const
from tcDefined import tcDefaults as default
from tcDefined import tcOptions as options
from balor import Balor                                # The Balor camera
from zyla import Zyla                                  # The Zyla camera
from balor_calculator import BalorCalculator           # The Balor calculator
from zyla_calculator import ZylaCalculator             # The Zyla calculator

ffi = cffi.FFI()


#===============================================================================
# CLASS: TestCalculator
#===============================================================================

class TestCalculator():
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, cmdLineArgs):
        self._andor = None          # The encapsulating camera class
        self._cam = None            # The SDK3 camera object
        self._cla = cmdLineArgs     # The command line arguments
        self._calc = None           # The camera calcualtor

        # Instantiate the correct camera and calculator based on camera type
        if self._cla.cameraType == options.ZYLA:
            self._andor = Zyla(self._cla)
            self._calc = ZylaCalculator()
        elif self._cla.cameraType == options.BALOR:
            self._andor = Balor(self._cla)
            self._calc = BalorCalculator()
        else:
            raise NotImplementedError(
                '{} camera/calculator not supported at this time!'.format(
                    self._cla.cameraType))

        self._cam = self._andor.cam    # Get the SDK3 camera object

    #
    # End of TestCalculator.__init__()


    #------------------------------- run() -------------------------------------
    #
    def run(self):
        success = True
        setup_number = 0
        setup_template = 'SETUP #{n:02}: TriggerMode={tmode}, ElectronicShutteringMode={smode: <7}, GainMode=\'{gmode}\''
        summary_report = []
        total_counts = Counts()
        total_runtime_started_at = time.perf_counter()

        # Select the appropriate test class and instantiate...
        if self._cla.whatToTest == options.READOUT_TIME:
            tests = ReadoutTimeTests( self._andor, self._cla )
        elif self._cla.whatToTest == options.EXPOSURE_TIME:
            tests = ExposureTimeTests( self._andor, self._cla )
        elif self._cla.whatToTest == options.JIRA:
            tests = JiraTests( self._andor, self._cla )
        else:
            return False

        for trigger_mode in self._cla.triggerMode:
            for shutter_mode in self._cla.shutterMode:
                for gain_mode in self._cla.gainMode:
                    setup_number += 1
                    setup_description = setup_template.format(
                        n=setup_number, tmode=trigger_mode,
                        smode=shutter_mode, gmode=gain_mode)

                    summary_report.append(setup_description)

                    print('=' * 100)
                    print(setup_description)
                    print('Date: {}'.format(time.strftime(
                        "%a, %d %b %Y %H:%M:%S +0000", time.localtime())))

                    # Setup camera with trigger, shutter, and gain modes
                    self._cam.TriggerMode = trigger_mode
                    self._cam.ElectronicShutteringMode = shutter_mode
                    if self._andor._type == options.BALOR:
                        self._cam.GainMode = gain_mode
                    elif self._andor._type == options.ZYLA:
                        self._cam.SimplePreAmpGainControl = gain_mode
                    else:
                        raise NotImplementedError(
                            '{} camera/calculator not supported at this time!'.format(
                                self._andor._type))

                    # Restore AOILayout and ExposureTime to defaults
                    self._cam.AOILayout = default.AOILAYOUT
                    self._cam.ExposureTime = self._cam.min_ExposureTime

                    # Configure calculator
                    if self._andor._type == options.ZYLA:
                        self._calc.configure(
                            shutter_mode, trigger_mode,
                            default.TRIGGER_DELAY, self._cla.fastAOIFrameRateEnable)
                    elif self._andor._type == options.BALOR:
                        self._calc.configure(
                            shutter_mode, trigger_mode,
                            default.TRIGGER_DELAY, False)
                    else:
                        raise NotImplementedError(
                            '{} camera/calculator not supported at this time!'.format(
                                self._andor._type))

                    # Execute tests passing along the configured calculator
                    (success, test_summary, test_counts) = tests.execute(self._calc)

                    # Append summary from the test to the summary report and
                    # add the test counts from the test to the totals
                    summary_report.extend(test_summary)
                    total_counts.add_counts(test_counts)

                    # NOTE: At the end we're just going to iterate through the
                    # test_summary list and print the contents.
                    # Add a blank line between setups (trigger,shutter,gain
                    # configs) for display purposes.
                    test_summary.append('')

        # Stop total runtime timer and output test duration
        total_runtime_completed_at = time.perf_counter()
        total_runtime = total_runtime_completed_at - total_runtime_started_at

        # Output summary report

        print('{:#^100}\n'.format(' SUMMARY REPORT '))
        print('Total # Tests Executed: {}\n'
              '              # Passed: {}\n'
              '    # Passed w/Warning: {}\n'
              '            # Failures: {}\n'
              '         Total Runtime: {}\n'.format(
                total_counts.num_tests,
                total_counts.num_pass,
                total_counts.num_warn,
                total_counts.num_fail,
                datetime.timedelta(seconds=total_runtime)))

        print(*summary_report, sep='\n')
        print('\n{}'.format('#' * 100))

        return success
    #
    # End of TestCalculator.run()


## Main program runtime ###

#-------------------------------- show_args() ----------------------------------
#
def show_args(args):
    print('Command Line Arguments:')
    for key, val in vars(args).items():
        print('    {}: {}'.format(key, val))
#
# End of show_args()


def main():
    # Setup a temporary argparser, with help disabled, so that we can get some
    # of the positional arguments. Once obtained we will re-parse with the real
    # parser with help enabled. This will allow customizing the help depending
    # on the positional choices.
    tmp_parser = argparse.ArgumentParser(add_help=False)
    tmp_parser.add_argument('cameraType', action='store', type=str,
        choices=options.CAMERAS)

    tmp_parser.add_argument('whatToTest', action='store', type=str,
        choices=options.WHAT_TO_TEST)

    # Now parse the known arguments to get the positional arguments.
    # They will be contained in 'namespace'
    namespace, args = tmp_parser.parse_known_args()

    # Setup the real parser...
    parser = argparse.ArgumentParser(
        description='Andor Balor/Zyla Calculator Tests',
        add_help=True,
        formatter_class=MultiFormatter)

    parser.add_argument('cameraType', action='store',
        choices=options.CAMERAS,
        help='The camera/calculator to test')

    parser.add_argument('whatToTest', action='store',
        choices=options.WHAT_TO_TEST,
        help='The test to execute for the selected camera.')

    # Using the values for cameraType and whatToTest, load the arguments that
    # are specific to the values provided on the cmmand line.
    load_camera_args(parser, namespace.cameraType)
    load_calc_test_args(parser, namespace.whatToTest)

    # Add arguments that are common across all modes
    parser.add_argument('--debug',
        action='store_true',
        help='Enable debug output.')

    parser.add_argument('--fastAOIFrameRateEnable',
        action='store_true',
        help='R|Enable the \"fastAOIFrameRateEnable\" feature of the camera.\n')

    parser.add_argument('--quickCheck', type=int,
        action=HasLimitsAction, default=default.QUICK_CHECK,
        minimum=const.MIN_QUICK_CHECK, maximum=const.MAX_QUICK_CHECK,
        help='R|Execute N tests from the active scenario where:\n'
             '    0 = Disable quick check\n'
             '    1 <= value <= 100 : Number of test cases to check\n')

    parser.add_argument('--startWith', type=int,
        action=HasLimitsAction, default=default.START_WITH, minimum=1,
        help='Start scenario tests beginning with supplied test number')

    parser.add_argument('--triggerMode', choices=options.TRIGGER_MODES,
        nargs='+', action='store', default=options.TRIGGER_MODES,
        help='R|Used to specify the trigger mode(s) to use in conjunction with this test.\n')

    # Now fully parse the command line arguments and display
    cmdLineArgs = parser.parse_args()

    show_args(cmdLineArgs)

    try:
        test_calculated = TestCalculator(cmdLineArgs)
        test_calculated.run()
    except NotImplementedError as nie:
        print(nie)
    except RuntimeError as err:
        print('Terminating due to startup error!')

#
# End of Tests.main()

if __name__ == '__main__':
    main()
