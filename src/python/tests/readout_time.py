#!/usr/bin/env python3

import sys
sys.path.append('../util')

import datetime
import time

from aoi_roi import Aoi, Roi
from counts import Counts                              # Encapsulates test counters
from tcDefined import tcConstants as const
from tcDefined import tcOptions as options

AOI_SCENARIO_DESCRIPTIONS = [
    'Full Sensor - Sweep AOIHeight from Max to Min, Top fixed at 1',
    'Full Sensor - Sweep AOIHeight from Min to Max, Top fixed at 1',
    '1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at 1',
    '1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at 1',
    '1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at Max/2 + 1',
    '1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at Max/2 + 1',
    'Full Sensor - Sweep AOITop from 1 to (HeightMax-HeightMin+1), Height adjusted from Max to Min',
    'Full Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to 1, Height adjusted from Min to Max',
    '1/2  Sensor - Sweep AOITop from 1 to HeightMax/2, Height adjusted from Max/2 to Min',
    '1/2  Sensor - Sweep AOITop from (HeightMax/2-HeightMin+1) to 1, Height adjusted from Min to Max/2',
    '1/2  Sensor - Sweep AOITop from (HeightMax/2+1) to (HeightMax-HeightMin+1), Height adjusted from Max/2 to Min',
    '1/2  Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to (HeightMax/2+1), Height adjusted from Min to Max/2',
    'Variable - Shrink AOI verticallly staying centered on sensor, Top increment by 1, Height decrement by 2',
    'Variable - Shrink AOI vertically and horizontally staying centered on sensor, Top increment by 1, Height/width decrement by 2',
]

MT_SCENARIO_DESCRIPTIONS = [
    '2 Contiguous MTs - Sweep MT1 start up from Top=1, For each pair MT1 start++, height of MT1&MT2++',
    '2 Contiguous MTs - Sweep MT2 down from Top=Max, For each pair MT2 (start+height)--, height of MT1&MT2++',
    '2 MTs - MT1/MT2 pin to edges, H1/H2=1, grow number of rows by 1 towards center, total # rows even',
    '2 MTs - MT1/MT2 pin to edges, H1=1,H2=2, grow number of rows by 1 towards center, total # rows odd',
    '2 MTs - MT1/MT2 height=sensorHeight/2, shrink both by 1 towards edges',
    '2 MTs - MT1/MT2 height=1, start at center and grow size by 1 towards edges',
    '2 MTs - MT1/MT2 height=sensorHeight/2, shrink height/top together to 1 row @ 1/4 & 3/4 height (even rows)',
    '2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT1 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)',
    '2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT2 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)',
    '2 MTs - Start/End MT1/MT2 at edges, H1/H2=1, size++ towards center, 10x MT1 Start++, MT2 End--',
    '2 MTs - MT1/MT2 centered, H1/H2=1, height++ towards edges, total # rows even',
    '2 MTs - MT1/MT2 centered, H1=2,H2=1, height++ towards edges, total # rows odd',
    '2 MTs - Explicit ViSP Use Cases',
    'Specific sensor edge/near sensor edge test cases, top1=1',
    'Specific sensor edge/near sensor edge test cases, top2+height=sensorHeight',
    'Random MTs'
]

# Complete list of all test scenarios
ALL_SCENARIO_DESCRIPTIONS = AOI_SCENARIO_DESCRIPTIONS + MT_SCENARIO_DESCRIPTIONS

# Scenarios are accessed by their integer place in the cumulative list.
# Get the total count and this will server as the default scenarios to test.
SCENARIOS_TO_TEST = range(1, len(ALL_SCENARIO_DESCRIPTIONS))


def add_arguments(parser):
    global AOI_SCENARIO_DESCRIPTIONS
    global MT_SCENARIO_DESCRIPTIONS
    global SCENARIOS_TO_TEST

    readout_group = parser.add_argument_group('readout_time',
        description=('Arguments specific for testing the ReadoutTime (Frame '
                     'Read Time) which is inclusive of calculating and testing '
                     'the total number of row reads.\n'))

    help_str = ('R|Used to select specific test scenarios.\n'
                'Use the integer index value for making a selection:\n')

    help_str += 'AOI TESTS\n'
    for index, value in enumerate(AOI_SCENARIO_DESCRIPTIONS):
        help_str += '    {}) {}\n'.format(index+1, value)

    help_str += 'MULTITRACK TESTS\n'
    for index, value in enumerate(MT_SCENARIO_DESCRIPTIONS):
        help_str += '    {}) {}\n'.format(index+len(AOI_SCENARIO_DESCRIPTIONS)+1, value)

    readout_group.add_argument('--scenarios', type=int,
        nargs='+', action='store', default=SCENARIOS_TO_TEST,
        help=help_str)


class ReadoutTimeTests():
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, camera, cmdLineArgs):
        self._andor = camera        # The encapsulating camera class
        self._cam = camera.cam      # The SDK3 camera object
        self._cla = cmdLineArgs     # The command line arguments
        self._calc = None           # The camera calculator
    #
    # End of ReadoutTimeTests.__init__()


    #----------------------------- execute() -----------------------------------
    #
    def execute(self, calculator):
        global AOI_SCENARIO_DESCRIPTIONS
        global ALL_SCENARIO_DESCRIPTIONS

        success = True
        test_summary = []
        total_counts = Counts()
        num_aoi_scenarios = len(AOI_SCENARIO_DESCRIPTIONS)

        self._calc = calculator     # Save calculator for use during tests

        scenario_template = 'Scenario #{s:02}: {d:}'

        for scenario_num in self._cla.scenarios:
            print('-' * 100)

            aois_to_test = []
            rois_to_test = []

            # Format test description with current scenario
            test_description = scenario_template.format(
                s=scenario_num,
                d=ALL_SCENARIO_DESCRIPTIONS[scenario_num-1])

            # Clear counters and start timer
            test_counts = Counts()
            test_started_at = time.perf_counter()

            if scenario_num <= num_aoi_scenarios:
                #============== AOI TEST
                #
                aois_to_test = self._generate_aois_to_test(scenario_num)
                if aois_to_test == None:
                    success = False
                else:
                    (success, test_counts) = self._set_aoi_and_check_calculated(
                        aois_to_test, test_description)
            else:
                #============== MULTITRACK TEST
                #
                mt_scenario_num = scenario_num - num_aoi_scenarios
                aois_to_test = self._generate_mts_to_test(mt_scenario_num)
                if aois_to_test == None:
                    success = False
                else:
                    (success, test_counts) = self._set_mt_and_check_calculated(
                        aois_to_test, test_description)


            # Stop test timer and output test duration
            test_completed_at = time.perf_counter()
            test_duration = test_completed_at - test_started_at
            print('Test Duration: {}'.format(
                datetime.timedelta(seconds=test_duration)))

            # Increment total counts by test counts
            total_counts.add_counts(test_counts)

            # Output overall status of test and save to summary list
            if success:
                if test_counts.num_warn > 0:
                    print('Completed successfully with warnings!\n')
                    test_summary.append(
                        '  WARN, ' + test_description)
                else:
                    print('Completed successfully!\n')
                    test_summary.append(
                        '  PASS, ' + test_description)
            else:
                print('Failed\n')
                test_summary.append(
                    '  FAIL, ' + test_description)
                success = False


        return (success, test_summary, total_counts)
    #
    # End of ReadoutTimeTests.execute()


    #--------------------- _generate_aois_to_test() ----------------------------
    #
    def _generate_aois_to_test(self, scenario_num):
        left = self._andor.full_frame_aoi.left
        width = self._andor.full_frame_aoi.width
        sensor_height = self._andor.full_frame_aoi.height
        half_height = int(sensor_height / 2)
        min_height = self._cam.min_AOIHeight
        max_height = self._cam.max_AOIHeight
        min_top = self._cam.min_AOITop

        aois_to_test = []
        reverse_list = False

        #============== AOI TESTS
        #
        if scenario_num == 1 or scenario_num == 2:
            # 1) Full Sensor - Sweep AOIHeight from Max to Min, Top fixed at 1
            # 2) Full Sensor - Sweep AOIHeight from Min to Max, Top fixed at 1
            top = 1
            for height in range(max_height, min_height-1, -1):
                aois_to_test.append( Aoi(left, top, width, height) )

            if scenario_num == 2: reverse_list = True

        elif scenario_num == 3 or scenario_num == 4:
            # 3) 1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at 1
            # 4) 1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at 1
            top = 1
            for height in range(half_height, min_height-1, -1):
                aois_to_test.append( Aoi(left, top, width, height) )

            if scenario_num == 4: reverse_list = True

        elif scenario_num == 5 or scenario_num == 6:
            # 5) 1/2  Sensor - Sweep AOIHeight from Max/2 to Min, Top fixed at Max/2 + 1
            # 6) 1/2  Sensor - Sweep AOIHeight from Min to Max/2, Top fixed at Max/2 + 1
            top = half_height + 1
            for height in range(half_height, min_height-1, -1):
                aois_to_test.append( Aoi(left, top, width, height) )

            if scenario_num == 6: reverse_list = True

        elif scenario_num == 7 or scenario_num == 8:
            # 7) Full Sensor - Sweep AOITop from 1 to (HeightMax-HeightMin+1), Height adjusted from Max to Min
            # 8) Full Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to 1, Height adjusted from Min to Max
            height = max_height;
            for top in range(1, max_height-min_height+2, 1):
                aois_to_test.append( Aoi(left, top, width, height) )
                height -= 1

            if scenario_num == 8: reverse_list = True

        elif scenario_num == 9 or scenario_num == 10:
            # 9)  1/2  Sensor - Sweep AOITop from 1 to HeightMax/2, Height adjusted from Max/2 to Min
            # 10) 1/2  Sensor - Sweep AOITop from (HeightMax/2-HeightMin+1) to 1, Height adjusted from Min to Max/2
            height = half_height
            for top in range(min_top, half_height-min_height+2, 1):
                aois_to_test.append( Aoi(left, top, width, height) )
                height -= 1

            if scenario_num == 10: reverse_list = True

        elif scenario_num == 11 or scenario_num == 12:
            # 11) 1/2  Sensor - Sweep AOITop from (HeightMax/2+1) to (HeightMax-HeightMin+1), Height adjusted from Max/2 to Min
            # 12) 1/2  Sensor - Sweep AOITop from (HeightMax-HeightMin+1) to (HeightMax/2+1), Height adjusted from Min to Max/2
            height = half_height
            for top in range(half_height+1, max_height-min_height+2, 1):
                aois_to_test.append( Aoi(left, top, width, height) )
                height -= 1

            if scenario_num == 12: reverse_list = True

        elif scenario_num == 13:
            # 13) Variable - Shrink AOI verticallly staying centered on sensor, Top increment by 1, Height decrement by 2
            top = 1
            for height in range(max_height, min_height-1, -2):
                aois_to_test.append( Aoi(left, top, width, height) )
                top += 1

        elif scenario_num == 14:
            # 14) Variable - Shrink AOI vertically and horizontally staying centered on sensor, Top increment by 1, Height/width decrement by 2
            top = 1
            for height in range(max_height, min_height-1, -2):
                aois_to_test.append( Aoi(left, top, width, height) )
                left += 1
                top += 1
                width -= 2

        else:
            print('TEST FOR AOI SCENARIO #{} IS NOT DEFINED!'.format(
                scenario_num))

        if not reverse_list:
            return aois_to_test
        else:
            return aois_to_test[::-1]
    #
    # End of ReadoutTimeTests._generate_aois_to_test()


    #---------------------- _generate_mts_to_test() ----------------------------
    #
    # NOTE: scenario_num supplied has been adjusted by the length of the AOI
    #       scenario list to 1 base it.
    def _generate_mts_to_test(self, scenario_num):
        left = self._andor.full_frame_aoi.left
        width = self._andor.full_frame_aoi.width
        sensor_height = self._andor.full_frame_aoi.height
        half_height = int(sensor_height / 2)
        min_height = self._cam.min_AOIHeight
        max_height = self._cam.max_AOIHeight
        min_top = self._cam.min_AOITop

        aois_to_test = []
        rois_to_test = []
        reverse_list = False

        #============== MULTITRACK TESTS
        #
        if scenario_num == 1:
            # 15) Contiguous MTs - Sweep MT1 start up from Top=1, For each pair MT1 start++, height of MT1&MT2++
            top = 1
            height = 1
            while True:
                aoi1 = Aoi(left, top, width, height)
                aoi2 = Aoi(left, top+height, width, height)
                aois_to_test.extend([aoi1, aoi2])
                top += 1
                height += 1
                if aoi2.top+2 + aoi2.height+2 - 1 > sensor_height:
                    break

        elif scenario_num == 2:
            # 16) 2 Contiguous MTs - Sweep MT2 down from Top=Max, For each pair MT2 (start+height)--, height of MT1&MT2++
            top = sensor_height
            height = 1
            while top - (height * 2) - 1 >= 1:
                aoi2 = Aoi(left, top - height + 1, width, height)
                aoi1 = Aoi(left, aoi2.top - height, width, height)
                aois_to_test.extend([aoi1, aoi2])
                top -= 1
                height += 1

        elif scenario_num == 3:
            # 17) 2 MTs - MT1/MT2 pin to edges, H1/H2=1, grow number of rows by 1 towards center, total # rows even
            top1, top2 = 1, sensor_height
            height = 1
            while top1+height-1 <= half_height and top2 > half_height:
                aois_to_test.extend([
                    Aoi(left, top1, width, height),
                    Aoi(left, top2, width, height) ])
                top2 -= 1
                height += 1

        elif scenario_num == 4:
            # 18) 2 MTs - MT1/MT2 pin to edges, H1=1,H2=2, grow number of rows by 1 towards center, total # rows odd
            height1, height2 = 1, 2
            top1, top2 = 1, sensor_height-height2+1
            while top1+height1-1 <= half_height and top2 > half_height:
                aois_to_test.extend([
                    Aoi(left, top1, width, height1),
                    Aoi(left, top2, width, height2) ])
                top2 -= 1
                height1 += 1
                height2 += 1

        elif scenario_num == 5:
            # 19) 2 MTs - MT1/MT2 height=sensorHeight/2, shrink both by 1 towards edges
            height = half_height
            top1, top2 = 1, sensor_height-height+1
            while height >= 1:
                aois_to_test.extend([
                    Aoi(left, top1, width, height),
                    Aoi(left, top2, width, height) ])
                top2 += 1
                height -= 1

        elif scenario_num == 6:
            # 20) 2 MTs - MT1/MT2 height=1, start at center and grow size by 1 towards edges
            height = 1
            top1, top2 = half_height, half_height+1
            while top1 >= 1 and top2+height-1 <= sensor_height:
                aois_to_test.extend([
                    Aoi(left, top1, width, height),
                    Aoi(left, top2, width, height) ])
                top1 -= 1
                height += 1

        elif scenario_num == 7:
            # 21)  MTs - MT1/MT2 height=sensorHeight/2, shrink height/top together to 1 row @ 1/4 & 3/4 height (even rows)
            height = half_height
            top1, top2 = 1, half_height+1
            # Both MTs top++ while shrinking size by 2 (even rows)
            while height >= 1:
                aois_to_test.extend([
                    Aoi(left, top1, width, height),
                    Aoi(left, top2, width, height) ])
                top1 += 1
                top2 += 1
                height -= 2

        elif scenario_num == 8:
            # 22) 2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT1 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)
            height1, height2 = half_height, half_height
            top1, top2 = 1, half_height+1
            # Both MTs top++ while shrinking MT1 size by 3 and MT2 size by 2 (odd rows)
            while height1 >= 1 and height2 >= 1:
                aois_to_test.extend([
                    Aoi(left, top1, width, height1),
                    Aoi(left, top2, width, height2) ])
                top1 += 1
                top2 += 1
                height1 -= 3
                height2 -= 2

        elif scenario_num == 9:
            # 23) 2 MTs - MT1/MT2 height=sensorHeight/2, shrink MT2 height/top down to 3 rows @ 1/4 & 3/4 height (odd rows)
            height1, height2 = half_height, half_height
            top1, top2 = 1, half_height+1
            # Both MTs top++ while shrinking MT1 size by 2 and MT2 size by 3 (odd rows)
            while height1 >= 1 and height2 >= 1:
                aois_to_test.extend([
                    Aoi(left, top1, width, height1),
                    Aoi(left, top2, width, height2) ])
                top1 += 1
                top2 += 1
                height1 -= 2
                height2 -= 3

        elif scenario_num == 10:
            # 24) 2 MTs - Start/End MT1/MT2 at edges, H1/H2=1, size++ towards center, 10x MT1 Start++, MT2 End--
            for i in range(10):
                # Move starting row for MT1 and ending row for MT2 in by one for each set
                top1, top2 = i+1, sensor_height-i
                height = 1
                while top1+height-1 <= half_height and top2 > half_height:
                    aois_to_test.extend([
                        Aoi(left, top1, width, height),
                        Aoi(left, top2, width, height) ])
                    top2 -= 1
                    height += 1

        elif scenario_num == 11:
            # 25) 2 MTs - MT1/MT2 centered, H1/H2=1, height++ towards edges, total # rows even
            top1, top2 = half_height, half_height+1
            height = 1
            while top1 >= 1 and top2+height-1 <= sensor_height:
                aois_to_test.extend([
                    Aoi(left, top1, width, height),
                    Aoi(left, top2, width, height) ])
                top1 -= 1
                height += 1

        elif scenario_num == 12:
            # 26) 2 MTs - MT1/MT2 centered, H1=2,H2=1, height++ towards edges, total # rows odd
            height1, height2 = 2, 1
            top1, top2 = half_height-height1+1, half_height+1
            while top1 >= 1 and top2+height2-1 <= sensor_height:
                aois_to_test.extend([
                    Aoi(left, top1, width, height1),
                    Aoi(left, top2, width, height2) ])
                top1 -= 1
                height1 += 1
                height2 += 1

        elif scenario_num == 13:
            # 27) 2 MTs - Explicit ViSP Use Cases as CSS ROI's.
            rois_to_test.extend([
                Roi( 0, 0,   2560, 1000), Roi( 0, 1160, 2560, 1000),
                Roi( 0, 447, 2560, 186),  Roi( 0, 1527, 2560, 186),
                Roi( 0, 0,   2560, 890),  Roi( 0, 1270, 2560, 890),
                Roi( 0, 447, 2560, 186),  Roi( 0, 1527, 2560, 186),

                # Theoreticals for ViSP
                # Edges
                Roi( 0, 0,    2560, 50), Roi( 0, 2110, 2560, 50),

                # Center
                Roi( 0, 1230, 2560, 50), Roi( 0, 1280, 2560, 50),

                # Centered in half
                Roi( 0, 515,  2560, 50), Roi( 0, 1595, 2560, 50),

                # Center                 Centered in half
                Roi( 0, 1230, 2560, 50), Roi( 0, 1595, 2560, 50) ])

            # ROIs are increasing origin order while AOIs need to be in
            # increasing top order. Pairs need to be converted from ROI to
            # AOI while being swapped.
            for i in range(1, len(rois_to_test), 2):
                aois_to_test.append(
                    Aoi.from_roi( rois_to_test[i], sensor_height))
                aois_to_test.append(
                    Aoi.from_roi( rois_to_test[i-1], sensor_height))

        elif scenario_num == 14:
            # 28) Specific sensor edge/near sensor edge test cases, top1=1
            max_height = 10
            for sep in range(max_height):
                for top1 in range(1, max_height+1):
                    for height in range(1, max_height-top1+2):
                        aois_to_test.extend([
                            Aoi(left, top1, width, height),
                            Aoi(left,top1+height+sep, width, height)])

        elif scenario_num == 15:
            # 29) Specific sensor edge/near sensor edge test cases, top2+height=sensorHeight
            max_height = 10
            for sep in range(max_height):
                for end2 in range(sensor_height, sensor_height-10+1, -1):
                    for height in range(1, max_height+1):
                        start2 = end2 - height + 1
                        end1 = start2 - 1
                        start1 = end1 - height + 1
                        aois_to_test.extend([
                            Aoi(left, start1, width, height),
                            Aoi(left, start2, width, height)])

        else:
            print('TEST FOR MULTITRACK SCENARIO #{} IS NOT DEFINED!'.format(
                scenario_num))

        return aois_to_test
    #
    # End of ReadoutTimeTests._generate_mts_to_test()


    #------------------ _set_aoi_and_check_calculated() ------------------------
    #
    def _set_aoi_and_check_calculated(self, aois_to_test, test_description):
        success = True             # Overall test success
        test_number = 0
        pathTaken = ''             # For tracking the calculator path taken
        counts = Counts()

        print('{}\nStarting {}\n  Ending {}'.format(
            test_description,
            format(aois_to_test[0], 'cl'),
            format(aois_to_test[len(aois_to_test)-1], 'cl')))

        print('{}|{: ^13}|{: ^39}|'.format(
            29*' ', 'NumRowReads', 'ReadoutTime' ))
        # if self._cla.debug:
        #     print('      OrigX,OrigY,SizeX,SizeY')
        print('{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >7},{: ^19},{: ^19},{: >7}'.format(
            'Test', 'Left', 'Top', 'Wdth', 'Hght',
            'Calc', 'Actual', 'Calc', 'Actual', 'Status'))

        # All AOI definitions have the same width/left value so setup.
        self._cam.AoiLayout = 'Image'
        self._cam.AoiWidth = aois_to_test[0].width
        self._cam.AoiLeft = aois_to_test[0].left
        exposure_time = self._cam.ExposureTime

        for aoi in aois_to_test:
            test_number += 1

            # If startWith > 0 then skip tests until we get to the specified
            # test.
            if test_number < self._cla.startWith:
                continue

            # If quickCheck > 0 then only run the first 'quickCheck' tests.
            if (self._cla.quickCheck > 0 and
               (test_number > (self._cla.startWith + self._cla.quickCheck - 1))):
                break

            counts.num_tests += 1           # increment test counter

            # Configure the camera with the remainder of the AOI...
            self._cam.AoiHeight = aoi.height
            self._cam.AoiTop = aoi.top

            # The calculator expects a list of ROI arrays.
            roi_list = []
            roi_list.append(
                Roi.from_aoi(aoi, self._cam.SensorHeight).to_array())

            # Run the calculator and get values from camera and compare...
            (status, let,
                num_row_reads,
                readout_time) = self._calculate_and_compare(roi_list, exposure_time)

            if 'PASS' in status:
                counts.num_pass += 1
            elif 'WARN' in status:
                counts.num_warn += 1
            else:
                counts.num_fail += 1

            # Assemble and print the test output string...
            print('{: >5}, {}, {:4d}, {:5d}, {:.16f}, {:.16f}, {}'.format(
                test_number, format(aoi, 'rp'),
                num_row_reads[const.IDX_CALC], num_row_reads[const.IDX_ACTUAL],
                readout_time[const.IDX_CALC], readout_time[const.IDX_ACTUAL],
                status))

        # Determine overall test status by examining the failure count...
        if counts.num_fail > 0:
            success = False

        return (success, counts)
    #
    # End of ReadoutTimeTests._set_aoi_and_check_calculated()


    #------------------ _set_mt_and_check_calculated() -------------------------
    #
    def _set_mt_and_check_calculated(self, aois_to_test, test_description):
        success = True             # Overall test success
        test_number = 0
        pathTaken = ''             # For tracking the calculator path taken
        counts = Counts()

        if len(aois_to_test) < 2:
            print('ERROR - _set_mt_and_check_calculated(): aois_to_test size='
                  '{}, size must be 2!'.format(len(aois_to_test)))
            return False

        print('{}\nStarting {}\n  Ending {}'.format(
            test_description,
            format(aois_to_test[0], 'cl'),
            format(aois_to_test[len(aois_to_test)-1], 'cl')))

        print('{}|{: ^23}|{: ^23}|{: ^13}|{: ^39}|'.format(
            5*' ','Multitrack #1', 'Multitrack #2', 'NumRowReads', 'ReadoutTime' ))
        # if self._cla.debug:
        #     print('      OrigX,OrigY,SizeX,SizeY,OrigX,OrigY,SizeX,SizeY')
        print('{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >5},{: >7},{: ^19},{: ^19},{: >7}'.format(
            'Test', 'Left', 'Top', 'Wdth', 'Hght', 'Left', 'Top', 'Wdth', 'Hght',
            'Calc', 'Actual', 'Calc', 'Actual', 'Status'))

        # All Multitrack definitions have the same width/left value so setup.
        self._cam.AoiLayout = 'Multitrack'
        self._cam.AoiWidth = aois_to_test[0].width
        self._cam.AoiLeft = aois_to_test[0].left
        exposure_time = self._cam.ExposureTime

        # Reset multi-track
        self._cam.MultitrackCount = 1
        self._cam.MultitrackSelector = 0
        self._cam.MultitrackStart = 1
        self._cam.MultitrackEnd = self._cam.SensorHeight

        test_number = self._cla.startWith - 1
        for idx in range(self._cla.startWith*2-1, len(aois_to_test)+1, 2):
            test_number += 1

            # If quickCheck > 0 then only run the first 'quickCheck' tests.
            if (self._cla.quickCheck > 0 and
               (test_number > (self._cla.startWith + self._cla.quickCheck) - 1)):
                break

            aoi1 = aois_to_test[idx-1]
            aoi2 = aois_to_test[idx]
            counts.num_tests += 1           # increment test counter

            # Configure the camera for the two multitracks...
            self._cam.MultitrackCount = 2

            self._cam.MultitrackSelector = 0
            self._cam.MultitrackBinned = False
            self._cam.MultitrackStart = aoi1.top
            self._cam.MultitrackEnd = aoi1.top + aoi1.height - 1

            self._cam.MultitrackSelector = 1
            self._cam.MultitrackBinned = False
            self._cam.MultitrackStart = aoi2.top
            self._cam.MultitrackEnd = aoi2.top + aoi2.height - 1

            # The calculator expects a list of ROI arrays.
            # Using the next two MultiTrack(AOI) definitions, create the list of
            # ROI arrays.
            # NOTE: The order is inverted due to CSS ROI origin(0,0) vs
            #       Sensor origin(1,1). CSS origin is lower left, Sensor origin
            #       is upper left): ROI1 == AOI2, ROI2 == AOI1
            roi_list = []
            roi_list.append(
                Roi.from_aoi(aoi2, self._cam.SensorHeight).to_array())
            roi_list.append(
                Roi.from_aoi(aoi1, self._cam.SensorHeight).to_array())

            # Run the calculator and get values from camera and compare...
            (status, let,
                num_row_reads,
                readout_time) = self._calculate_and_compare(roi_list, exposure_time)

            if 'PASS' in status:
                counts.num_pass += 1
            elif 'WARN' in status:
                counts.num_warn += 1
            else:
                counts.num_fail += 1

            # Assemble and print the test output string...
            print('{: >5}, {}, {}, {:4d}, {:5d} , {:.16f}, {:.16f}, {}'.format(
                test_number,
                format(aoi1, 'rp'), format(aoi2, 'rp'),
                num_row_reads[const.IDX_CALC], num_row_reads[const.IDX_ACTUAL],
                readout_time[const.IDX_CALC], readout_time[const.IDX_ACTUAL],
                status))

        # Determine overall test status by examining the failure count...
        if counts.num_fail > 0:
            success = False

        return (success, counts)
    #
    # End of ReadoutTimeTests._set_mt_and_check_calculated()


    #--------------------- _calculate_and_compare() ----------------------------
    #
    def _calculate_and_compare(self, roi_list, exposure_time):
        # Clear calculated/actual lists
        readout_time = [0.0] * 2 # Calculated/actual frame readout time
        num_row_reads = [0] * 2  # Calculated/actual number of row reads
        let = [0.0] * 2          # Calculated/actual longExposureTransition

        # Invoke calculator
        if self._andor._type == options.ZYLA:
            vBin = 1    # Zyla is always 1 for the calculator
        elif self._andor._type == options.BALOR:
            vBin = 1    # Balor calculator needs the vBin value because it
                        # performes it's internal calculations based on
                        # super pixels. This is a place holder until binning
                        # support is added to this test

        (min_exp_time,
            max_exp_time,
            actual_exp_time,
            cycle_time,
            let[const.IDX_CALC]) = self._calc.calculate(exposure_time, roi_list, vBin)

        (num_row_reads[const.IDX_CALC],
            row_read_time,
            readout_time[const.IDX_CALC]) = self._calc.get_calculated_extras()

        # Retrieve actual values from camera...
        readout_time[const.IDX_ACTUAL] = self._cam.ReadoutTime

        num_row_reads[const.IDX_ACTUAL] = round(
            readout_time[const.IDX_ACTUAL] / row_read_time)

        let[const.IDX_ACTUAL] = self._cam.LongExposureTransition

        warning = ''               # calculated value > actual, ok
        what_failed = ''
        max_rowreads_over = 4
        if num_row_reads[const.IDX_CALC] != num_row_reads[const.IDX_ACTUAL]:
            # There may be a discrepancy with the calculated number of
            # rowReads. Allow the calculated value to be upto 4 rowReads
            # greater than what is obtained from the camera. This will
            # ultimately result in a max exposure rate that is slower than
            # what the camera can actually do.
            # Once some of the boundary conditions on calcualting the number
            # of row reads is sorted out this cam be removed.
            diff = num_row_reads[const.IDX_CALC] - num_row_reads[const.IDX_ACTUAL]
            if( num_row_reads[const.IDX_CALC] > num_row_reads[const.IDX_ACTUAL] and
                diff <= max_rowreads_over):
                warning = 'NumRowReads +{}, max=+{}'.format(diff, max_rowreads_over)
            else:
                what_failed += "NumRowReads "

        # Readout time follows number of row reads so we will only note a
        # failure when the calculated < the readout time obtained from the
        # camera.
        if readout_time[const.IDX_CALC] < readout_time[const.IDX_ACTUAL]:
            what_failed += 'ReadoutTime '

        status = ''
        if len(what_failed) == 0:
            if len(warning) == 0:
                status = 'PASS'
            else:
                status = 'WARN: ' + warning;
        else:
            status = 'FAIL: ' + what_failed;

        if self._cla.debug:
            print(self._calc.get_all_params_as_string())
            print("Calculated Values:\n"
                  "  {}\n"
                  "    min/max exposureTime={:.07f}ms/{:.06f}sec\n"
                  "     actual exposureTime={:.06f}ms\n"
                  "  longExposureTransition={:.06f}sec\n"
                  "               cycleTime={:.09f}sec\n"
                  "        max exposureRate={:.18f}Hz (== 1/cycleTime)\n".format(
                    self._calc.get_calculated_extras_as_string(),
                    min_exp_time * 1000.0, max_exp_time,
                    actual_exp_time * 1000.0,
                    # long_exposure_transition,
                    let[const.IDX_CALC],
                    cycle_time,
                    (1.0/cycle_time)))

        return (status, let, num_row_reads, readout_time)
    #
    # End of ReadoutTimeTests._calculate_and_compare()
