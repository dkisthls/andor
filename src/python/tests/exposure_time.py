#!/usr/bin/env python3

import sys
sys.path.append('../util')
sys.path.append('../calculators/balor')
sys.path.append('../calculators/zyla')

import copy
import datetime
import time
import numpy as np

from aoi_roi import Aoi, Roi
from counts import Counts                    # Encapsulates test counters
from stats import Statistics
import misc                                  # Miscellaneous util methods
from tcDefined import tcConstants as const
from tcDefined import tcOptions as options

RANGE_FULL = 'full'
RANGE_SPECIFIED = 'specified'
RANGES = [RANGE_FULL, RANGE_SPECIFIED]
DEFAULT_RANGE = RANGE_FULL
MIN = 0              # index for minimum exposure time
MAX = 1              # index for maximum exposure time
STEP = 2             # index for exposure step size

SET_DESIRED = 'desired'
SET_CALCULATED = 'calculated'
SET_CHOICES = [SET_DESIRED, SET_CALCULATED]
SET_DEFAULT = SET_DESIRED


def add_arguments(parser):
    exposure_group = parser.add_argument_group('exposure_time',
        description=('Arguments specific for testing the calculated vs actual'
                     'exposure time and cycle time.\n'))

    exposure_group.add_argument('--set', type=str, choices=SET_CHOICES,
        action='store', default=SET_DEFAULT,
        help='R|Use to specify whether the requested or calculated exposure time\n'
             'value will be set on the camera and tested:\n'
             '{:>12}: Set the desired exposure time and test.\n'
             '{:>12}: Calculate the exposure time from the desired\n'
             '{:11}   exposure time, set the calculated value, and test.\n'.format(
                SET_DESIRED, SET_CALCULATED,' '))

    exposure_group.add_argument('--roi', type=int,
        nargs=4, action='append', default=None,
        help='R|Use to specify an ROI as a list of 4 integers. Include this\n'
             'argument multiple times to test with different ROIs.\n'
             'By default, this argument is set to \'None\' which will result\n'
             'in executing the test with a full-frame ROI. An ROI is defined\n'
             'as follows: originX originY sizeX sizeY\n')

    exposure_group.add_argument('--range', type=str, choices=RANGES,
        action='store', default=DEFAULT_RANGE,
        help='R|Used to specify the range of exposure times that will be tested.\n'
             '{:>11}: Min Exp = RowReadtime, Max Exp = DKIST (30s), varying step sized\n'
             '{:>11}: Uses the defined min/max/step\n'.format(
                RANGE_FULL, RANGE_SPECIFIED) )

    exposure_group.add_argument('--limits', type=float,
        nargs=3, action='store', default=[0.0005, 0.50, 0.0005],
        help='R|When --range=\'specified\', use this argument to specify the\n'
             'minimum and maximum exposure time and theexposure time step size.\n'
             'Enter as three individual float values: min max stepSize\n' )


class ExposureTimeTests():
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, camera, cmdLineArgs):
        self._andor = camera        # The encapsulating camera class
        self._cam = camera.cam      # The SDK3 camera object
        self._cla = cmdLineArgs     # The command line arguments
        self._calc = None           # The camera calculator
    #
    # End of ExposureTimeTests.__init__()


    #----------------------------- execute() -----------------------------------
    #
    def execute(self, calculator):
        success = True
        test_summary = []
        total_counts = Counts()

        self._calc = calculator     # Save calculator for use during tests

        # If ROIs have been defined, convert from cmd line format of list of lists
        # to a list of arrays otherwise just test full frame...
        rois_to_test = []
        if self._cla.roi == None:
            rois_to_test.append(self._andor.full_frame_roi)
        else:
            for r in self._cla.roi:
                rois_to_test.append(Roi(r[0], r[1], r[2], r[3]))

        # Clear counters and start timer
        total_counts = Counts()
        test_started_at = time.perf_counter()

        test_num = 0
        num_tests = len(rois_to_test)
        for roi in rois_to_test:
            test_num += 1

            print('-' * 100)

            if self._cla.range == RANGE_FULL:
                (success,
                    test_counts, test_description) = self._do_full_range_test(
                    test_num, num_tests, roi)

            else:    # RANGE_SPECIFIED
                (success,
                    test_counts, test_description) = self._do_specified_range_test(
                    test_num, num_tests, roi)

            # Increment total counts by test counts
            total_counts.add_counts(test_counts)

            # Stop test timer and output test duration
            test_completed_at = time.perf_counter()
            test_duration = test_completed_at - test_started_at
            print('Test Duration: {}'.format(
                datetime.timedelta(seconds=test_duration)))

            # Output overall status of test and save to summary list
            if success:
                if test_counts.num_warn > 0:
                    print('Completed successfully with warnings!\n')
                    test_summary.append(
                        '  WARN, ' + test_description)
                else:
                    print('Completed successfully!\n')
                    test_summary.append(
                        '  PASS, ' + test_description)
            else:
                print('Failed\n')
                test_summary.append(
                    '  FAIL, ' + test_description)
                success = False

        return (success, test_summary, total_counts)
    #
    # End of ExposureTimeTests.execute()


    #----------------------- _do_full_range_test() -----------------------------
    #
    def _do_full_range_test(self, test_num, num_tests, roi_to_test):
        global MIN              # index for minimum exposure time
        global MAX              # index for maximum exposure time
        global STEP             # index for exposure step size

        row_read_time = self._cam.RowReadTime
        min_exp_time = self._cam.min_ExposureTime
        print('row_read_time={}, min_exp_time={}'.format(row_read_time, min_exp_time))

        # Setup up exposure time test limits and corresponding increment values.
        # Arrays of 3 floats: (min, max, stepSize)
        limits_1 = (min_exp_time, 0.1, (0.25 * row_read_time))
        limits_2 = (limits_1[MAX],  1.0, 0.001)
        limits_3 = (limits_2[MAX],  10.0, 0.010)
        limits_4 = (limits_3[MAX],  const.DKIST_MAX_EXP_TIME, 0.100)

        print('Test Range 1 (min, max, stepSize): {}'.format(limits_1))
        print('Test Range 2 (min, max, stepSize): {}'.format(limits_2))
        print('Test Range 3 (min, max, stepSize): {}'.format(limits_3))
        print('Test Range 4 (min, max, stepSize): {}'.format(limits_4))

        list_of_limits = [limits_1, limits_2, limits_3, limits_4]

        overall_success = True
        total_counts = Counts()
        stats = []

        test_description = ('Test #{} of {}: '
            'Full Range: Min={:0.6f}us, Max={:0.1f}s, Step Size=varies, Configured ROI: {}'.format(
                test_num, num_tests,
                row_read_time * 1000000.0, const.DKIST_MAX_EXP_TIME,
                roi_to_test))

        print(test_description)

        # Set the camera with the defined ROI (as an AOI)
        aoi = Aoi.from_roi(roi_to_test, self._cam.SensorHeight)
        self._cam.AoiWidth = aoi.width
        self._cam.AoiLeft = aoi.left
        self._cam.AoiHeight = aoi.height
        self._cam.AoiTop = aoi.top

        print_header = True
        for limits in list_of_limits:
            (test_success, test_counts, test_stats) = self._set_exp_time_and_check_calculated(
                limits, roi_to_test, print_header)
            overall_success = overall_success and test_success

            total_counts.add_counts(test_counts)  # Add test counts to totals
            stats.append(copy.deepcopy(test_stats))      # Save statistics
            print_header = False

        # Print out statistics (min, max, avg) for time to set exposure time
        # across each of the test ranges...
        idx = 0
        range_num = 1
        for limits in list_of_limits:
            print('Range #{}: Min Exp Time={}, Max Exp Time={}, StepSize={}'.format(
                range_num, limits[MIN], limits[MAX], limits[STEP]))
            print('  Set Exposure Time Min: {:.6f}s @ Exposure Time={:.13e}ms\n'
                  '  Set Exposure Time Max: {:.6f}s @ Exposure Time={:.13e}ms\n'
                  '  Set Exposure Time Avg: {:.6f}s'.format(
                stats[idx].get_min(), stats[idx].get_data_for_min() * 1.0e3,
                stats[idx].get_max(), stats[idx].get_data_for_max() * 1.0e3,
                stats[idx].get_avg()))
            idx += 1
            range_num += 1

        return (overall_success, total_counts, test_description)
    #
    # End of ExposureTimeTests._do_full_range_test()


    #-------------------- _do_specified_range_test() ---------------------------
    #
    def _do_specified_range_test(self, test_num, num_tests, roi_to_test):
        global MIN              # index for minimum exposure time
        global MAX              # index for maximum exposure time
        global STEP             # index for exposure step size

        # Setup up exposure time test ranges and corresponding incrment valeus
        # Arrays of 3 floats: (min, max, stepSize)
        test_limits = (
            self._cla.limits[MIN], self._cla.limits[MAX], self._cla.limits[STEP])

        test_description = ('Test #{} of {}: '
            'Specified Range: Min={:0.6f}s, Max={:0.1f}s, Step Size={:0.3f}s, Configured ROI: {}'.format(
                test_num, num_tests,
                test_limits[MIN], test_limits[MAX], test_limits[STEP], roi_to_test))

        print(test_description)

        (overall_success, total_counts, stats) = self._set_exp_time_and_check_calculated(
            test_limits, roi_to_test)

        print(test_description)
        print('  Set Exposure Time Min: {:.6f}s @ Exposure Time={:.13e}ms\n'
              '  Set Exposure Time Max: {:.6f}s @ Exposure Time={:.13e}ms\n'
              '  Set Exposure Time Avg: {:.6f}s'.format(
            stats.get_min(), stats.get_data_for_min() * 1.0e3,
            stats.get_max(), stats.get_data_for_max() * 1.0e3,
            stats.get_avg()))


        return (overall_success, total_counts, test_description)
    #
    # End of ExposureTimeTests._do_specified_range_test()


    #--------------- _set_exp_time_and_check_calculated() ----------------------
    #
    # def _set_exp_time_and_check_calculated(
    #     self, min_exp, max_exp, step_size, roi_to_test, print_header=True):
    def _set_exp_time_and_check_calculated(
        self, exp_info, roi_to_test, print_header=True):
        global MIN              # index for minimum exposure time
        global MAX              # index for maximum exposure time
        global STEP             # index for exposure step size
        success = True
        test_summary = []
        counts = Counts()
        stats = Statistics()    # Tracks min/max/sum/cnt

        # SAVE FOR USE WITH SIMPLE HEADER CLA OPTION
        # print('{: ^13},{: ^13},{: ^13},{: ^21},{: ^13},{: ^13},{: ^21}, {}'.format(
        #     'reqExpTime', 'camExpTime', 'calcExpTime', '(cam-calc)',
        #     'camCycleTime', 'calcCycleTime', '(cam-calc)', 'Status'))

        # We are not supporteing Multitrack in this test at this time...
        self._cam.AoiLayout = 'Image'

        # print('-' * 100)

        # print('Test #{} of {}: '
        #     'Exposure Time Min/Max/Step={}s/{}s/{}s, Configured ROI: {}'.format(
        #         test_num, len(rois_to_test),
        #         min_exp, max_exp, step_size, format(roi, 'cl')))

        if print_header:
            print('{: ^50}|{: ^16}|{: ^33}|{: ^16}|'.format(
                'Exposure Time', 'Exp Diff','Cycle Time', 'Cycle Diff'))
            print('{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: >7}'.format(
                'Req', 'Calc', 'Actual', '(actual-calc)',
                'Calc', 'Actual', '(actual-calc)', 'Status'))

        # Set the camera with the defined ROI (as an AOI)
        aoi = Aoi.from_roi(roi_to_test, self._cam.SensorHeight)
        self._cam.AoiWidth = aoi.width
        self._cam.AoiLeft = aoi.left
        self._cam.AoiHeight = aoi.height
        self._cam.AoiTop = aoi.top

        # The calculator expects a list of ROI arrays.
        roi_list = []
        roi_list.append(roi_to_test.to_array())

        for exp_time in np.arange(
            exp_info[MIN], exp_info[MAX]+exp_info[STEP], exp_info[STEP]):

            counts.num_tests += 1           # increment test counter

            # Clear calculated/actual lists
            cycle_time = [0.0] * 2     # Calcualted/actual cycle time
            exposure_time = [0.0] * 2  # Calculated/actual exposure time
            let = [0.0] * 2            # Calculated/actual longExposureTransition
            readout_time = [0.0] * 2   # Calculated/actual frame readout time
            row_read_time = [0.0] * 2  # Calculated/actual row read time

            # Invoke calculator
            if self._andor._type == options.ZYLA:
                vBin = 1    # Zyla is always 1 for the calculator
            elif self._andor._type == options.BALOR:
                vBin = 1    # Balor calculator needs the vBin value because it
                            # performes it's internal calculations based on
                            # super pixels. This is a place holder until binning
                            # support is added to this test

            (min_exp_time,
                max_exp_time,
                exposure_time[const.IDX_CALC],
                cycle_time[const.IDX_CALC],
                let[const.IDX_CALC]) = self._calc.calculate(exp_time, roi_list, vBin)

            (num_row_reads,
                row_read_time[const.IDX_CALC],
                readout_time[const.IDX_CALC]) = self._calc.get_calculated_extras()

            start = time.perf_counter()

            # Pick exposure time to set on the camera...
            if self._cla.set == SET_DESIRED:
                # Use the desired exposure time...
                exp_time_to_use = exp_time
            elif self._cla.set == SET_CALCULATED:
                # Use the exposure time that was calculated from the desired
                # exposure time.
                exp_time_to_use = exposure_time[const.IDX_CALC]

            ###################### SET THE EXPOSURE TIME #######################
            self._cam.ExposureTime = exp_time_to_use

            stop = time.perf_counter()
            duration = stop - start       # Total time to set exposure time

            stats.aggregate( duration, exp_time_to_use )    # Update stats...

            # Retrieve actual values from the camera...
            cycle_time[const.IDX_ACTUAL] = 1.0 / self._cam.max_FrameRate
            exposure_time[const.IDX_ACTUAL] = self._cam.ExposureTime
            let[const.IDX_ACTUAL] = self._cam.LongExposureTransition
            readout_time[const.IDX_ACTUAL] = self._cam.ReadoutTime
            row_read_time[const.IDX_ACTUAL] = self._cam.RowReadTime

            # Convert actual cycle time (1/Max Frame Rate) to an integral
            # multiple of row read time
            if self._andor._type == options.BALOR:
                rrt = row_read_time[const.IDX_ACTUAL]
                cycle_time[const.IDX_ACTUAL] = int(cycle_time[const.IDX_ACTUAL] / rrt) * rrt
            else:
                rrt = row_read_time[const.IDX_ACTUAL]
                cycle_time[const.IDX_ACTUAL] = misc.round_half_up(cycle_time[const.IDX_ACTUAL] / rrt) * rrt

            warning = ''
            what_failed = ''
            if row_read_time[const.IDX_CALC] != row_read_time[const.IDX_ACTUAL]:
                what_failed += 'RowReadTime '
            if let[const.IDX_CALC] != let[const.IDX_ACTUAL]:
                what_failed != 'LongExposureTransition '
            if self._andor._type == options.ZYLA or self._andor._type == options.BALOR:
                # Verify that both calculated and actual exposure times are within
                # 1 RowReadTime from the desired exposure time and then that the
                # calculated and actual exposure times are equal.

                # Set rough limits for calculated and actual exposure time:
                #   calculated == requested +/- 1rrt
                #       actual == requested +/- 1rrt
                lower = exp_time - row_read_time[const.IDX_CALC]
                upper = exp_time + row_read_time[const.IDX_CALC]

                # Verify calculated value is within the expected range...
                if( exposure_time[const.IDX_CALC] < lower  or
                    exposure_time[const.IDX_CALC] > upper ):
                    what_failed += 'Exp:CalcOutOfRange '

                # Verify actual value is within the expected range ..
                if( exposure_time[const.IDX_ACTUAL] < lower or
                    exposure_time[const.IDX_ACTUAL] > upper ):
                    what_failed += 'Exp:ActualOutOfRange '

                # Verify actual value is what we calculated (+/-1 picosecond)...
                exp_time_diff = exposure_time[const.IDX_ACTUAL] - exposure_time[const.IDX_CALC]
                if abs(exp_time_diff) > 1.0e-12:
                    what_failed += 'Exp:Calc_ne_Actual '

                # Diff between actual and calculated cycle time
                cycle_time_diff = cycle_time[const.IDX_ACTUAL] - cycle_time[const.IDX_CALC]
                if abs(cycle_time_diff) > 1.0e-9:
                    what_failed += 'CycleTime '
            else:
                print('EXPOSURE TIME CHECKS NOT IN PLACE FOR {}'.format(self._andor._type))
                exit()

            status = ''
            if len(what_failed) == 0:
                if len(warning) == 0:
                    status = 'PASS'
                else:
                    status = 'WARN: ' + warning;
            else:
                status = 'FAIL: ' + what_failed;

            if self._cla.debug:
                print('\n'
                    'Calculated Exposure Time Details:\n'
                    '             Max FrameRate: {:.16e}\n'
                    '             Exposure Time:   req={:.16e},   calc={:.16e}, actual={:.16e}\n'
                    '      Exposure Time Limits: lower={:.16e},  upper={:.16e} (== req +/- 1 rrt)\n'
                    '        Row Read Time(rrt):  calc={:.16e}, actual={:.16e}\n'
                    '    LongExposureTransition:  calc={:.16e}, actual={:.16e}\n'
                    '    Frame ReadoutTime     :  calc={:.16e}, actual={:.16e}'.format(
                    self._cam.max_FrameRate,
                    exp_time, exposure_time[const.IDX_CALC], exposure_time[const.IDX_ACTUAL],
                    lower, upper,
                    row_read_time[const.IDX_CALC], row_read_time[const.IDX_ACTUAL],
                    let[const.IDX_CALC], let[const.IDX_ACTUAL],
                    readout_time[const.IDX_CALC], readout_time[const.IDX_ACTUAL]))

            # Assemble and print the test output string...
            print('{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e}, {}'.format(
                exp_time, exposure_time[const.IDX_CALC],
                exposure_time[const.IDX_ACTUAL], exp_time_diff,
                cycle_time[const.IDX_CALC], cycle_time[const.IDX_ACTUAL],
                cycle_time_diff, status))

            if 'PASS' in status:
                counts.num_pass += 1
            elif 'WARN' in status:
                counts.num_warn += 1
            else:
                counts.num_fail += 1

        if counts.num_fail > 0: success = False

        return (success, counts, stats)
    #
    # End of ExposureTimeTests._set_exp_time_and_check_calculated()
