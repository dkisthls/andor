import sys
sys.path.append('../util')

from pyAndorSDK3 import AndorSDK3
import numpy as np

from aoi_roi import Aoi, Roi
from counts import Counts                    # Encapsulates test counters
from tcDefined import tcConstants as const
from tcDefined import tcOptions as options

#===============================================================================
# CLASS: HLSOPS_1828
#===============================================================================

class HLSOPS_1828():
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, camera, cmdLineArgs):
        self._andor = camera        # The encapsulating camera class
        self._cam = camera.cam      # The SDK3 camera object
        self._cla = cmdLineArgs     # The command line arguments
        self._calc = None           # The camera calculator
        self._step_size = self._cla.stepSize  # Fraction of RowReadTime

        # Get some values from the camera
        self._let = self._cam.LongExposureTransition
        self._rrt = self._cam.RowReadTime
        self._startup_exp_time = self._cam.ExposureTime

        # Establish max frame rate at the LongExposureTransition
        self._cam.ExposureTime = self._let
        self._let_max_frame_rate = self._cam.max_FrameRate

        # Reset exposure time to startup value
        self._cam.ExposureTime = self._startup_exp_time
    #
    # End of HLSOPS_1828.__init__()


    #----------------------------- execute() -----------------------------------
    #
    def execute(self, calculator, rois_to_test):
        success = True
        test_counts = Counts()
        total_counts = Counts()

        # The calculator being passed in has been configured for a specific
        # trigger, shutter, and gain mode. Save it for later use.
        self._calc = calculator

        # Establish the set of exposure times to be tested where:
        #  LET == LongExpsoureTranstion
        #  RRT == RowReadTime
        #                  LET - RRT <= Exposure Time < LET
        exp_min = self._let - self._rrt
        exp_max = self._let - self._step_size * self._rrt
        exp_step = self._step_size * self._rrt
        exposure_times = np.arange(exp_min, exp_max, exp_step)

        for roi in rois_to_test:
            # Set the camera with the defined ROI (as an AOI)
            aoi = Aoi.from_roi(roi, self._cam.SensorHeight)
            self._cam.AoiLayout = 'Image'
            self._cam.AoiWidth = aoi.width
            self._cam.AoiLeft = aoi.left
            self._cam.AoiHeight = aoi.height
            self._cam.AoiTop = aoi.top
            print('\n     Configured ROI: {}'.format(format(roi, 'cl')))
            print('     Configured AOI: {}'.format(format(aoi, 'cl')))

            # On the camera, set the exposure time to the LongExposureTransntion
            # and retrieve the Max FrameRate from the camera.
            self._let = self._cam.LongExposureTransition
            self._cam.ExposureTime = self._let
            self._let_max_frame_rate = self._cam.max_FrameRate

            # Run calculator to get the calculated cycle time and
            # LongExposureTransition values for this ROI.
            (x1, x2, x3,
                calc_cycle_time, calc_let) = self._calc.calculate(self._let, [roi.to_array()], 1)

            (x1, calc_rrt, x2) = self._calc.get_calculated_extras()

            # Perform a 1 time check to verify that the calcualted and actual
            # RowReadTimes are correct.
            status = 'PASS'
            if calc_rrt != self._rrt:
                status = 'FAIL'
                total_counts.num_fail += 1
            print('          RRT Check: Expected={:.9f}, Actual={:.9f}...{}'.format(
                calc_rrt, self._rrt, status))

            # Perform a 1 time check to verify that the calcualted and actual
            # LongExposureTransition values, for the current ROI, are correct.
            status = 'PASS'
            if calc_let != self._let:
                status = 'FAIL'
                total_counts.num_fail += 1
            print('          LET Check: Expected={:.9f}, Actual={:.9f}...{}'.format(
                calc_let, self._let, status))

            # Perform a 1 time check to verify that the max_FrameRate read from
            # the camera is equal to the 1/calculatedCycleTime when the exposure
            # time is set to the value of the LongExposureTransition and the
            # camera is configured for the test ROI. For this check we only care
            # about the calculated cycle_time.
            cycle_time_diff = calc_cycle_time - (1.0 / self._let_max_frame_rate)
            status = 'PASS'
            if abs(cycle_time_diff) > 1.0e-9:
                status = 'FAIL'
                total_counts.num_fail += 1

            print('LET CycleTime Check: Expected={:.16e}, Actual={:.16e}...{}'.format(
                    calc_cycle_time, 1.0/self._let_max_frame_rate, status))

            # NOTE:
            # Assuming the above check passed, we now know when the exposure time
            # is equal to the LongExposureTransition, the calculated cycle time
            # is correct. We can safely compare 1/camMaxFrameRate with the
            # calculated cycle time when checking the problem where the camera is
            # reporting the incorrect max FrameRate when the exposure time is
            # slightly < LET and we then set the exposure time to the LET.

            # Now, run the tests...
            test_counts = self._test_exposure_times(1, ['<', '==', '>'], exposure_times, roi)
            total_counts.add_counts(test_counts)

            test_counts = self._test_exposure_times(2, ['>', '==', '<'], exposure_times, roi)
            total_counts.add_counts(test_counts)

            test_counts = self._test_exposure_times(3, ['<', '>', '=='], exposure_times, roi)
            total_counts.add_counts(test_counts)

            test_counts = self._test_exposure_times(4, ['>', '<', '=='], exposure_times, roi)
            total_counts.add_counts(test_counts)

        # Return camera to full frame, no binning...
        self._andor.reset_aoi_bin()

        if total_counts.num_fail > 0:
            success = False

        return (success, total_counts)
    #
    # End of HLSOPS_1828.execute()


    ############################################################################
    #                          INTERNAL USE ONLY
    #

    #--------------------- _calculate_and_compare() ----------------------------
    #
    def _calculate_and_compare(self, roi_list, req_exp_time):
        # Clear calculated/actual lists
        cycle_time = [0.0] * 2   # Calculated/actual cycle time
        exposure_time = [0] * 2  # Calculated/actual expsoure time
        let = [0.0] * 2          # Calculated/actual LongExposureTransition

        # Invoke calculator
        if self._andor._type == options.ZYLA:
            vBin = 1    # Zyla is always 1 for the calculator
        elif self._andor._type == options.BALOR:
            vBin = 1    # Balor calculator needs the vBin value because it
                        # performes it's internal calculations based on
                        # super pixels. This is a place holder until binning
                        # support is added to this test

        (min_exp_time,
            max_exp_time,
            exposure_time[const.IDX_CALC],
            cycle_time[const.IDX_CALC],
            let[const.IDX_CALC]) = self._calc.calculate(req_exp_time, roi_list, vBin)

        # Retrieve actual values from camera...
        cycle_time[const.IDX_ACTUAL] = 1.0 / self._cam.max_FrameRate
        exposure_time[const.IDX_ACTUAL] = self._cam.ExposureTime
        let[const.IDX_ACTUAL] = self._cam.LongExposureTransition

        warning = ''               # calculated value > actual, ok
        what_failed = ''

        # Verify actual value is what we calculated (+/-1 picosecond)...
        exp_time_diff = exposure_time[const.IDX_ACTUAL] - exposure_time[const.IDX_CALC]
        if abs(exp_time_diff) > 1.0e-12:
            what_failed += 'Exp:Calc_ne_Actual '

        # Verify actual cycle time is what we calcualted (+/- 1 nanosecond)...
        cycle_time_diff = cycle_time[const.IDX_ACTUAL] - cycle_time[const.IDX_CALC]
        if abs(cycle_time_diff) > 1.0e-9:
            what_failed += 'CycleTime(Max FrameRate) '

        status = ''
        if len(what_failed) == 0:
            if len(warning) == 0:
                status = 'PASS'
            else:
                status = 'WARN: ' + warning;
        else:
            status = 'FAIL: ' + what_failed;

        if self._cla.debug:
            print("Calculated Values Details:\n"
                  '  {}\n'
                  '           Exposure Time:  req={:.16e},   calc={:.16e}, actual={:.16e}\n'
                  '  LongExposureTransition: calc={:.16e}, actual={:.16e}\n'
                  '               CycleTime: calc={:.16e}, actual={:.16e}\n'
                  '           Max FrameRate: calc={:.16e}, actual={:.16e}\n'.format(
                    self._calc.get_calculated_extras_as_string(),
                    req_exp_time, exposure_time[const.IDX_CALC], exposure_time[const.IDX_ACTUAL],
                    let[const.IDX_CALC], let[const.IDX_ACTUAL],
                    cycle_time[const.IDX_CALC], cycle_time[const.IDX_ACTUAL],
                    1.0/cycle_time[const.IDX_CALC], 1.0/cycle_time[const.IDX_ACTUAL]))

        return (status, exposure_time, cycle_time)
    #
    # End of HLSOPS_1828._calculate_and_compare()


    #----------------------- _print_table_header() -----------------------------
    #
    def _print_table_header(self, description):
        print(description)
        print('{}|{: ^15}|{:7}|{: ^50}|{: ^33}|{: ^17}|'.format(
            'Test', 'Exposure Time', ' ', 'Exposure Time(s)', 'CycleTime(s)', 'FrameRate(Hz)'))
        print('{: ^4}|{: ^15}|{:7}|{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: ^16}|{: ^8}|{: ^8}| {}'.format(
            '#', 'Formula', ' ', 'Requested', 'Calculated', 'Actual', 'Calculated', 'Actual', 'Calc', 'Actual', 'Status'))
    #
    # End of HLSOPS_1828._print_table_header()


    #------------------------ _print_seperator() -------------------------------
    #
    def _print_seperator(self):
        print('-' * 100)
    #
    # End of HLSOPS_1828._print_seperator()


    #---------------------- _test_exposure_times() -----------------------------
    #
    def _test_exposure_times(self, scenario_num, order, exposure_times, roi):
        counts = Counts()

        # The calculator expects a list of ROI arrays. This is a single ROI
        # test so the list will contain one ROI array.
        roi_list = []
        roi_list.append(roi.to_array())

        self._print_seperator()

        self._print_table_header('Scenario {}: ExpTime {} LET, ExpTime {} LET, ExpTime {} LET'.format(
            scenario_num, order[0], order[1], order[2]))

        # Reset expsoure time to startup value before running tests
        self._cam.ExposureTime = self._startup_exp_time

        test_num = 1
        inc = 1.00        # For tracking current fraction of RowReadTime
        for exp_time_lt_let in exposure_times:
            counts.num_tests += 1

            # The exposure time greater than the LET is always the value that
            # is less than + 1 RowReadTime
            exp_time_gt_let = exp_time_lt_let + self._rrt

            first = True
            for what in order:
                # We want the test number to appear on the same line as the
                # first 'what' in the current 'order'.
                test_num_str = ' ' * 4
                if first:
                    test_num_str = '{:0>4}'.format(test_num)
                    first = False

                if what == '>':
                    self._cam.ExposureTime = exp_time_gt_let

                    (status, exp, cyc) = self._calculate_and_compare( roi_list, exp_time_gt_let )
                    print('{}:{: ^15}:  > LET:{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e},{: ^8.3f},{: ^8.3f}, {}'.format(
                        test_num_str, 'req+rrt',
                        exp_time_gt_let,  exp[const.IDX_CALC], exp[const.IDX_ACTUAL],
                        cyc[const.IDX_CALC], cyc[const.IDX_ACTUAL],
                        1.0/cyc[const.IDX_CALC], 1.0/cyc[const.IDX_ACTUAL], status))

                elif what == '<':
                    self._cam.ExposureTime = exp_time_lt_let

                    (status, exp, cyc) = self._calculate_and_compare( roi_list, exp_time_lt_let )

                    print('{}: let-({:.02f}*rrt):  < LET:{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e},{: ^8.3f},{: ^8.3f}, {}'.format(
                        test_num_str, inc,
                        exp_time_lt_let,  exp[const.IDX_CALC], exp[const.IDX_ACTUAL],
                        cyc[const.IDX_CALC], cyc[const.IDX_ACTUAL],
                        1.0/cyc[const.IDX_CALC], 1.0/cyc[const.IDX_ACTUAL], status))

                elif what == '==':
                    status = 'PASS'
                    self._cam.ExposureTime = self._let

                    (status, exp, cyc) = self._calculate_and_compare( roi_list, self._let )
                    # if self._cam.max_FrameRate != self._let_max_frame_rate:
                    #     status = 'FAIL'
                    print('{}:{:15}: == LET:{:16.9e},{:16.9e},{:16.9e},{:16.9e},{:16.9e},{: ^8.3f},{: ^8.3f}, {}'.format(
                        test_num_str, ' ',
                        self._let, exp[const.IDX_CALC], exp[const.IDX_ACTUAL],
                        cyc[const.IDX_CALC], cyc[const.IDX_ACTUAL],
                        1.0/cyc[const.IDX_CALC], 1.0/cyc[const.IDX_ACTUAL], status))

                else:
                    print("UNKNOWN order designator '{}' in HLSOPS_1828.py".format(what))
                    status = 'FAIl'

            if 'PASS' in status:
                counts.num_pass += 1
            else:
                if 'WARN' in status:
                    counts.num_warn += 1
                else:
                    counts.num_fail += 1

            test_num += 1
            inc -= self._step_size

        return counts
    #
    # End of HLSOPS_1828._test_exposure_times()
