import sys
sys.path.append('../../util')

import math
import numpy

import misc                                  # Miscellaneous util methods
from aoi_roi import Aoi, Roi
from zc_defined import zcConstants as const
from zc_defined import zcDefaults as default


class ZylaCalculator:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self):
        # Initialization values
        self._shutter_mode  = default.SHUTTER_MODE
        self._trigger_mode  = default.TRIGGER_MODE
        self._trigger_delay = 0.0
        self._vBin          = default.VBIN
        self._sensor_height = const.SENSOR_HEIGHT
        self._aoi_list      = []
        self._roi_list      = []
        self._fastAoiFrameRateEnable = default.FAST_AOI_FRAME_RATE_ENABLE
        self._pixel_scan_rate = default.PIXEL_SCAN_RATE
        self._clock_period  = default.CLOCK_PERIOD
        self._row_read_time = default.ROW_READ_TIME
        self._interframe    = default.INTERFRAME

        # Additional values that will be calculated
        self._min_exposure_time = 0.0      # Minimum exposure time
        self._max_exposure_time = 0.0      # Maximum exposure time
        self._act_exposure_time = 0.0      # Actual exposure time
        self._num_row_reads = 0            # Total number of row reads
        self._readout_time = 0.0           # Time to readout a frame
        self._cycle_time = 0.0             # 1/_cycle_time == max frame rate
        self._long_exposure_transition = 0.0

        self._reset_calculated()
    #
    # End of __init__()


    #---------------------------- __str__() ------------------------------------
    #
    def __str__(self):
#        return f'{vars(self)}'
        return str(""
            "Python Calculator Parameters/Values:\n"
            "            constants: sensorHeight=%d\n"
            "          shutterMode=\"%s\", triggerMode=\"%s\"\n"
            "         triggerDelay=%f\n"
            "          verticalBin=%d\n"
            "              numROIs=%d\n"
            "              roiList=%s""" % (
                self._sensor_height,
                self._shutter_mode, self._trigger_mode,
                self._trigger_delay,
                self._vBin,
                len(self._roi_list), self._roi_list ) )
    #
    # End of ZylaCalculator.__str__()


    #---------------------------- configure() ----------------------------------
    #
    def configure(self, shutterMode, triggerMode, triggerDelay, fastAOI,
        enumPixelScanRate=const._280MHz):
        self._shutter_mode  = shutterMode
        self._trigger_mode  = triggerMode
        self._trigger_delay = triggerDelay
        self._fastAoiFrameRateEnable = fastAOI
        self._pixel_scan_rate = const.ACTUAL_PIXEL_SCAN_RATES[enumPixelScanRate] * const.HZ_PER_MHZ
        self._clock_period  = 1.0 / self._pixel_scan_rate
        self._row_read_time = const.CLOCK_CYCLES_PER_ROW_READ * self._clock_period
        self._interframe    = 9 * self._row_read_time
    #
    # End of ZylaCalculator.configure()


    #---------------------------- calculate() ----------------------------------
    # NOTE: Calculations are based on current shutter mode, trigger mode, and
    #       acquiring state.
    # NOTE: Don't need to pass in actual exposuretime - will calc actual exp as 
    #       part of framerate calc
    # Input:
    #   desired_exp_time - The desired exposure time (in sec).
    #   roiList - A list or ROIs which will be used to determine the total
    #             number of row reads.
    #   vBin - The vertical binning value. Note that the Zyla Calcualtor does
    #          not need vertical binning. The value is optional and only
    #          provided for call compatability with the Balor calculator.
    #          All ROIs provided to the calculator are in sensor pixels, not
    #          super pixeld so there is no need to back out any binning that
    #          may be applied and reflected in super-pixels.
    # 
    # Return: (min_exposure_time, max_exposure_time, act_exposure_time,
    #          cycle_time, long_exposure_transition)
    #   min_exposure_time:
    #   The minimum exposure time based on acquiring state and shutter mode:
    #
    #     acquiring=true: Value depends on current shutter mode
    #        Global/Rolling: The minimum value for either a short or long
    #                        exposure.
    #        Overlap: Essentially 1/frameRate (CURRENTLY NOT IMPLEMENTED)
    #
    #    acquiring=false: The absolute minimum for the camera and the current
    #                     shutter mode.
    #   max_exposure_time:
    #   The maximum exposure time based on acquiring state and shutter mode:
    #
    #     acquiring=true: Value depends on current shutter mode
    #        Global/Rolling: The maximum value for either a short or long
    #                        exposure.
    #        Overlap: Essentially 1/frameRate (CURRENTLY NOT IMPLEMENTED)
    #
    #     acquiring=false: The absolute maximum for the camera and the current 
    #                      shutter mode.
    #
    #   act_exposure_time:
    #   The calculated actual exposure time based on the desired_exp_time
    #
    #   cycle_time:
    #   The calculated cycle time which is the time from when the camera is
    #   triggered to when it is ready to be triggered again. The inverse
    #   cycleTime (1/cycleTime) is the maximum frame rate.
    #
    #   long_exposure_transition:
    #   The boundary beteen long and short exposures. Crossing this boundary
    #   from a current exposure time to a new exposure time requires acquisition
    #   be stopped on the camera.
    #      Long Exposure: desired_exp_time >= longExposureTransition
    #     Short Exposure: desired_exp_time < longExposureTransition
    # 
    #
    def calculate(self, desired_exp_time, roiList, vBin=default.VBIN):
        # Save vertical binning value.
        # We're not using it right now but maybe later?
        self._vBin = vBin

        # Convert the list or ROI arrays to a list of Roi objects and save
        self._roi_list.clear()
        for r in roiList:
            self._roi_list.append(Roi.from_array(r))

        # ROIs are increasing origin order while AOIs need to be in
        # increasing top order.
        # The list of ROIs needs to be converted to a list of AOIs while being
        # swapped (processed in reverse order).
        self._aoi_list.clear()
        for roi in reversed(self._roi_list):
            self._aoi_list.append(Aoi.from_roi(roi, const.SENSOR_HEIGHT))

        # Reset all calcualted valued
        self._reset_calculated()

        # Calculate the min/max and actual exposure times.
        self._min_exposure_time = self._calc_min_exposure_time()

        self._max_exposure_time = self._calc_max_exposure_time()

        self._act_exposure_time = self._calc_act_exposure_time(desired_exp_time)

        # Determine number of row reads. This is the basis for calculating
        # the frame read time and the long exposure transition both of which
        # are needed to calculate the cycle time.

        # self._num_row_reads = self._calc_num_row_reads_roi(self._roi_list)
        self._num_row_reads = self._calc_num_row_reads(self._aoi_list)
        # sts = 'PASS'
        # if self._num_row_reads != nrr: sts = 'FAIL'
        # print('ORIG={}, NEW={}, {}'.format(self._num_row_reads, nrr, sts))

        # Calculate frame readout time
        self._readout_time = self._calc_readout_time(self._num_row_reads)

        # Calculate the LongExposureTransition
        self._long_exposure_transition = \
            self._calc_long_exposure_transition(self._readout_time)

        # Calculate the cycle time. 1/cycletime is the maximum exposure rate
        # Cycle time depends on the actual exposure time and calculated
        # frame readout time
        self._cycle_time = \
            self._calc_cycle_time(self._act_exposure_time,
                self._long_exposure_transition, self._readout_time)

        return [self._min_exposure_time, self._max_exposure_time,
                self._act_exposure_time, self._cycle_time,
                self._long_exposure_transition]
    #
    # End of ZylaCalculator.calculate()


    #-------------------- get_all_params_as_string() ---------------------------
    #
    def get_all_params_as_string(self):
        return ( self.__str__() )
    #
    # End of ZylaCalculator.get_all_params_as_string()


    #---------------------- get_calculated_extras() ----------------------------
    #
    def get_calculated_extras(self):
        return [int(self._num_row_reads),
                self._row_read_time,
                self._readout_time]
    #
    # End of ZylaCalculator.get_calculated_extras()


    #----------------- get_calculated_extras_as_string() -----------------------
    #
    def get_calculated_extras_as_string(self):
        (num_row_reads, row_read_time, readout_time) = self.get_calculated_extras()
        return str(""
            "numRowReads=%d, rowReadTime=%.09fsec, readoutTime=%.09fsec""" % (
                num_row_reads, row_read_time, readout_time) )
    #
    # End of ZylaCalculator.get_calculated_extras_as_string()


    ############################################################################
    #                          INTERNAL USE ONLY
    #

    #------------------ _calc_cycle_time() -------------------------------------
    #
    #  act_exp_time: The actual exposure time based on requested value
    #           let: Calculated LongExposureTransition
    #  readout_time: Calculated frame read time
    #
    def _calc_cycle_time(self, act_exp_time, let, readout_time):
        cycle_time = 0.0

        if self._shutter_mode == const.SHUTTER_MODES[const.GLOBAL]:

            if self._trigger_mode == const.TRIGGER_MODES[const.INTERNAL]:
                if act_exp_time < let:                              # short exposure
                    cycle_time = act_exp_time + \
                                 (2 * readout_time) + \
                                 (2 * self._interframe)
                else:                                               # long exposure
                    cycle_time = act_exp_time + \
                                 readout_time + \
                                 (2 * self._interframe)

            else:     # triggerMode == SOFTWARE | EXTERNAL
                if act_exp_time < let:                              # short exposure
                    cycle_time = act_exp_time + \
                                 (2 * readout_time) + \
                                 (2 * self._interframe) + \
                                 (2 * self._row_read_time)
                else:                                               # long exposure
                    cycle_time = act_exp_time + \
                                 readout_time + \
                                 (2 * self._interframe)

        else:         # shutterMode == ROLLING
            if self._trigger_mode == const.TRIGGER_MODES[const.INTERNAL]:
                cycle_time = act_exp_time + \
                             readout_time + \
                             (2 * self._row_read_time)
            else:     # triggerMode == SOFTWARE | EXTERNAL
                cycle_time = act_exp_time + \
                             readout_time + \
                             (5 * self._row_read_time)



        return cycle_time
    #
    # End of ZylaCalculator._calc_cycle_time()


    #------------------ _calc_act_exposure_time() ------------------------------
    # Utility method used to calculate the actual exposure time based on the
    # supplied desired exposure time.
    #
    def _calc_act_exposure_time(self, desired_exp_time):
        row_reads = misc.round_half_up(desired_exp_time / self._row_read_time)
        return row_reads * self._row_read_time
    #
    # End of ZylaCalculator._calc_act_exposure_time()


    #------------------ _calc_min_exposure_time() ------------------------------
    #
    # Utiltiy method used to calcuate the minimum exposure time based on the
    # current shutter and trigger mode.
    # NOTE: Only a limited subset of all trigger/shutter mode configurations are
    # supported: Global/Rolling: Internal/Software/External. Overlap is not
    # supported at this time.
    #
    def _calc_min_exposure_time(self):
        min_exp_time = 0.0

        if self._shutter_mode == const.SHUTTER_MODES[const.GLOBAL]:
            min_exp_time = self._row_read_time
        else:
            if self._trigger_mode == const.TRIGGER_MODES[const.INTERNAL]:
                min_exp_time = self._row_read_time
            else:
                min_exp_time = 3.0 * self._row_read_time

        # Convert to 100's of picoseconds, round up to the next 100 picoseconds
        # then convert back to seconds. This eliminates the need for fractional
        # seconds out to many decimal places and makes sure the minimum expsoure
        # time allowed by the CSS is slightly larger than the minimum allowed by
        # the camera as calculated above.
        min_exp_time = math.ceil(min_exp_time * 10000000000.0) / 10000000000.0

        return min_exp_time
    #
    # End of ZylaCalculator._calc_min_exposure_time()


    #--------------------- _calc_max_exposure_time() ---------------------------
    #
    # Utility method used to calcuate the maximum exposure time.
    #
    def _calc_max_exposure_time(self):
        return 30.0                         # in seconds
    #
    # End of ZylaCalculator._calc_max_exposure_time()


    #----------------------- _calc_readout_time() ------------------------------
    #
    def _calc_readout_time(self, num_row_reads):
        return num_row_reads * self._row_read_time
    #
    # End of ZylaCalculator._calc_readout_time()


    #------------------ _calc_long_exposure_transition() -----------------------
    #
    def _calc_long_exposure_transition(self, readout_time):
        let = 0.0

        if self._shutter_mode == const.SHUTTER_MODES[const.GLOBAL]:
            let = readout_time + (3.5 * self._row_read_time)

        return let
    #
    # End of ZylaCalculator._calc_long_exposure_transition()


    #----------------------- _calc_num_row_reads() -----------------------------
    #
    # NOTE: We deal with raw pixel AOIs, not super-pixel AOIs. Therefore, we do
    #       not need to account for binning having already been applied to an
    #       AOI definition in super-pixels.
    def _calc_num_row_reads(self, aoiList, buffer_rows=default.BUFFER_ROWS):
        aoi = aoiList[0]                    # extract first AOI from list
        half_height = const.SENSOR_HEIGHT / 2

        if len(aoiList) > 1:
            # Multiple AOIs will result in using Multitracks...
            aoi_top = aoiList[0].top
            aoi_height = aoiList[-1].top + aoiList[-1].height - aoi_top

            if aoi_height < const.MIN_AOI_HEIGHT:
                aoi_height = const.MIN_AOI_HEIGHT

                # Also, readjust aoi_top
                aoi_top_max = const.SENSOR_HEIGHT - const.MIN_AOI_HEIGHT + 1
                aoi_top = min(aoi_top, aoi_top_max)

            # At this point we've converted the Multitracks into a single,
            # all encompassing, AOI. Now process as a single AOI.
            # Replace the first AOI with this new, single AOI that encompasses all
            # of the Multitracks.
            #        (original    new    original     new     )
            aoi = Aoi(aoi.left, aoi_top, aoi.width, aoi_height)

        # Now calculate for a single AOI
        top = aoi.top - 1                   # Zero base AOI top
        top_row = max(top - buffer_rows, 0)
        bottom_row = min(top + aoi.height + buffer_rows, const.SENSOR_HEIGHT)

        rows_in_top_half, rows_in_bottom_half = 0, 0
        if bottom_row < half_height or top_row > half_height:
            rows_in_top_half = bottom_row - top_row
            rows_in_bottom_half = 0
        else:
            rows_in_top_half = half_height - top_row
            rows_in_bottom_half = bottom_row - half_height

        num_row_reads = max(rows_in_top_half, rows_in_bottom_half)

        # Total number of row reads must be an even number
        if num_row_reads % 2 != 0:
            num_row_reads += 1              # Increment to make even

        return num_row_reads
    #
    # End of ZylaCalcualtor._calc_num_row_reads()


    #---------------------------  _reset_calculated() --------------------------
    #
    def _reset_calculated(self):
        self._act_exposure_time  = 0.0         # Actual exposure time
        self._min_exposure_time  = 0.0         # Minimum exposure time
        self._max_exposure_time  = 0.0         # Maximum exposure time
        self._readout_time       = 0.0         # Time to readout a frame
        self._num_row_reads      = 0           # Total number of row reads
        self._cycle_time         = 0.0         # 1/_cycle_time == max frame rate
        self._long_exposure_transition = 0.0   #
    #
    # End of ZylaCalcualtor._reset_calculated()
