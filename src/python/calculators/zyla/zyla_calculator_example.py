from zyla_calculator import ZylaCalculator, Roi

wrapper = ZylaCalculator()

#================================ EXAMPLE #1 ===================================
#
print("==================== Global Shutter, 1 ROI")
shutter_mode = "Global"
trigger_mode = "External"
trigger_delay = 0.0
fast_aoi = False                # FastAOIFrameRateEnable
roi_list = [(0,0,2560,2160)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

wrapper.configure(shutter_mode, trigger_mode, trigger_delay, fast_aoi)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    wrapper.calculate(exposure_time, roi_list)

print(wrapper.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.07f}ms/{:.06f}sec\n"
      "              act_exp_time={:.06f}ms\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        wrapper.get_calculated_extras_as_string(),
        min_exp_time * 1000.0, max_exp_time,
        act_exp_time * 1000.0,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))

#================================ EXAMPLE #2 ===================================
#
print("==================== Rolling Shutter, 1 ROI")
shutter_mode = "Rolling"
trigger_mode = "External"
trigger_delay = 0.0
roi_list = [(0,0,2560,2160)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

wrapper.configure(shutter_mode, trigger_mode, trigger_delay, fast_aoi)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    wrapper.calculate(exposure_time, roi_list)

print(wrapper.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.07f}ms/{:.06f}sec\n"
      "              act_exp_time={:.06f}ms\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        wrapper.get_calculated_extras_as_string(),
        min_exp_time * 1000.0, max_exp_time,
        act_exp_time * 1000.0,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))

#================================ EXAMPLE #3 ===================================
#
print("==================== Global Shutter, 2 ROIs(Multitrack)")
shutter_mode = "Global"
trigger_mode = "External"
trigger_delay = 0.0
roi_list = [(0,0,2560,1000),(0,1160,2560,1000)]
exposure_time = 0.1
exposureRate = 1.0

print("Desired exposure time={}s\n".format(exposure_time))

wrapper.configure(shutter_mode, trigger_mode, trigger_delay, fast_aoi)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    wrapper.calculate(exposure_time, roi_list)

print(wrapper.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.07f}ms/{:.06f}sec\n"
      "              act_exp_time={:.06f}ms\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        wrapper.get_calculated_extras_as_string(),
        min_exp_time * 1000.0, max_exp_time,
        act_exp_time * 1000.0,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #4 ===================================
#
print("==================== Rolling Shutter, 2 ROIs(Multitrack)")
shutter_mode = "Rolling"
trigger_mode = "External"
trigger_delay = 0.0
roi_list = [(0,0,2560,1000),(0,1160,2560,1000)]
exposure_time = 0.0005
exposureRate = 1.0

print("Desired exposure time={}s\n".format(exposure_time))

wrapper.configure(shutter_mode, trigger_mode, trigger_delay, fast_aoi)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    wrapper.calculate(exposure_time, roi_list)

print(wrapper.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.07f}ms/{:.06f}sec\n"
      "              act_exp_time={:.06f}ms\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        wrapper.get_calculated_extras_as_string(),
        min_exp_time * 1000.0, max_exp_time,
        act_exp_time * 1000.0,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))
