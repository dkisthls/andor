import numpy

###------------------------------ zcConstants ----------------------------------
class zcConstants:
  DEBUG = False
  CHARGE_TRANSFER_TIME_S = 0.000002
  CLOCK_CYCLES_PER_ROW_READ = 2624
  SENSOR_WIDTH = 2560                # Sensor array width(x) in pixels
  SENSOR_HEIGHT = 2160               # Sensor array height(y) in pixels
  MIN_AOI_HEIGHT = 8
  MS_PER_SECOND = 1.0e3
  NS_PER_SECOND = 1.0e9
  HZ_PER_MHZ    = 1.0e6

  # Define the pixel scan rates for the camera
  _100MHz = '100 MHz'
  _280MHz = '280 MHz'
  ACTUAL_PIXEL_SCAN_RATES = {_100MHz: 103.3, _280MHz: 284.0}

  # Define the gain modes for the camera (just for reference, not used)
  _12BIT_HIGH_WELL = 0
  _12BIT_LOW_NOISE = 1
  _16BIT_LOW_NOISE_HIGH_WELL = 2
  GAIN_MODES = ['12-bit (high well capacity)',
                '12-bit (low noise)',
                '16-bit (low noise & high well capacity)']

  # Define the ElectronicShutteringModes for the camera
  GLOBAL  = 0
  ROLLING = 1
  SHUTTER_MODES = ['Global', 'Rolling']

  # Define the Trigger Modes for the camera
  INTERNAL = 0
  SOFTWARE = 1
  EXTERNAL = 2
  TRIGGER_MODES = ['Internal', 'Software', 'External']

###------------------------------ zcDefaults -----------------------------------
class zcDefaults:
  PIXEL_SCAN_RATE = zcConstants.ACTUAL_PIXEL_SCAN_RATES[zcConstants._280MHz] * zcConstants.HZ_PER_MHZ
  CLOCK_PERIOD    = 1.0 / PIXEL_SCAN_RATE
  ROW_READ_TIME   = zcConstants.CLOCK_CYCLES_PER_ROW_READ * CLOCK_PERIOD
  INTERFRAME      = 9 * ROW_READ_TIME
  FAST_AOI_FRAME_RATE_ENABLE = False
  SHUTTER_MODE = zcConstants.SHUTTER_MODES[zcConstants.GLOBAL]
  TRIGGER_MODE = zcConstants.TRIGGER_MODES[zcConstants.EXTERNAL]
  VBIN = 1
  BUFFER_ROWS = 2
