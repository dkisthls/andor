#calc_exposure.py
import math
import MultitrackHelper as mh

#### USER SETTINGS ####
#es_mode = "Rolling"
#es_mode = "Rolling - 100% Duty Cycle"
es_mode = "Global"
#es_mode = "Global - 100% Duty Cycle"
height = 4104
aoitop = 1
vbin = 1
trigger_mode = "External"
acquiring = False
trigger_delay = 0
overlap = "100% Duty Cycle" in es_mode

               #(       str, int,  str, bool,    float, int, int)
def set_Values(shutter_mode,   h, trig,  acq, trig_dly, top, bin):
    global es_mode
    es_mode = shutter_mode
    global height
    height = h
    global trigger_mode
    trigger_mode = trig
    global acquiring
    acquiring = acq
    global trigger_delay
    trigger_delay = trig_dly
    global aoitop
    aoitop = top
    global vbin
    vbin = bin
    global overlap
    overlap = "100% Duty Cycle" in es_mode

multitrack=False
def set_Multitrack(enabled, start_end_binned_list=[]):
    global multitrack 
    multitrack = enabled
    if enabled:
        if len(start_end_binned_list) <= 0:
            raise ValueError("start_end_binned_list cannot be empty if enabling multitrack")
        while (mh.GetNumberTracks() > len(start_end_binned_list)):
            mh.Remove()
        while (mh.GetNumberTracks() < len(start_end_binned_list)):
            mh.Add()

        for index, entry in enumerate(start_end_binned_list):
            mh.SetTrackStart(index, entry[0])
            mh.SetTrackEnd(index, entry[1])
            mh.SetTrackBinned(index, entry[2])

        mh.RealignTrackStartAndEnd()

#######################

clock_freq = 28000000
UINTMAX = 2**31 -1

def _get_ExternalTriggerDelay():
    return (trigger_delay / (clock_freq *2) )* (clock_freq*2)

def _get_LongExposureTransition():
    RRT = _get_RowReadTime()
    RO = _get_ReadoutTime()
    return RO + 20 * RRT

def _get_ExposureType(exposuretime):
    if "Global" in es_mode:
        if overlap or trigger_mode == "External Exposure" :
            return "Long"
        if exposuretime < _get_LongExposureTransition() :
            return "Short"
    return "Long"

def _get_TimeResidual():
    if es_mode == "Rolling" : #note rolling NON overlap
        return 420 / clock_freq
    return 0

def _get_RowReadTime():
    if "Global" in es_mode: #Global
        num_pulses = 392
    else:
        num_pulses =  616 #Rolling
        if overlap:
            num_pulses = 504
    return num_pulses / clock_freq

def _get_TotalSensorRows():
    if not multitrack:
        aoistart = 1 + (int(aoitop-1) - int(aoitop-1) % 8)/4
        
        end = aoitop + height * vbin - 1
        if ((end % 8) > 0) :
            end += 8 - (end % 8)
        aoiend =  end/4
        return (aoiend - aoistart + 1)
    else:
        return mh.GetNumberSensorRows() / 4 

def _get_ReadoutTime():
    return _get_TotalSensorRows() * _get_RowReadTime()

def _python2round(f):
    if round(f + 1) - round(f) != 1:
        return f + abs(f) / f * 0.5
    return round(f)

###### ExposureTime ######
def _get_MaxExposureTime(exposuretype):
    if acquiring :
        if "Global" in es_mode :
            if exposuretype == "Short" :
                RO = _get_ReadoutTime()
                RRT = _get_RowReadTime()
                val = RO + 19 * RRT # (LongExposureTransition - 1 rowtime)
                return val
    return 3600.0

def _get_MinExposureTime(exposuretype):
    RO = _get_ReadoutTime()
    RRT = _get_RowReadTime()
    TRes = _get_TimeResidual()

    if acquiring :
        if "Global" in es_mode: # checking LET during acquisition
            if exposuretype == "Long" and not overlap: 
                return _get_LongExposureTransition()

    if "Global" in es_mode : #Global
        if overlap :
            if trigger_mode == "External Exposure" :
                return 2 * RO + 26 * RRT
            else :
                return 2 * RO + 27 * RRT
        else:
            if trigger_mode == "External Exposure" :
                return RO + 16 * RRT
            else:
                return 8 * RRT

    if overlap :#Rolling
        if trigger_mode == "External Exposure" :
            return RO + 3 * RRT
        return RO + RRT
    return RRT + TRes

def calc_exposure(exposuretime):
    ## calculate internal EXP value
    exposuretime = exposuretime + 0.000001
    exposuretype = _get_ExposureType(exposuretime)
    min_exp_time = _get_MinExposureTime(exposuretype)

    if trigger_mode == "External Exposure":
        return min_exp_time
    
    RRT = _get_RowReadTime()
    TRes = _get_TimeResidual()

    if "Global" in es_mode :  #Global
        if exposuretype == "Long" and not overlap:
            min_exp_time = _get_LongExposureTransition()
        additional_exp = exposuretime - min_exp_time + RRT
    else :
        if overlap : #Rolling
            additional_exp = exposuretime
        else :
            additional_exp = exposuretime - TRes
    EXP = int(max(_python2round(additional_exp / RRT),1))

    ## recalculating actual ExposureTime
    if "Global" in es_mode : #Global
        return min_exp_time + (EXP - 1) * RRT
    else :
        if overlap: #Rolling
            return EXP * RRT
        else :
            return EXP * RRT + TRes
###### End ExposureTime ######

###### FraeRate ######
def _get_MaxCycleTime(actual_exposure_time):
    if overlap  :
        exposuretype = _get_ExposureType(actual_exposure_time)
        return _get_MaxExposureTime(exposuretype)
    RRT = _get_RowReadTime()
    RO = _get_ReadoutTime()
    return UINTMAX * RRT + actual_exposure_time + RO

def _get_MinCycleTime(actual_exposure_time):
    RRT = _get_RowReadTime()
    RO = _get_ReadoutTime()
    exposure_type = _get_ExposureType(actual_exposure_time)
    TRes = _get_TimeResidual()

    if "Global" in es_mode:
        if overlap :
            return actual_exposure_time
        elif trigger_mode == "External Exposure" :
            return 23 * RRT + 2 * RO
        elif exposure_type == "Long" :
            return actual_exposure_time + 6 * RRT + RO
        else :
            return actual_exposure_time + 13 * RRT + 2 * RO
    else :
        if overlap :
            return _get_MinExposureTime(exposure_type)
        elif trigger_mode == "External Exposure" :
            return 3 * RRT + RO
        elif trigger_mode == "External" :
            return max(actual_exposure_time + 5 * RRT - TRes, 3 * RRT + RO)
        elif exposure_type == "Long" :
            return max(actual_exposure_time + 3 * RRT - TRes, 2 * RRT + RO)
        else :
            return max(actual_exposure_time + 3 * RRT - TRes, 2 * RRT + RO)

def calc_framerate(exposuretime, framerate):
    cycletime = 1./framerate
    if overlap : # in overlap framerate = exposuretime
        exposuretime = 1./framerate
    
    actual_exposure_time =  calc_exposure(exposuretime)
    if overlap :
        actual_cycle_time = actual_exposure_time
    else :
        min_ct = _get_MinCycleTime(actual_exposure_time)
        if cycletime < min_ct: 
            cycletime = min_ct
        else:
            max_ct = _get_MaxCycleTime(actual_exposure_time)
            if cycletime > max_ct:
                cycletime = max_ct
        # calculate internal CT value
        CT = 0
        if not overlap :
            if (trigger_mode == "Internal") or (trigger_mode == "External Start") :
                additional_delay = cycletime - min_ct
                CT = int(_python2round(max(_python2round(additional_delay / _get_RowReadTime()),0)))

        # calculate actual FPS
        additional_delay = 0
        if (trigger_mode == "Internal") or (trigger_mode == "External Start") :
            additional_delay = CT * _get_RowReadTime()
        elif trigger_mode == "External":
            additional_delay = (CT * _get_RowReadTime()) + _get_ExternalTriggerDelay()
        actual_cycle_time = min_ct + additional_delay
    return 1.0/actual_cycle_time

def calc_cycletime(exposuretime):
    framerate = 1.0 / exposuretime    # Fake rate based on requested exposure time
    return 1.0/calc_framerate(exposuretime, framerate)

######  End FrameRate ######

if __name__ == "__main__":
    print("expecting {}".format( 34.00000799999999401))
    print("actual    {}".format(calc_exposure(34.0)))
