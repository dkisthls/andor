class Track():
    def __init__(self,start,end,binned):
        self.start = start
        self.end = end
        self.binned = binned

store = [Track(1, 1, True)]
rowGranularity = 8
sensorRowGranularity = 4
trackIndex = 0
sensorHeight = 4104
store[0].end = sensorHeight
maxTrackCount = 253

def _SetTrackIndex(index) :
    global store
    global trackIndex
    global maxTrackCount

    if (len(store) > index and index < maxTrackCount) :
        trackIndex = index
        return True

def GetNumberTracks() :
    global store
    return len(store)

def SetTrackStart(index, value) :
    global store

    if (_SetTrackIndex(index)) :
        if (value <= _GetTrackMax(index)) :
            end = store[index].end
            store[index].start = value
            if (value > end) :
                store[index].end = value
            if (index < len(store)-1) and (value >= store[index+1].start):
                SetTrackStart(index+1, value+1)

            _SetTrackIndex(index)
            RealignTrackStartAndEnd()

def SetTrackEnd(index, value) :
    global store

    if (_SetTrackIndex(index)) :
        if (value <= _GetTrackMax(index)) :
            start = store[index].start
            if (value < start) :
                store[index].start = value
            store[index].end = value

            if (index > 0) and (value <= store[index-1].end):
                SetTrackEnd(index-1, value-1)
            elif (index < len(store)-1) and (value >= store[index+1].start):
                SetTrackStart(index+1,value+1)

            _SetTrackIndex(index)
            RealignTrackStartAndEnd()

def _GetTrackStart(index) :
    global store

    if (_SetTrackIndex(index)) :
        return store[index].start
    return -1

def _GetTrackEnd(index) :
    global store

    if (_SetTrackIndex(index)) :
        return store[index].end
    return -1

def _GetTrackMax(index) :
    global store
    global sensorHeight

    if (_SetTrackIndex(index)) :
        return sensorHeight - (len(store) - 1 - index)
    return -1

def SetTrackBinned(index, value):
    global store
    if _SetTrackIndex(index):
        store[index].binned = value

def Add() :
    global store
    global sensorHeight

    if (_SetTrackIndex(len(store) - 1)) :
        if (store[-1].end == sensorHeight) :
            value = sensorHeight
        else :
            value = store[-1].end + 1
        store.append(Track(value, value, True))

def Remove() :
    global store

    if (len(store) > 1) :
        store.pop(-1)
        _SetTrackIndex(len(store))

def RealignTrackStartAndEnd() :
    global store
    global trackIndex

    if (len(store) == 1) :
        return

    index = trackIndex
    if (index < len(store) - 1) :
        currentEnd = store[index].end
        for s in store[index + 1:-1] :
            if (s.start <= currentEnd) :
                s.start = currentEnd + 1
            if (s.end < s.start) :
                s.end = s.start
            currentEnd = s.end

    for index, s in reversed(list(enumerate(store))):
        if (index > 0) :
            currentStart = s.start
            previous = store[index - 1]
            if (previous.end >= currentStart) :
                previous.end = currentStart - 1
            if (previous.start > previous.end) :
                previous.start = previous.end

def _GetFPGATrackStart(index) :
    start = -1
    if (_SetTrackIndex(index)) :
        start = _GetTrackStart(index)
        start = _roundDown(start - 1, rowGranularity)
        start = 1 + (start/sensorRowGranularity)
    return start

def _GetFPGATrackEnd(index) :
    end = -1
    if (_SetTrackIndex(index)) :
        end = _GetTrackEnd(index)
        end = _roundUp(end, rowGranularity)
        end /= sensorRowGranularity # previously adding 1 to this
    return end

def GetNumberSensorRows() :
    count = GetNumberTracks()
    number = 0
    PreviousEndAddress = 0
    for index in range(0, count) :
        StartAddress = _GetFPGATrackStart(index)
        EndAddress = _GetFPGATrackEnd(index)

        if EndAddress <= PreviousEndAddress :
            continue

        PreviousEndAddress = EndAddress
        number += (EndAddress - StartAddress + 1)
        
    return number * sensorRowGranularity

def _roundUp(val, multiple) :
    if ((val % multiple) > 0) :
        val += multiple - (val % multiple)
    return val

def _roundDown(val, multiple) :
    if ((val % multiple) > 0) :
        val = int(val) - int(val) % multiple
    return val
