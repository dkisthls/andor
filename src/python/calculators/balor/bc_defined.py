import numpy

###------------------------------ bcConstants ----------------------------------
class bcConstants:
  SENSOR_WIDTH = 4128                # Sensor array width(x) in pixels
  SENSOR_HEIGHT = 4104               # Sensor array height(y) in pixels
  AOI_TOP = 1

  # Define the gain modes for the camera (just for reference, not used)
  STANDARD_16_BIT = 0
  GAIN_MODES = ['Standard (16-bit)']

  # Define the ElectronicShutteringModes for the camera
  GLOBAL  = 0
  ROLLING = 1
  SHUTTER_MODES = ['Global', 'Rolling']

  # Define the Trigger Modes for the camera
  INTERNAL = 0
  SOFTWARE = 1
  EXTERNAL = 2
  TRIGGER_MODES = ['Internal', 'Software', 'External']

###------------------------------ bcDefaults -----------------------------------
class bcDefaults:
  SHUTTER_MODE = bcConstants.SHUTTER_MODES[bcConstants.ROLLING]
  TRIGGER_MODE = bcConstants.TRIGGER_MODES[bcConstants.EXTERNAL]
  ACQUIRING = False
  TRIGGER_DELAY = 0.0
  VBIN = 1
