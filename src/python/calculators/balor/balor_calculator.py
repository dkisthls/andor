import sys
sys.path.append('../../util')

import numpy

import misc                                  # Miscellaneous util methods
import calc_exposure_and_framerate as calc
from aoi_roi import Aoi, Roi
from bc_defined import bcConstants as const
from bc_defined import bcDefaults as default


class BalorCalculator:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self):
        # Initialization values
        self._shutter_mode  = default.SHUTTER_MODE
        self._trigger_mode  = default.TRIGGER_MODE
        self._acquiring     = default.ACQUIRING
        self._trigger_delay = default.TRIGGER_DELAY
        self._vBin          = default.VBIN
        self._sensor_height = const.SENSOR_HEIGHT
        self._aoi_top       = const.AOI_TOP
        self._roi_list      = []
        self._start_end_binned = []

        # Calculated values
        self._act_exposure_time = 0.0      # Actual exposure time
        self._min_exposure_time = 0.0      # Minimum exposure time
        self._max_exposure_time = 0.0      # Maximum exposure time
        self._cycle_time = 0.0             # 1/_cycle_time == max frame rate
        self._long_exposure_transition = 0.0
    #
    # End of BalorCalculator.__init__()


    #---------------------------- __str__() ------------------------------------
    #
    def __str__(self):
#        return f'{vars(self)}'
        return str(""
            "Python Calculator Parameters/Values:\n"
            "            constants: AOITop=%d, sensorHeight=%d\n"
            "          shutterMode=\"%s\", triggerMode=\"%s\"\n" 
            "         triggerDelay=%f\n"
            "            acquiring=%r\n"
            "          verticalBin=%d\n"
            "              numROIs=%d\n"
            "              roiList=%s\n"
            "     start_end_binned=%s""" % (
                self._aoi_top, self._sensor_height,
                self._shutter_mode, self._trigger_mode,
                self._trigger_delay,
                self._acquiring,
                self._vBin,
                len(self._roi_list), self._roi_list,
                self._start_end_binned ) )
    #
    # End of BalorCalculator.__str__()


    #---------------------------- configure() ----------------------------------
    #
    def configure(self, shutterMode, triggerMode, triggerDelay, acquiring):
        self._shutter_mode  = shutterMode
        self._trigger_mode  = triggerMode
        self._trigger_delay = triggerDelay
        self._acquiring     = acquiring
    #
    # End of BalorCalculator.configure()


    #---------------------------- calculate() ----------------------------------
    # NOTE: Calculations are based on current shutter mode, trigger mode, and
    #       acquiring state.
    # NOTE: Don't need to pass in actual exposuretime - will calc actual exp as
    #       part of framerate calc
    # Input:
    #   desired_exp_time - The desired exposure time (in sec).
    #   desired_frame_rate - The desired frame rate (in Hz).
    #   roiList - A list or ROIs which will be used to determine the total
    #             number of row reads.
    #   vBin - The vertical binning value. Note that the Zyla Calcualtor does
    #          not need vertical binning. The value is optional and only
    #          provided for call compatability with the Balor calculator.
    #          All ROIs provided to the calculator are in sensor pixels, not
    #          super pixeld so there is no need to back out any binning that
    #          may be applied and reflected in super-pixels.
    #
    # Return: (minExp, maxExp, actualExp, maxFrameRate, longExposureTranstion)
    #   minExp:
    #   The minimum exposure time based on acquiring state and shutter mode:
    #
    #     acquiring=true: Value depends on current shutter mode
    #         Global/Rolling: The minimum value for either a short or long
    #                          exposure.
    #        100% Duty Cycle: Essentially 1/frameRate
    #
    #    acquiring=false: The absolute minimum for the camera and the current
    #                     shutter mode.
    #   maxExp:
    #   The maximum exposure time based on acquiring state and shutter mode:
    #
    #     acquiring=true: Value depends on current shutter mode
    #         Global/Rolling: The maximum value for either a short or long
    #                         exposure.
    #        100% Duty Cycle: Essentially 1/frameRate
    #
    #     acquiring=false: The absolute maximum for the camera and the current
    #                      shutter mode.
    #
    #   actualExp:
    #   The calculated actual exposure time based on the desired_exp_time
    #
    #   cycle_time:
    #   The calculated cycle time which is the time from when the camera is
    #   triggered to when it is ready to be triggered again. The inverse
    #   cycleTime (1/cycleTime) is the maximum frame rate.
    #
    #   longExposureTransition:
    #   The boundary beteen long and short exposures. Crossing this boundary
    #   from a current exposure time to a new exposure time requires acquisition
    #   be stopped on the camera.
    #      Long Exposure: desired_exp_time >= longExposureTransition
    #     Short Exposure: desired_exp_time < longExposureTransition
    #
    #
    def calculate(self, desired_exp_time, roiList, vBin):
        self._vBin = vBin

        # Convert the list or ROI arrays to a list of Roi objects and save
        self._roi_list.clear()
        for r in roiList:
            self._roi_list.append( Roi.from_array(r))

        # Reset all calcualted valued
        self._reset_calculated()

        if len(self._roi_list) == 1:      # Using AOI features
            # 1) AOI origin is one based, CSS is zero based - Add +1 to ROI originY
            # 2) AOI Y-origin must be transformed from lower-left to upper-left to
            #    reference the desired ROI origin with the sensor pixel (1,1).
            #    This is independent of actual sensor orientation.
            # 3) AOI size is expressed in super-pixels. Take defined ROI size and
            #    divide by defined vertical (Y) binning size
            roi = self._roi_list[0];
            top = self._sensor_height - (roi.originY + roi.sizeY) + 1
            sizeY = roi.sizeY / self._vBin

            # Make sure multitrack is disabled in the calculator...
            self._start_end_binned = []
            calc.set_Multitrack( False, self._start_end_binned )

            # Set values in calculator...
            calc.set_Values(           #    param      type        comment
                self._shutter_mode,    # shutter_mode, str,   Current shutter mode
                sizeY,                 # h,            int,   AOIHeight (in super pixels)
                self._trigger_mode,    # trig,         str,   Current trigger mode
                self._acquiring,       # acq,          bool,  Camera acquiring
                self._trigger_delay,   # trig_dly,     float, Trigger delay
                top,                   # top,          int    AOITop (transformed ROI to AOI)
                self._vBin)            # bin,          int    Vertical binning value

        else:                               # Using Multitrack features

            # ROIs are defined in order starting from the CSS origin (0,0),
            # Multi-track need to be defined on the camera in row order starting
            # from sensor row 1 which maps to CSS (0,4103) per the CSS origin.
            # Therefore, we have to program the multi-tracks on the camera using
            # the reverse ordering of the defined ROIs so that we're starting
            # with the ROI/Multi-track closest to row 1 of the sensor.
            # For purposes of the calculator, we're not programming the camera
            # but the multi-tracks should be defined in the same order as the
            # camera will be programmed.
            self._start_end_binned.clear()
            binned = False               # CSS does not support Multitrack binning

            # Build array of tuples for the set_Multitrack() call
            for roi in reversed(self._roi_list):
                start = self._sensor_height - (roi.originY + roi.sizeY) + 1
                end   = start + roi.sizeY - 1
                self._start_end_binned.append( (start, end, binned) )

            # Set values in calculator...
            # Use default for height/top since multitrack will override these.
            calc.set_Values(           #    param      type        comment
                self._shutter_mode,    # shutter_mode, str,   Current shutter mode
                self._sensor_height,   # h,            int,   Full sensor height
                self._trigger_mode,    # trig,         str,   Current trigger mode
                self._acquiring,       # acq,          bool,  Camera acquiring
                self._trigger_delay,   # trig_dly,     float, Trigger delay
                self._aoi_top,         # top,          int    Default AOITop value
                self._vBin)            # bin,          int    Vertical binning value

            #       param            type                   comment
            #       -----            ----                   -------
            # set_Multitrack(
            #    enabled,           string                  Multitrack enabled
            #    start_end_binned   tuples(int, int, bool)  (Multitrack start, end, binned)
            # )
            # Set multitrack values in calculator...
            calc.set_Multitrack( True, self._start_end_binned )

        if "Rolling" in self._shutter_mode:
            self._long_exposure_transition = 0.0  # per as reported by camera head
        else:
            self._long_exposure_transition = calc._get_LongExposureTransition()

        # Calculate actual exposure time and retrieve min/max exposure times.
        # We're only retrieveing the min/max for when acquisition is disabled
        # so the 'Long'/'Short' parameter for _get_Min(Max)ExposureTime() can
        # (unofficially) be anything. I'm using "NA".
        self._act_exposure_time = calc.calc_exposure( desired_exp_time )
        self._min_exposure_time = calc._get_MinExposureTime("NA")
        self._max_exposure_time = calc._get_MaxExposureTime("NA")

        # Calculate maximum frame rate. 1/maxFrameRate == cycleTime
        self._cycle_time = calc.calc_cycletime(desired_exp_time)

        return [self._min_exposure_time, self._max_exposure_time,
                self._act_exposure_time, self._cycle_time,
                self._long_exposure_transition]
    #
    # End of BalorCalculator.calculate()


    #-------------------- get_all_params_as_string() ---------------------------
    #
    def get_all_params_as_string(self):
        return ( self.__str__() )
    #
    # End of BalorCalculator.get_all_params_as_string()


    #---------------------- get_calculated_extras() ----------------------------
    #
    def get_calculated_extras(self):
        return [int(calc._get_TotalSensorRows()),
                calc._get_RowReadTime(), 
                calc._get_ReadoutTime()]
    #
    # End of BalorCalculator.get_calculated_extras()


    #----------------- get_calculated_extras_as_string() -----------------------
    #
    def get_calculated_extras_as_string(self):
        (total_sensor_rows, row_read_time, readout_time) = self.get_calculated_extras()
        return str(""
            "totalSensorRows=%d, rowReadTime=%.09fsec, readoutTime=%.09fsec""" % (
                total_sensor_rows,
                row_read_time,
                readout_time) )
    #
    # End of BalorCalculator.get_calculated_extras_as_string()


    ############################################################################
    #                          INTERNAL USE ONLY
    #

    #---------------------------  _reset_calculated() --------------------------
    #
    def _reset_calculated(self):
        self._act_exposure_time  = 0.0         # Actual exposure time
        self._min_exposure_time  = 0.0         # Minimum exposure time
        self._max_exposure_time  = 0.0         # Maximum exposure time
        self._cycle_time         = 0.0         # 1/_cycle_time == max frame rate
        self._long_exposure_transition = 0.0
    #
    # End of ZylaCalcualtor._reset_calculated()


