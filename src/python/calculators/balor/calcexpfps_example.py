import calc_exposure_and_framerate as calc

             #(       str,  int,        str,  bool,    float, int, int)
 #set_Values(shutter_mode,    h,       trig,   acq, trig_dly, top, bin): ' setting features as desired
calc.set_Values("Rolling", 4104, "Internal", False,        0,   1,   1) # set the 

                 #calc_exposure(exposuretime)
actual_exp = calc.calc_exposure(0.5)
                 #calc_framerate(exposuretime, framerate) # note don't need to pass in actual exposuretime - will calc actual exp as part of framerate calc
actual_fps = calc.calc_framerate(0.5, 1) 


# Similar Test With Multitrack first set basic values (it wont matter what values are used for height, aoitop, and aoihbin when using multitrack)
calc.set_Values("Rolling", 4104, "Internal", False, 0, 1, 1) # set the 
#calc.set_Multitrack(enabled, start_end_binned_list=[]):
calc.set_Multitrack(True, [(1, 2052, False), (3000, 3100, True), (3200, 3300, False)]) # set the 

                 #calc_exposure(exposuretime)
actual_exp = calc.calc_exposure(0.5)
                 #calc_framerate(exposuretime, framerate) # note don't need to pass in actual exposuretime - will calc actual exp as part of framerate calc
actual_fps = calc.calc_framerate(0.5, 1) 