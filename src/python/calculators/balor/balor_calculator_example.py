from balor_calculator import BalorCalculator

calculator = BalorCalculator()

#================================ EXAMPLE #1 ===================================
#
print("==================== Global Shutter, 1 ROI")
shutter_mode = "Global"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,4104)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "         act_exposure_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #2 ===================================
#
print("==================== Rolling Shutter, 1 ROI")
shutter_mode = "Rolling"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,4104)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "              act_exp_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #3 ===================================
#
print("==================== Global Shutter, 2 ROIs(Multitrack)")
shutter_mode = "Global"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,1000),(0,1500,4128,1000)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "              act_exp_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #4 ===================================
#
print("==================== Rolling Shutter, 2 ROIs(Multitrack)")
shutter_mode = "Rolling"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,1000),(0,1500,4128,1000)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "              act_exp_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #5 ===================================
#
print("==================== Global Shutter 100% Duty Cycle, 1 ROI")
shutter_mode = "Global - 100% Duty Cycle"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,4104)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "              act_exp_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))


#================================ EXAMPLE #6 ===================================
#
print("==================== Rolling Shutter 100% Duty Cycle, 1 ROI")
shutter_mode = "Rolling - 100% Duty Cycle"
trigger_mode = "External"
acquiring = False
trigger_delay = 0.0
vBin = 1
roi_list = [(0,0,4128,4104)]
exposure_time = 0.1

print("Desired exposure time={}s\n".format(exposure_time))

calculator.configure(shutter_mode, trigger_mode, trigger_delay, acquiring)

(min_exp_time, max_exp_time, act_exp_time, cycle_time, long_exposure_transition) = \
    calculator.calculate(exposure_time, roi_list, vBin)

print(calculator.get_all_params_as_string())
print("\nCalculated Values:\n"
      "{}\n"
      "          min/max_exp_time={:.06f}sec/{:.06f}sec\n"
      "              act_exp_time={:.06f}sec\n"
      "  long_exposure_transition={:.06f}sec\n"
      "                cycle_time={:.09f}sec\n"
      "         max exposure rate={:.18f}Hz (== 1/cycle_time)\n".format(
        calculator.get_calculated_extras_as_string(),
        min_exp_time, max_exp_time,
        act_exp_time,
        long_exposure_transition,
        cycle_time,
        (1.0/cycle_time)))

