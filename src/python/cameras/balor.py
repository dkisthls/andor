#!/usr/bin/env python3

from andor_camera import AndorCamera

GAIN_STANDARD = 'Standard (16-bit)'
GAIN_MODES = [GAIN_STANDARD]
GAIN_MODE_DEFAULT = [GAIN_STANDARD]

SHUTTER_MODE_GLOBAL = 'Global'
SHUTTER_MODE_GLOBAL_100PCT = 'Global - 100% Duty Cycle'
SHUTTER_MODE_ROLLING = 'Rolling'
SHUTTER_MODE_ROLLING_100PCT = 'Rolling - 100% Duty Cycle'
SHUTTER_MODES = [SHUTTER_MODE_GLOBAL, SHUTTER_MODE_ROLLING]
SHUTTER_MODE_DEFAULT = SHUTTER_MODES

def add_arguments(parser):
    global GAIN_MODES
    global GAIN_MODE_DEFAULT
    global SHUTTER_MODES
    global SHUTTER_MODE_DEFAULT

    balor_group = parser.add_argument_group('balor')

    balor_group.add_argument('--gainMode', choices=GAIN_MODES,
        action='store', default=GAIN_MODE_DEFAULT,
        help='R|Used to select a specific Balor gain mode for the test.\n')

    balor_group.add_argument('--shutterMode', choices=SHUTTER_MODES,
        nargs='+', action='store', default=SHUTTER_MODE_DEFAULT,
        help='R|Used to select one or more shutter modes for the test.\n')

#===============================================================================
# CLASS: Balor
#===============================================================================

class Balor(AndorCamera):
    def __init__(self, cmdLineArgs):
        AndorCamera.__init__(self, cmdLineArgs)
    #
    # End of Balor.__init__()

