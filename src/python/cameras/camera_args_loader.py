#!/usr/bin/env python3

import importlib

def load_camera_args(parser, camera):
    module = importlib.import_module(camera)
    module.add_arguments(parser)

