#!/usr/bin/env python3

from andor_camera import AndorCamera

GAIN_16BIT_LNHW = '16-bit (low noise & high well capacity)'
GAIN_12BIT_HW   = '12-bit (high well capacity)'
GAIN_MODES  = [GAIN_16BIT_LNHW, GAIN_12BIT_HW]
GAIN_MODE_DEFAULT = [GAIN_16BIT_LNHW]

SHUTTER_MODE_GLOBAL = 'Global'
SHUTTER_MODE_ROLLING = 'Rolling'
SHUTTER_MODES = [SHUTTER_MODE_GLOBAL, SHUTTER_MODE_ROLLING]
SHUTTER_MODE_DEFAULT = SHUTTER_MODES

def add_arguments(parser):
    global GAIN_MODES
    global GAIN_MODE_DEFAULT
    global SHUTTER_MODES
    global SHUTTER_MODE_DEFAULT

    zyla_group = parser.add_argument_group('zyla')

    zyla_group.add_argument('--gainMode', choices=GAIN_MODES,
        nargs='+', action='store', default=GAIN_MODE_DEFAULT,
        help='R|Used to select a specific Zyla gain mode for the test.\n')

    zyla_group.add_argument('--shutterMode', choices=SHUTTER_MODES,
        nargs='+', action='store', default=SHUTTER_MODE_DEFAULT,
        help='R|Used to select one or more shutter modes for the test.\n')

#===============================================================================
# CLASS: Zyla
#===============================================================================

class Zyla(AndorCamera):
    def __init__(self, cmdLineArgs):
        AndorCamera.__init__(self, cmdLineArgs)
        self.cam.FastAOIFrameRateEnable = self._cla.fastAOIFrameRateEnable
    #
    # End of Zyla.__init__()

