#!/usr/bin/env python3

from pyAndorSDK3 import AndorSDK3
from pyAndorSDK3.andor_camera import CameraException
from aoi_roi import Aoi, Roi

#===============================================================================
# CLASS: AndorCamera
#===============================================================================
DEFAULT_PIXEL_ENCODING = 'Mono16'

class AndorCamera():
    #--------------------------- __init__() ------------------------------------
    #
    def __init__(self, cmdLineArgs):
        global DEFAULT_PIXEL_ENCODING
        self.cam = None           # Holds camera object
        self.ready = False
        self._cla = cmdLineArgs
        self._type = self._cla.cameraType
        self._name = None         # As reported by CameraName
        self._sdk3 = None         # Holds sdk3 object
        self._sn = None           # As reported by SerialNumber

        # Initialize SDK3 and connect to camera
        if not self._open():
            raise RuntimeError

        self.full_frame_aoi = Aoi(
            1, 1, self.cam.SensorWidth, self.cam.SensorHeight)

        self.full_frame_roi = Roi(
            0, 0, self.cam.SensorWidth, self.cam.SensorHeight)

        # Set cam features that are persistent for life of camera
        self.cam.PixelEncoding = DEFAULT_PIXEL_ENCODING
    #
    # End of AndorCamera.__init__()


    #----------------------------- __del__() -----------------------------------
    #
    def __del__(self):
        if self.ready:
            self._close()
    #
    # End of AndorCamera.__del__()


    #------------------------------ _open() ------------------------------------
    #
    def _open(self):
        if self.ready: return

        print('Initializing SDK3...', end='', flush=True)
        self._sdk3 = AndorSDK3()                # Initialise SDK3
        print('Success!')

        print('Connecting to camera...', end='', flush=True)
        try :
            self.cam = self._sdk3.GetCamera(0);
            self._name = self.cam.CameraName
            self._sn = self.cam.SerialNumber
            print('Success!')
            print('Connected to {}, S/N {}'.format(self._name, self._sn))
            self.ready = True

        except ATCoreException as err :
            print('     SDK3 Error {0}'.format(err));
            print('  Closing camera');
            self._sdk3.close(self._hndl);

        # Check that the camera running is the expected type
        if not self._name.casefold() in self._type.casefold():
            print('ERROR - Camera mismatch: Expected={}, Running={}'.format(
                self._type, self._name))
            self.close()
            return

        return self.ready
    #
    # End of AndorCamera._open()


    #----------------------------- _close() ------------------------------------
    #
    def _close(self):
        #Reinitiate this once testing is completed
        #sdk3.set_bool(hndl, "SensorCooling", 0)
        if self.cam.CameraAcquiring == 1:
            Stop_Acquisition(self._sdk3, self._hndl)

        self.cam.close()

        self.ready = False
        print ('Camera closed!')
    #
    # End of AndorCamera._close()

    #-------------------------- reset_aoi_bin() --------------------------------
    # Utility method to reset AOI to full frame and binning to 1x1
    def reset_aoi_bin(self):
        self.cam.AOIHBin = 1
        self.cam.AOIVBin = 1
        self.cam.AoiLayout = 'Image'
        self.cam.AoiWidth = self.full_frame_aoi.width
        self.cam.AoiLeft = self.full_frame_aoi.left
        self.cam.AoiHeight = self.full_frame_aoi.height
        self.cam.AoiTop = self.full_frame_aoi.top



